require 'lib/code_owners'

class Codeowners < Middleman::Extension
  attr_reader :repository
  expose_to_template :codeowners_for_page

  def initialize(app, options_hash = {}, &block)
    super

    @root = File.expand_path('..', __dir__)
    @codeowners = Gitlab::CodeOwners.load_codeowners_file(root)
  end

  def codeowners_for_page(page)
    codeowners
      .owners_for_path(relative_path(page))
      .reject { |user| user == '@gitlab-com' }
      .map { |user| user_presenter.present(user) }
      .compact
  end

  private

  attr_reader :codeowners, :root

  def relative_path(page)
    Pathname.new(page.source_file).relative_path_from(root).to_s
  end

  def user_presenter
    @user_presenter ||= UserPresenter.new(@app.data.team)
  end

  class UserPresenter
    PATH_TO_IMAGES = '/images/team/'.freeze

    def initialize(team)
      @team = team
    end

    def present(username)
      return render(username) unless gitlab_name?(username)

      gitlab_name = username.gsub(/@/, '')
      user = team.find { |user| user['gitlab'] == gitlab_name } || { 'name' => username }

      render(user['name'], url: "https://gitlab.com/#{gitlab_name}", gitlab_name: username, image_url: image_url(user['picture']))
    end

    private

    attr_reader :team

    def render(description, url: nil, gitlab_name: nil, image_url: nil)
      {
        description: description,
        image_url: image_url,
        url: url,
        gitlab_name: gitlab_name
      }
    end

    def gitlab_name?(username)
      username.start_with?('@')
    end

    def image_url(picture_name)
      return unless picture_name

      File.join(PATH_TO_IMAGES, File.basename(picture_name, File.extname(picture_name)) + "-crop.jpg")
    end
  end
end

::Middleman::Extensions.register(:codeowners, Codeowners)
