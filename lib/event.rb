require 'yaml'

module Gitlab
  module Homepage
    class Event
      include Comparable

      def initialize(data)
        @data = data
      end

      # Some dates are actually a range. Need to get the start date
      # for event sort comparison.
      def sort_date
        @sort_date ||= Date.parse(date.gsub(/(\s+)(\d+)\s*-\s*\d+,/, '\1\2,'))
      end

      def <=>(other)
        sort_date <=> other.sort_date
      end

      # rubocop:disable Style/MethodMissingSuper
      # rubocop:disable Style/MissingRespondToMissing

      ##
      # Middeman Data File objects compatibility
      #
      def method_missing(name, *args, &block)
        @data[name.to_s]
      end

      # rubocop:enable Style/MethodMissingSuper
      # rubocop:enable Style/MissingRespondToMissing

      def self.all!
        @events ||= YAML.load_file(File.expand_path('../data/events.yml', __dir__))
        @events.map do |data|
          new(data)
        end
      end
    end
  end
end
