---
layout: job_family_page
title: "Software Engineer in Test"
---

Software Engineers in Test at GitLab help grow our test automation efforts across the entire GitLab ecosystem.
As a Software Engineer in Test, you'll work in a key position, where your efforts will have a noticeable impact on both the company and product.
In addition to the requirements below, successful candidates and team members will share a passion for high-quality software,
strong engineering principles, and methodical problem-solving skills.

## Responsibilities

- Expand our existing test automation framework and test coverage.
- Develop new tests and tools for our GitLab.com frontend, backend APIs and services, and low-level systems like geo-replication,
CI/CD, and load balancing.
- Have the ability to grow your knowledge of GitLab provisioning and setup tools like Terraform and Ansible.
- Setup and maintain new GitLab test environments.
- You’ll work on test automation issues related to the Quality department, the [stage group you're embedded in](https://about.gitlab.com/handbook/product/product-categories/#devops-stages), and the entire GitLab product. The work you’ll do will be [transparent and open to the GitLab community](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Quality)
- Participate in test pipeline triage rotation and ensure pipeline failures are triaged promptly.
- Develop working knowledge of the entire GitLab application stack including tests at all levels. (Unit, Integration, and End-to-end).
- Groom and prune GitLab tests at all levels to ensure optimal coverage and effective deduplication.
- Collaborate with a [Product Manager and with the wider product and development team](https://about.gitlab.com/handbook/product-development-flow/#build-track) to understand how new features should be tested and to contribute to automated tests for these new features.
- Collaborate with engineers to define and implement mechanisms to inject testing earlier into the software development process.
- Identify, share, and nurture the adoption of best practices in code health, testing, testability, and maintainability in collaboration with the wider Quality department. You should champion clean code and the concept of the test pyramid.
- Analyze complex software systems and collaborate with others to improve the overall design, testability, and quality.
- Strive for the fastest feedback possible. Test parallelization should be a top priority.
You see distributed systems as a core challenge of good test automation infrastructure.
- Configure automated tests to execute reliably and efficiently in CI/CD environments.
- Track and communicate test results in a timely, effective, and automated manner.


## Requirements

- You have a few years of professional experience developing in Ruby or working on a Ruby on Rails application
- You bring significant experience using Git and its command line
- You’re comfortable with Selenium-based test automation tools like Capybara
- You’ve got a working knowledge of Docker
- You’re interested in growing your knowledge and skills in Test Environment Provisioning and Configuring using technologies like Terraform, Ansible, Kubernetes, GCP, or AWS
- You have experience with Continuous Integration systems (e.g., Jenkins, Travis, GitLab)
- Our [values](https://about.gitlab.com/handbook/values/) of collaboration, results, efficiency, diversity, iteration, and transparency resonate with you
- You’ll thrive in an environment where [self-learning and self-service](https://about.gitlab.com/company/culture/all-remote/self-service/#self-service-and-self-learning-in-onboarding) is encouraged and instilled as a part of our culture
- Ability to use GitLab


## Junior Software Engineer in Test

1 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.

#### Job Grade

The Junior Software Engineer in Test is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

* **Responsibilities**
  *  Implement basic test automation and continuous integration when collaborating with other team members and when given designs.
  *  Maintain existing test automation framework.
  *  Maintain our CI system.
* **Planning & Organization**
  * Organize and complete structured assignments.
* **Independence & Initiative**
  * Work under general direction on problems of limited scope; use discretion to complete tasks.

## Intermediate Software Engineer in Test

3 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.

#### Job Grade

The Software Engineer in Test is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

* **Responsibilities** (extends the responsibilities of the previous level)
  * Implement test automation for new features in collaboration with other Software Engineers in Test.
  * Occasionally lead test automation efforts on new features.
  * Participate in test plan discussion.
  * Participate in architectural discussions.
  * Participate in design reviews with product management and engineering teams.
  * Maintain test infrastructure stability in production and non-production environments.
* **Planning & Organization**
  * Establish deadlines and approach for completing some assignments; demonstrates project management skills as part of a larger team.
* **Independence & Initiative**
  * Work on problems of moderate scope; exercises judgment and independently identifies the next steps.

## Senior Software Engineer in Test

7 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.

#### Job Grade

The Senior Software Engineer in Test is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

* **Responsibilities** (extends the responsibilities of the previous level)
  * Lead test automation implementation and guide the testing approach for new feature development in collaboration with stage group counterparts.
  * Create test plans for new features and steer the team to ensure test coverage based on the test plan.
  * Identify test gaps and prioritize adding coverage based on areas of risk.
  * Provide input into testing the security and scalability of the product.
  * Lead development of new tooling and infrastructure in collaboration with other Quality department team members.
  * Implement new automation framework features with little guidance.
  * Recommend new test automation tools and processes that will improve our quality and velocity.
  * Take ownership of test failures and ensure that our CI system is reliable.
  * Mentor other engineers.
  * Occasionally contribute to the company blog.
* **Planning & Organization**
  * Independently and regularly manage project schedules ensuring objectives are aligned with team/department goals.
* **Independence & Initiative**
  * Work on problems of diverse scope requiring independent evaluation of identifiable factors; recommend new approaches to resolve problems.

## Staff Software Engineer in Test

10 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.

#### Job Grade

The Staff Software Engineer in Test in Test is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

* **Responsibilities** (extends the responsibilities of the previous level)
  * Lead test automation infrastructure implementation across multiple product areas in collaboration with stage group and functional area counterparts.
  * Regularly lead discussions on architectural improvements of test tooling and infrastructure.
  * Regularly consulted on testing approach for new feature development.
  * Analyze engineering metrics and make suggestions to improve engineering processes and velocity.
  * Participate in customer calls and take part in Engineering outreach.
  * Regularly review new content from the department added to the company blog.
* **Planning & Organization**
  * Define and independently manages multiple projects within the department.
* **Independence & Initiative**
  * Work on complex department issues that have an impact on the company's bottom line. Able to create new methods for obtaining results.

## Career Ladder

The next step for both individual contributors and managers of people is to move to the [Quality Engineering Leadership](/job-families/engineering/engineering-management-quality) job family.

## Performance Indicators

Software Engineers in Test have the following job-family performance indicators.

* [Average CE/EE master end-to-end test suite execution duration per month](/handbook/engineering/quality/performance-indicators/#average-ce-ee-master-end-to-end-test-suite-execution-duration-per-month)
* [Ratio of quarantine vs total end-to-end tests in master per month](/handbook/engineering/quality/performance-indicators/#ratio-of-quarantine-vs-total-end-to-end-tests-in-master-per-month)
* [Successful vs failed CE/EE master pipelines per month](/handbook/engineering/quality/performance-indicators/#successful-vs-failed-ce-ee-master-pipelines-per-month)
* [New issue first triage SLO](/handbook/engineering/quality/performance-indicators/#new-issue-first-triage-slo)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below.
Please keep in mind that candidates can be declined from the position at any stage of the process.
As a result an interview can be canceled at any time even if the interviews are very close (e.g. a few hours apart).

To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Candidates will be invited to complete a technical assessment from our Recruiting team.
- Qualified candidates will be invited to schedule a 30 minute screening call with our Recruiting team.
- Next, candidates will be invited to schedule a 45 minute first interview with the Hiring Manager for the team you are interviewing for. Once the interview is completed, there may be occasions where the interviewer believes you are a better match for another team we are hiring for. In this instance, we will ask you to meet another Hiring Manager as part of the process. 
- Next, candidates will then be invited to schedule a 1 hour peer interview with two Engineers in the Quality department.
- Next, candidates will then be invited to schedule a 1 hour technical interview with a Senior Software Engineer in Test. This will be booked for a date that is around 7-10 business days into the future. This is done in order to give them time to complete the asynchronous portion of the technical interview.  
- Finally, candidates will be invited to schedule a 45 minute interview with the Head of Quality.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
