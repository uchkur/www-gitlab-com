---
layout: job_family_page
title: "UX Research Management"
---

## UX Research Management Roles at GitLab

Managers in the UX Research department at GitLab see the team as their product. While they are credible as researchers and know the details of what UX Researchers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of UX Research commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Manager, UX Research

The User Experience (UX) Research Manager reports to the Senior Manager or Director of UX Research, and UX Researchers report to the Manager, UX Research.

#### Job Grade

The Manager, UX Research is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Manage up to 7 UX Researchers, and hold regular 1:1s with team members.
* Lead and mentor UX Researchers to support their career development, including making sure they each have an [Individual Growth Plan](/handbook/people-group/learning-and-development/career-development/#internal-resources).
* Demonstrate, explain, and teach others how to apply a range of research methods, helping them to understand the balance and trade-offs between smart, scrappy research and rigor.
* Regularly meet with Product Management Directors to understand strategy, discover blockers, manage quality, and help ensure UX prioritization.
* Ensure that UX Researchers have clearly prioritized deliverables for every milestone.
* Review research deliverables that your team creates, and provide feedback to ensure high-quality output.
* Build relationships with other managers across disciplines, and act as a liaison with teams throughout the wider organization that have ongoing research needs.
* Actively seek out difficult impediments to our efficiency as a team (process, tooling, and so on), and propose and implement solutions that will enable the entire team to work more efficiently.
* Exert significant influence on the overall objectives and long-range goals of the UX Research team.
* Interview potential UX candidates.

#### Requirements

* Expert-level knowledge in the field of UX Research with at least 5 years of experience in hands-on research.
* A minimum of 3 years of experience managing UX Researchers or large research teams.
* Outstanding communicator both verbally and in writing.
* Strong collaboration skills.
* A proven record of driving change through UX Research.
* Passion for the field of UX Research.
* Evangelize research. Share user insights within the broader organization and externally in creative ways to increase empathy.
* Able to use GitLab to plan/manage work and update the company handbook.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)

**NOTE:** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

### Senior Manager, UX Research

The Senior Manager, UX Research reports to the Director of UX, and Manager, UX Research report to the Senior Manager, UX Research.

#### Job Grade

The Senior Manager, UX Research is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Create an open and collaborative culture based on trust in the UX Research team.
* Develop an overarching UX research strategy for GitLab, and work with the UX Research team and cross-functional partners to execute that strategy.
* Mentor UX Research Managers in their career growth, including making sure they each have an [Individual Growth Plan](/handbook/people-group/learning-and-development/career-development/#internal-resources).
* Help scale UX Research by defining a plan to provide research training for Product Managers and Product Designers, and coordinate the UX Research team to create and maintain training materials.
* Actively advocate for UX Research throughout the company, so that the company is aware of what UX Research does, how they do it, and what insights they have delivered.
* Stay informed of industry best practices, and help the team to evolve our research practice accordingly.
* Identify ways to more efficiently and effectively conduct remote user research, and share those learnings with the wider community.
* Work with Product Operations to ensure UX research is included appropriately in the [Product Development Flow](/handbook/product-development-flow/).
* Define and manage [Performance Indicators](/handbook/engineering/ux/performance-indicators/) for the UX Research team.
* Create and manage [OKRs](/company/okrs/) for the UX Research team with guidance from UX Leadership.
* Align the way that UX Researchers work with how their cross-functional partners in Product Managment, Product Design, and Engineering prioritize, track, and manage work.
* Conduct regular [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with their direct reports and quarterly [skip-level meetings](https://about.gitlab.com/handbook/leadership/skip-levels/) with the reports of their direct reports.
* Provide clear and direct feedback to the people they manage, their own manager, and cross-functional partners when needed.
* Hire a world-class team of UX Researchers.

#### Requirements

* Expert-level knowledge in the field of UX Research with at least 8 years of experience in hands-on research.
* A minimum of 2 years of experience managing UX Research Managers.
* Outstanding communicator both verbally and in writing.
* Strong collaboration skills.
* A proven record of leading a team that drives change through UX Research.
* Experience advocating for UX Research and broadly communicating about research insights to stakeholders and partners.
* Passion for the field of UX Research.
* Able to use GitLab to plan/manage work and update the company handbook.
* Ideally, has experience leading a remote UX research team that conducts research fully remote, too.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)

**NOTE:** In the compensation calculator below, fill in "Senior Manager" in the `Level` field for this role.

#### Interview Process

- [Screening call](/handbook/hiring/#screening-call) with a recruiter. In this call we will discuss your experience, understand what you are looking for in a UX Research leadership role, talk about your work and approach to user research, discuss your compensation expectations and reasons why you want to join GitLab, and answer any questions you have.
- Interview with a UX Researcher. In this interview, the interviewer will spend a lot of time trying to understand the experiences and challenges you've had as a manager, specific details of any initiatives you've led, the problems you were trying to solve, and the outcomes of these initiatives. Do be prepared to talk about your work and research experience, too.
- Interview with a UX Manager. In this interview, we want to get to know how you think about the place of user research within an organization, understand how you define strategy and vision, and understand the size and scope of teams you've managed throughout your career.
- Interview with a UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, your ways of ensuring the team is well-positioned to produce research studies that have a strong impact on product strategy, what types of teams you have led, and your management style. The interviewer will also want to understand how you work with cross-functional partners, how you've handled conflict, and how you've dealt with difficult situations in the past.
- Interview with Executive VP of Engineering. In this interview, the interviewer will look to understand how you drive strategy, approach difficult conversations, and how you see the role of user research in a cross-functional research and development organization.  

## Director, UX Research

The Director of UX Research reports to the Director of UX, and UX Research Managers report to the Director of UX Research.

#### Job Grade

The Director, UX Research is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Create an open and collaborative culture based on trust in the UX Research team.
* Set an ambitious vision for UX Research that other companies use as a benchmark for their own research initiatives.
* Align UX Research to business goals and company revenue and clearly communicate the resulting impact.
* Develop an overarching UX research strategy for GitLab, and work with the UX Research team and cross-functional partners to execute that strategy.
* Mentor UX Research Managers in their career growth, including making sure they each have an [Individual Growth Plan](/handbook/people-group/learning-and-development/career-development/#internal-resources).
* Help scale UX Research by defining a plan to provide research training for Product Managers and Product Designers, and coordinate the UX Research team to create and maintain training materials.
* Actively advocate for UX Research throughout the company, so that the company is aware of what UX Research does, how they do it, and what insights they have delivered.
* Stay informed of industry best practices, and help the team to evolve our research practice accordingly.
* Identify ways to more efficiently and effectively conduct remote user research, and share those learnings with the wider community.
* Work with Product Operations to ensure UXR consideration in the [Product Development Flow](/handbook/product-development-flow/).
* Define and manage [Performance Indicators](/handbook/engineering/ux/performance-indicators/) for the UX Research team.
* Independently create and manage [OKRs](/company/okrs/) for the UX Research team.
* Align the way that UX Researchers work with how their cross-functional partners in Product Managment, Product Design, and Engineering prioritize, track, and manage work.
* Conduct regular [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with their direct reports and quarterly [skip-level meetings](https://about.gitlab.com/handbook/leadership/skip-levels/) with the reports of their direct reports.
* Provide clear and direct feedback to the people they manage, their own manager, and cross-functional partners when needed.
* Hire a world-class team of UX Researchers.
* Manage the UX Research budget with oversight from UX leadership.

#### Requirements

* Expert-level knowledge in the field of UX Research with at least 10 years of experience in hands-on research.
* A minimum of 4 years of experience managing UX Research Managers.
* Outstanding communicator both verbally and in writing.
* Strong collaboration skills.
* A proven record of leading a team that drives change through UX Research.
* Experience advocating for UX Research and broadly communicating about research insights to stakeholders and partners.
* Passion for the field of UX Research.
* Able to use GitLab to plan/manage work and update the company handbook.
* Ideally, has experience leading a remote UX research team that conducts research fully remote, too.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)

**NOTE:** In the compensation calculator below, fill in "Director" in the `Level` field for this role.

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.

## Performance indicators
* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)

#### Interview Process

- [Screening call](/handbook/hiring/#screening-call) with a recruiter. In this call we will discuss your experience, understand what you are looking for in a UX Research leadership role, talk about your work and approach to user research, discuss your compensation expectations and reasons why you want to join GitLab, and answer any questions you have.
- Interview with a UX Researcher. In this interview, the interviewer will spend a lot of time trying to understand the experiences and challenges you've had as a manager, specific details of any initiatives you've led, the problems you were trying to solve, and the outcomes of these initiatives. Do be prepared to talk about your work and research experience, too.
- Interview with a UX Manager. In this interview, we want to get to know how you think about the place of user research within an organization, understand how you define strategy and vision, and understand the size and scope of teams you've managed throughout your career.
- Interview with a UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, your ways of ensuring the team is well-positioned to produce research studies that have a strong impact on product strategy, what types of teams you have led, and your management style. The interviewer will also want to understand how you work with cross-functional partners, how you've handled conflict, and how you've dealt with difficult situations in the past.
- Interview with Executive VP of Engineering. In this interview, the interviewer will look to understand how you drive strategy, approach difficult conversations, and how you see the role of user research in a cross-functional research and development organization.   
