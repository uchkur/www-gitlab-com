---
layout: job_family_page
title: "Community Advocate"
---

## Role

As a GitLab Community Advocate, you will be a member of the Community Advocacy team within Community Relations, with a goal of responding to every question about GitLab asked online. You will help to create processes and documentation around the way the team interacts with the community, as well as help make it easier to provide feedback about GitLab. Most importantly, you will play an active role in connecting experts within the GitLab team to our wider community. With this, you will be contributing to ensure that the GitLab team continues to remain engaged with our community, to provide the best answers we can give, and to actively listen and act on their feedback as the company scales.

## Responsibilities

- Respond to the wider GitLab community across [our response channels](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/#community-response-channels) in a timely manner.
- Engage with the developer and user community in a way that is direct, but friendly and authentic. Be able to carry the tone of the GitLab brand, while also giving the proper answers or direction to members of the community.
- Help update the [social media guidelines](/handbook/marketing/social-media-guidelines/) and GitLab voice as new situations arise.
- Work with leadership to find a way to track and measure response time across all channels with the ideal response time being under 1 hour for all channels.
- Explore different tools to find a way to effectively track and respond to all mentions of GitLab across the internet.
- Don't be afraid of animated gifs and well-placed humor! We are not robots.
- Work within the GitLab process to help users report bugs, make feature requests, contact support, and provide feedback to the product.

## Requirements

- Have an understanding of Git, GitLab, and modern development practices.
- A broad knowledge of the application development ecosystem.
- Be a good conversation partner for experienced developers.
- Know multiple programming languages, Ruby on Rails experience is a plus.
- Excellent written and spoken English.
- Accurate, nuanced, direct, and kind messaging.
- Being able to work independent and respond quickly.
- Able to articulate the GitLab mission, values, and vision.
- Understand the difference between different online developer communities.
- You LOVE working with the GitLab community.
- Ability to use GitLab

## Levels

### Manager, Community Advocate

#### Responsibilities

- Lead and develop a remote team of engaged Community Advocates that can effectively and timely respond to our growing community
- Define and execute the strategy with which all GitLab team members can actively engage with the wider community as the company scales
- Recruit new Community Advocates according the dynamic needs of a rapidly growing organization
- Define the Community Advocates' response strategy and collaborate to align it across teams
- Act as the liaison to represent the community views when coordinating company-wide communication as a response to particular events or news stories
- Support the Community Advocates to conduct their day to day engagement with the community in a way that is efficient and genuine
- Mentor, guide, and grow the careers of all team members
- Build upon and iterate on the team's processes and workflows 
- Report and iterate on performance indicators and results for community response
- Produce and execute the Community Advocates team's quarterly OKRs

#### Requirements

- 5+ years experience in developer public relations
- 3+ years experience in leading a team
- A history in engaging with Open Source communities, including users and contributors
- Analytical and data driven in the approach to serving and engaging with the community
- Bonus: experience with Zendesk, Zapier, or other tools to effectively track and respond to GitLab mentions online

## Performance Indicators

- Median first response time across social response channels
- Number of online GitLab mentions processed

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* A 45 minute interview with one of our Senior Community Advocates
* A 45 minute interview with one of the Program Managers in the Community Relations team
* A 45 minute interview with our Director of Community Relations
* A 45 minute interview with our Chief Marketing Officer
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Relevant links

- [Community Relations Handbook](/handbook/marketing/community-relations)
- [Community Advocacy Handbook](/handbook/marketing/community-relations/community-advocacy)
