---
layout: job_family_page
title: "PR Manager"
---

## Role

The GitLab PR Manager will help GitLab build thought leadership and drive conversation in external communications channels. This position is responsible for developing and overseeing the public relations strategy at GitLab, and will work across teams and the globe to develop and execute public relations campaigns in line with GitLab initiatives.

## Responsibilities

- Think globally to implement global public relations campaigns.
- Execute thought leadership, product, partner, technical, channel, crisis, rapid response and proactive PR campaigns.
- Manage GitLab’s PR agency relationship and develop a PR program in line with overall corporate marketing objectives and goals.
- Work closely with executives, spokespeople and the greater organization to develop press releases, blog posts and media relations strategy for GitLab announcements and news.
- Collaborate across the organization to support the news cycle through various channels, as well as educate teams on news.
- Oversee the GitLab awards submission program.
- Respond to daily media inquiries in a timely and professional manner.
- Have your finger on the pulse of the news and provide an overview of interesting news and trends.
- Report back on press activities, coverage, opportunities, successes and press feedback.
- Measure our PR successes in relation to awareness and impact.

## Requirements

- Strong media relations skills and a passion for PR.
- A natural storyteller with excellent writing skills.
- Able to coordinate across many teams and perform in fast-moving startup environment.
- Proven ability to be self-directed and work with minimal supervision.
- Outstanding written and verbal communications skills.
- You share our values, and work in accordance with those values.
- Highly organized, detail-oriented and able to meet deadlines consistently.
- Ability to use GitLab

## Levels

### PR Manager

#### Job Grade 

The PR Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Reponsibilities

- Execute PR plans.
- Work day-to-day with media and the PR agency to support PR efforts.
- Ability to develop press release, pitches and more.
- Work across teams to find potential news or story ideas.
- Independently manage projects from start to finish.

#### Requirements

- 5+ years experience in public relations.
- Experience in enterprise software or developer public relations.
- Provien experience in working for or with a PR agency in the past.

### Senior PR Manager

#### Job Grade 

The Senior PR Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Develop public relations strategy and vision.
- Collaborate across teams to determine public relations calendar.
- Determine ways to drive traffic to site to support various intiatives.
- Manage and mentor members of the team to grow in their public relations careers.
- Responsible for ideation of public relations activities, OKRs, and reporting on results.

#### Requirements

- 8+ years experience in public relations
- Experience in enterprise software or developer public relations.
- Experience managing PR agencies and driving results.
- Experience managing a team of public relatiions professionals.

### Specialty
Read more about what a [specialty](https://about.gitlab.com/handbook/hiring/#definitions) is at GitLab here.

### Content
The content specialty covers developing and overseeing the public relations content strategy, which includes contributed articles and award submissions. This role also ensures the GitLab Press Page, Press Kit and Handbook pages are updated regularly.

### Product
This specialty maps to the [Product Marketing](https://about.gitlab.com/handbook/marketing/product-marketing/) department at GitLab. The product specialty covers developing and overseeing the public relations product strategy, which includes promotion of releases, developing product storylines for press engagements, and product awards submissions. Additionally, this role is responsibile for ensuring partner and channel storylines are integrated for a wholistic GitLab offering view.

### Events
This specialty maps to the [Field Marketing](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/) and [Corporate Event](https://about.gitlab.com/handbook/marketing/corporate-marketing/#corporate-events) departments at GitLab. The event specialty covers developing and overseeing the public relations strategy for 3rd party tradeshows and GitLab hosted-events.

## Career Ladder

The next step in the Public Relations job family is not yet defined at GitLab. 
