---
layout: job_family_page
title: Sales Communications Manager
---

The Sales Communications Manager is a key role within GitLab’s Field Operations team and is responsible for creating effective, timely, and easily consumable communications with Sales and Customer Success audiences. The role requires excellent written and verbal communication skills, the ability to meet deadlines while juggling multiple projects, and effective collaboration with multiple stakeholders.

## Job Grade 

The Sales Communications Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).


## Responsibilities
- Develop and manage effective sales enablement communication strategies and tactics including but not limited to newsletters, videos, memos, presentations, and more
- Collaborate and partner with GitLab Field Operations leaders and Sales and Customer Enablement team members to build and execute holistic, cross-program communications plans in support of prioritized field enablement-related initiatives
- Champion efforts to improve sales enablement communications via [GitLab’s handbook-first approach to learning and development](https://about.gitlab.com/handbook/sales/field-operations/field-enablement/#handbook-first-approach-to-gitlab-learning-and-development-materials)
- Develop and implement a strategy for soliciting feedback from GitLab Sales and Customer Success team members to inform enablement priorities and requirements
- Assist in communication of strategies or messages from senior leadership as needed
- Take on additional projects and responsibilities as needed

## Requirements
- BS/BA in communications or relevant field
- Proven experience as a communications specialist, preferably with high-tech B2B sales audiences
- Experience in copywriting and editing
- Strong project management skills and attention to detail needing minimal supervision
- Proven experience developing and executing effective sales newsletters is a huge plus
- Experience with Mailchimp (or a similar marketing/communications platform) is preferred
- Working knowledge of Google docs; photo and video-editing software is an asset
- Excellent communication (oral and written) and presentation skills
- Outstanding organizational and planning abilities
- Excellent team player and ability to effectively collaborate with others
- Experience in web design and content production is a plus
- Knowledge of the software development life cycle, DevOps, and/or open source software is preferred
- Ability to use GitLab

## Performance Indicators
- Sales communication/newsletter open and click-through rates
- Field survey response rates
- Feedback from key stakeholders

## Career Ladder 

The next step in the Sales Communications Manager job family is not yet defined at GitLab. 

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
1. Phone screen with a GitLab Talent Acquisition team member
1. Zoom videoconference interviews with members of the [GitLab Field Operations team](/company/team/?department=field-operations)
1. Final Zoom videoconference interview(s) with the Director of Sales and Customer Enablement and the VP of Field Operations
Additional details about our process can be found on our [hiring page](/handbook/hiring).
