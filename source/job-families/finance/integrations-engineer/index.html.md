---
layout: job_family_page
title: "Integrations Engineering"
---
The Integrations Engineering job family is responsible for ownership and delivery of the core services and APIs that serve as an enterprise data backbone, using an array of different platforms, APIs, databases, protocols and data formats to ensure that the data and the business processes that intersect in the Enterprise Applications Ecosystem are as efficient and high fidelity as possible. Integrations Engineering works to align Gitlab's Enterprise Architecture by building and maintaining key integrations that connect the GitLab Enterprise Application Ecosystem, from the GitLab product ecosytem to cloud systems.

## Integrations Engineer

The Integrations Engineer develops core services and APIs that serve as an enterprise data backbone, identifying elegant solutions to complex workflows.

### Job Grade

The Integrations Engineer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities

- Design, develop, and maintain integration flows using an array of different platforms, APIs, databases, protocols and data formats
- Collaborate and review code with other developers in engineering and sales operations to ensure each integration maintains a consistent level of technical standards as set by the team
- Document technical details clearly to various stakeholders of different technical expertise (VP of IT, Sales Operations, Software Engineer)
- Develop integrations that are designed and implemented as reusable building blocks as appropriate to allow for more efficient integrations of a similar type
- Work directly with Business Systems Analysts to ensure that the data and the business processes that intersect in the Enterprise Applications ecosystem are as efficient and high fidelity as possible.

## Requirements

- Ability to use GitLab
- BA/BS degree
- 2+ years of application integration experience
- Write well structured, quality code that’s easily maintainable by others.
- Write code that interacts with various HTTP-based API’s, primarily REST-based.
- Understand basics of SQL and relational databases.
- Proactive doer and communicator.
- SaaS and B2B experience preferred
- Interest in GitLab and open source software
- You share our values and work in accordance with those values
- Ability to thrive in a fully remote organization

## Senior Integrations Engineer

The Senior Integrations Engineer is the owner of core services and APIs that serve as an enterprise data backbone, identifying elegant solutions to complex workflows.

### Job Grade

The Senior Integrations Engineer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities

- Design, develop, and maintain integration flows using an array of different platforms, APIs, databases, protocols and data formats
- Collaborate and review code with other developers in engineering and sales operations to ensure each integration maintains a consistent level of technical standards as set by the team
- Communicate and document technical details clearly to various stakeholders of different technical expertise (VP of IT, Sales Operations, Software Engineer)
- Ensure that integrations are designed and implemented as reusable building blocks as appropriate to allow for more efficient integrations of a similar type
- Work directly with Business Systems Analysts to ensure that the data and the business processes that intersect in the Enterprise Applications ecosystem are as efficient and high fidelity as possible.
- Align the Enterprise Architecture by building and maintaining key integrations that connect the GitLab Enterprise Application Ecosystem, from product ecosytem to cloud systems.

## Requirements

- Ability to use GitLab
- BA/BS degree
- 5+ years of application integration experience
- Write well structured, quality code that’s easily maintainable by others.
- Write code that interacts with various HTTP-based API’s, primarily REST-based.
- Understand basics of SQL and relational databases.
- Proactive doer and communicator.
- SaaS and B2B experience preferred
- Interest in GitLab and open source software
- You share our values and work in accordance with those values
- Ability to thrive in a fully remote organization

## Nice to Have

- MacOS experience
- Google Suite experience
- A love of open source
- Experience with SaaS products
- Experience using GitLab/Git

## Performance Indicators

- [Evaluating System or Process Efficiency](/handbook/business-ops/metrics/#evaluating-system-or-process-efficiency)
- [Average Merge Request](/handbook/business-ops/metrics/#average-merge-request)
- Evaluating health of integrations
- Number of integrations implemented that increase the efficiency of the company
- Total hours saved by improving systems integrations
- Number of reference architecture designs
- Ensure that architecture related decisions follow the different department roadmaps  

## Hiring Process

- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to a 45 minute interview with a senior member of the team.
- Second round interviews will involve interviews with 2 members of the Data Team and/or others.
- Third round interview is with the Senior Director of Enterprise Applications.
- At final stage candidates will meet with the VP of Information Technology.

- Additional details about our process can be found on our [hiring page](/handbook/hiring).

### Career Ladder

The next step in the Integration Engineering job family is to move a higher level IC role which is not yet defined at GitLab. 
