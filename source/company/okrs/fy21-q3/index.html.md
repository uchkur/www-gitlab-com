---
layout: markdown_page
title: "FY21-Q3 OKRs"
description: "View GitLabs Objective-Key Results for FY21 Q3. Learn more here!"
canonical_path: "/company/okrs/fy21-q3/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from August 1, 2020 to October 31, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2020-06-30 | CEO shares top goals with E-group for feedback |
| -4 | 2020-07-06 | CEO pushes top goals to this page |
| -3 | 2020-07-13 | E-group pushes updates to this page |
| -2 | 2020-07-20 | E-group 50 minute draft review meeting |
| -2 | 2020-07-22 | E-group discusses with teams |
| -1 | 2020-07-27 | CEO reports give a How to Achieve presentation. Pre-Meeting and How to Achieve Presentation recordings are added to this page by EBAs |
| 0  | 2020-08-03 | CoS updates OKR page for current quarter to be active |
| +2 | 2020-08-17 | Review previous and next quarter during the next Board meeting |

## OKRs

### 1. CEO: IACV
[Epic 720](https://gitlab.com/groups/gitlab-com/-/epics/720)
1. **CEO KR:** Achieve a [CAC Revenue Ratio](https://about.gitlab.com/handbook/sales/#cac-to-revenue-payback-ratio) < $X
    1. **CFO**: Drive strategic alignment through financial insights. [Epic 799](https://gitlab.com/groups/gitlab-com/-/epics/799)
    1. **CRO**: Deliver three pilot programs to increase iACV / decrease Sales and Marketing expense. [Epic 797](https://gitlab.com/groups/gitlab-com/-/epics/797)
    1. **CMO**: Segment our database. [Epic 1331](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1331)
1. **CEO KR:** Achieve new logo targets for each of the three segmetns (link to goals)
    1. **CRO**: Achieve new logo targets for each of the three segments. [Epic 808](https://gitlab.com/groups/gitlab-com/-/epics/808)
    1. **CFO**: Drive strategic alignment through financial insights. [Epic 799](https://gitlab.com/groups/gitlab-com/-/epics/799)
    1. **CMO**: Refresh about.gitlab.com. [Epic 1330](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1330)
3. **CEO KR:** Double down on what works in marketing.
    1. **CMO**: ROI analysis of FY20 marketing spend. [Epic 1332](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1332)
4. **EVP of Engineering:** [Run GitLab.com more efficiently](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8303)
5. **Senior Director, People Success** [5,000 all remote certifications taken from leads at enterprise companies](https://gitlab.com/groups/gitlab-com/people-group/-/epics/38)
1. **CLO - Contracts:** Create and launch “Learning GitLab Legal”. [KRs in Epic 738](https://gitlab.com/groups/gitlab-com/-/epics/738)

### 2. CEO: Popular next generation product
[Epic 722](https://gitlab.com/groups/gitlab-com/-/epics/722)
1. **CEO KR:** Continue winning against Microsoft Azure GitHub. Improve our competitive win rate from X to Y.
    1. **CRO**: Enable top competitor GTM competitive initiatives. [Epic 809](https://gitlab.com/groups/gitlab-com/-/epics/809)
    1. **EVP, Product:** Web and Git response time performance for GitLab.com as fast as GitHub.com [KR's in Epic 732](https://gitlab.com/groups/gitlab-com/-/epics/732)
    1. **CMO**: Launch main competitor strategy across GitLab. [Epic 1333](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1333)
    1. **CLO - Privacy:** Implement New Privacy Specific Training Module [KRs in Epic 741](https://gitlab.com/groups/gitlab-com/-/epics/741)
2. **CEO KR:** Drive total monthly active users, [TMAU](https://about.gitlab.com/handbook/product/performance-indicators/#total-monthly-active-users-tmau) and [Paid TMAU](https://about.gitlab.com/handbook/product/performance-indicators/#paid-total-monthly-active-users-paid-tmau), from X to Y.
    1. **EVP, Product:** Increase TMAU by 17% and Paid TMAU from X to Y to help drive paid conversion [KR's in Epic 731](https://gitlab.com/groups/gitlab-com/-/epics/731)
    1. **VP, Product:** Shift R&D organization to become Pajamas First to improve efficiency and user experience [KR's in Epic 734](https://gitlab.com/groups/gitlab-com/-/epics/734)
3. **CEO KR:** Complete three key packaging projects.
    1. **EVP, Product**:  Complete mission critical packaging projects to help improve product margins [KR's in Epic 733](https://gitlab.com/groups/gitlab-com/-/epics/733)
4. **EVP of Engineering**: [Increase dogfooding, performance, and productivity](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8303)
5. **Senior Director, People Succes** [100% of GitLab team members are certified in how to use GitLab](https://gitlab.com/groups/gitlab-com/people-group/-/epics/39)

### 3. CEO: Great team
[Epic 721](https://gitlab.com/groups/gitlab-com/-/epics/721)
1. **CEO KR:** Exceed 50% top of funnel from outreach and 90% of outreach to candidates from [underrepresented groups](/handbook/incentives/#add-on-bonus-for-select-underrepresented-groups).
    1. **CFO**: Institute public company financial reporting standards. [Epic 800](https://gitlab.com/groups/gitlab-com/-/epics/800)
    1. **Senior Director, People Success** [Support an environment where all team members feel they belong, are valued, and can contribute; understand the key drivers for team member engagement and continuously improve.](https://gitlab.com/groups/gitlab-com/people-group/-/epics/40)
    1. **VP of Recruiting** [Implement Phase II of Outbound Recruiting model](https://gitlab.com/groups/gitlab-com/people-group/-/epics/42)
    1. **People Group Leadership** [Increase diversity and sense of belonging across the company](https://gitlab.com/groups/gitlab-com/people-group/-/epics/41)
2. **CEO KR:** Certify 25 non-engineering team members in self-serve data via handbook first materials
    1. **CFO**: Deliver handbook first training and Self-Serve Data capabilities for product geolocation analysis and paying customer firmographic analysis. [Epic 801](https://gitlab.com/groups/gitlab-com/-/epics/801)
3. **CEO KR:** Certify 25% of people leaders through manager enablement certification. Epic
4. **EVP of Engineering:** [Expand the capabilities of our team](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8303)
1. **CLO - Contracts:** Develop knowledge base and playbook related to legal engagement with respect to GitLab procurement.[KRs in Epic 737](https://gitlab.com/groups/gitlab-com/-/epics/737)
1. **CLO - Employment:** Update onboarding process [KRs in Epic 745](https://gitlab.com/groups/gitlab-com/-/epics/745)
1. **CLO - Corporate:** Institute a bounty program for GitLab Team Members to locate and identify edits needed in the Handbook [KRs in Epic 744](https://gitlab.com/groups/gitlab-com/-/epics/744)


## Pre-Meeting Recordings

* Engineering
* Finance
* Legal
* Marketing
* People
* Product
* Sales

## How to Achieve Presentations

* [Engineering](https://youtu.be/-H6H4cEt-Os)
* [Finance](https://www.youtube.com/watch?v=QWzfeivf33o&feature=youtu.be)
* Legal
* [Marketing](https://youtu.be/CVHV2I-zIuQ)
* [People](https://youtu.be/c9ljAvBMzTk)
* [Product](https://youtu.be/peoaBL4hbiI)
* [Sales](https://youtu.be/JAZoxbAsrDQ)
