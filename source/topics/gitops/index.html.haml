---
layout: default
title: "What is GitOps?"
description: "GitOps is a process of automating IT infrastructure using infrastructure as code and software development best practices such as Git, code review, and CI/CD pipelines."
canonical_path: "/topics/gitops/"
suppress_header: true
extra_css:
  - devops.css
  - agile-delivery.css
extra_js:
  - libs/on-scroll.js
  - in-page-nav.js
  - all-clickable.js
---
.blank-header
  = image_tag "/images/home/icons-pattern-left.svg", class: "image-border image-border-left", alt: "Gitlab hero border pattern left svg"
  = image_tag "/images/home/icons-pattern-right.svg", class: "image-border image-border-right", alt: "Gitlab hero border pattern right svg"
  .header-content
    = image_tag "/images/topics/gitops-header.png", class: "hero-image-huge", alt: "GitLab git ops graphic"
    %h1 GitOps
    %a.btn.cta-btn.orange.devops-cta{ href: "/why/gitops-infrastructure-automation/" } Watch our GitOps webcast

.wrapper.wrapper--large-fonts
  #content.devops-content.u-margin-top-md
    .wrapper.container.js-in-page-nav-group{role: "main"}
      .row.u-margin-top-lg.js-in-page-nav-section#what-is-gitops
        .col-md-10.col-md-offset-1
          %h2.u-text-brand What is GitOps?

          %p  GitOps is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation.

          %p Modern applications are developed with speed and scale in mind. Organizations with <a href="/devops/" target="_blank">a mature DevOps culture</a> can deploy code to production hundreds of times per day. DevOps teams can accomplish this through development best practices such as version control, code review, and CI/CD pipelines that automate testing and deployments.

          %p While <a href="/stages-devops-lifecycle/" target="_blank">the software development lifecycle</a> has been automated, infrastructure has remained a largely manual process that requires specialized teams. With the demands made on today's infrastructure, it's becoming increasingly crucial to implement infrastructure automation. Modern infrastructure needs to be elastic so that it can effectively manage cloud resources that are needed for frequent deployments. 

          %p GitOps is an operational framework that can automate this process of provisioning infrastructure. Similar to how we use application source code, infrastructure teams that practice GitOps use configuration files stored as code (infrastructure as code). GitOps configuration files generate the same infrastructure environment every time it’s deployed, just as application source code generates the same application binaries every time it's built.

      .row.u-margin-top-lg.js-in-page-nav-section#gitops-overview
        .col-md-10.col-md-offset-1
          %h2.hidden Overview
          %h2.u-text-brand GitOps overview

          %p GitOps is an operational framework that takes DevOps best practices used for application development such as version control, code review, collaboration, compliance, and CI/CD, and applies them to infrastructure automation.

          %h3 How do teams put GitOps into practice?

          %p GitOps is not a single product, plugin, or platform. GitOps is a framework that helps teams manage IT infrastructure through processes they already use in application development. To put GitOps into practice, it needs three core components:

          %blockquote.blockquote-alt.cicd-blockquote
            GitOps = IaC + MRs + CI/CD

          %p IaC - GitOps uses a <a href="/blog/2020/04/20/ultimate-git-guide/" target="_blank">Git repository</a> as the single source of truth for infrastructure definitions. Git is an open source version control system that tracks code management changes, and a Git repository is a <code>.git</code> folder in a project that tracks all changes made to files in a project over time. Infrastructure as code (IaC) is the practice of keeping all infrastructure configuration stored as code. The actual desired state may or may not be not stored as code (e.g., number of replicas, pods).

          %p MRs - GitOps uses <a href="https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html" target="_blank">merge requests</a> (MRs) as the change mechanism for all infrastructure updates. The MR is where teams can collaborate via reviews and comments and where formal approvals take place. A merge commits to your master (or trunk) branch and serves as a changelog for auditing and troubleshooting.

          %p CI/CD - GitOps automates infrastructure updates using a Git workflow with <a href="/ci-cd/" target="_blank">continuous integration and continuous delivery</a> (CI/CD). When new code is merged, the CI/CD pipeline enacts the change in the environment. Any configuration drift, such as manual changes or errors, is overwritten by GitOps automation so the environment converges on the desired state defined in Git. GitLab uses CI/CD pipelines to manage and implement GitOps automation, but other forms of automation such as definitions operators can be used as well.

      .row.u-margin-top-sm.js-in-page-nav-section#benefits-of-gitops
        .col-md-10.col-md-offset-1
          %h2.hidden Benefits
          %h2.u-text-brand The benefits of GitOps

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px" }
                = image_tag "/images/topics/gitops-benefits-collaboration.png"
            .media-body.media-middle
              %h3.u-margin-top-0 Enables collaboration on infrastructure changes
              %p.u-margin-bottom-0 Since every change will go through the same change/merge request/review/approval process, senior engineers can focus on other areas beyond the critical infrastructure management.

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px;" }
                = image_tag "/images/topics/gitops-benefits-access-control.png"
            .media-body.media-middle
              %h3.u-margin-top-0 Improved access control
              %p.u-margin-bottom-0 There's no need to give credentials to all infrastructure components since changes are automated (only CI/CD needs access).

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px;" }
                = image_tag "/images/topics/gitops-benefits-market.png"
            .media-body.media-middle
              %h3.u-margin-top-0 Faster time to market
              %p.u-margin-bottom-0 Execution via code is faster than manual point and click. Test cases are automated and repeatable, so stable environments can be delivered rapidly.

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px" }
                = image_tag "/images/topics/gitops-benefits-risk.png"
            .media-body.media-middle
              %h3.u-margin-top-0 Less risk
              %p.u-margin-bottom-0 All changes to infrastructure are tracked through merge requests, and changes can be rolled back to a previous state.

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px" }
                = image_tag "/images/topics/gitops-benefits-costs.png"
            .media-body.media-middle
              %h3.u-margin-top-0 Reduced costs
              %p.u-margin-bottom-0 Automation of infrastructure definition and testing eliminates manual tasks, improves productivity, and reduces downtime due to built-in revert/rollback capability. Automation also allows for infrastructure teams to better manage cloud resources which can also improve cloud costs. 

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px" }
                = image_tag "/images/topics/gitops-benefits-error.png"
            .media-body.media-middle
              %h3.u-margin-top-0 Less error prone
              %p.u-margin-bottom-0 Infrastructure definition is codified and repeatable, making it less prone to human error. With code reviews and collaboration in merge requests, errors can be identified and corrected long before they make it to production.


      .row.u-margin-top-sm.js-in-page-nav-section#gitops-challenges
        .col-md-10.col-md-offset-1
          %h2.hidden Challenges
          %h2.u-text-brand GitOps challenges

          %p With any collaborative effort, change can be tricky and GitOps is no exception. GitOps is a process change that will require discipline from all participants and a commitment to doing things in a new way. It is vital for teams to write everything down.

          %p GitOps allows for greater collaboration, but that is not necessarily something that comes naturally for some individuals or organizations. A GitOps approval process means that developers make changes to the code, create a merge request, an approver merges these changes, and the change is deployed. This sequence introduces a “change by committee” element to infrastructure, which can seem tedious and time-consuming to engineers used to making quick, manual changes.

          %p It is important for everyone on the team to record what’s going on in merge requests and issues. The temptation to edit something directly in production or change something manually is going to be difficult to suppress, but the less “cowboy engineering” there is, the better GitOps will work.

      .row.u-margin-top-sm.js-in-page-nav-section#gitops-challenges
        .col-md-10.col-md-offset-1
          %h2.hidden What makes it work
          %h2.u-text-brand What makes GitOps work?

          %p As with any emerging technology term, GitOps isn't strictly defined the same way by everyone across the industry. GitOps principles can be applied to all types of infrastructure automation including VMs and containers, and can be very effective for teams looking to manage <a href="/solutions/kubernetes/" target="_blank">Kubernetes clusters</a>.

          %p While many tools and methodologies promise faster deployment and seamless management between code and infrastructure, GitOps differs by focusing on a developer-centric experience. Infrastructure management through GitOps happens in the same version control system as the application development, enabling teams to collaborate more in a central location while benefiting from all the <a href="https://devops.com/an-inside-look-at-gitops/" target="_blank">built-in features of Git</a>.

      .row.u-margin-top-lg.js-in-page-nav-section#resources
        .col-md-10.col-md-offset-1

          %h2.hidden Resources
          %h2.u-text-brand GitOps resources

          %p Here’s a list of resources on GitOps that we find to be particularly helpful in understanding version control and implementation. We would love to get your recommendations on books, blogs, videos, podcasts and other resources that tell a great version control story or offer valuable insight on the definition or implementation of the practice.

          %p
            Please share your favorites with us by tweeting us
            %a{:href => 'https://twitter.com/gitlab'} @GitLab!

          .feature-group.feature-group--alt.u-margin-top-lg.u-padding-top-0.u-padding-bottom-0
            .row.flex-row
              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/topics/gitops-blog-ansible.jpg"
                  .feature-body
                    %h3.feature-title Infrastructure as Code using GitLab & Ansible
                    %p.feature-description Explore the power of GitLab CI as we demo Ansible playbooks in infrastructure as code.
                    = link_to "Read", "/blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/", class: "feature-more"

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/topics/gitops-blog-series.jpg"
                  .feature-body
                    %h3.feature-title Part 1 of 3: Why collaboration technology is critical for GitOps
                    %p.feature-description How GitLab can be the single source of truth for infrastructure and deployment teams.
                    = link_to "Read", "/blog/2019/11/04/gitlab-for-gitops-prt-1/", class: "feature-more"

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/topics/gitops-blog-series.jpg"
                  .feature-body
                    %h3.feature-title Part 2 of 3: Why collaboration technology is critical for GitOps
                    %p.feature-description How GitLab can be the single source of truth for infrastructure and deployment teams.
                    = link_to "Read", "/blog/2019/11/12/gitops-part-2/", class: "feature-more"

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/topics/gitops-blog-series.jpg"
                  .feature-body
                    %h3.feature-title Part 3 of 3: Why collaboration technology is critical for GitOps
                    %p.feature-description Why multi-cloud compatibility supports a clean GitOps workflow.
                    = link_to "Read", "/blog/2019/11/18/gitops-prt-3/", class: "feature-more"

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/topics/gitops-blog-workflow.jpg"
                  .feature-body
                    %h3.feature-title Optimize GitOps workflow with version control from GitLab
                    %p.feature-description A GitOps workflow improves development, operations and business processes and GitLab’s CI plays a vital role.
                    = link_to "Read", "/blog/2019/10/28/optimize-gitops-workflow/", class: "feature-more"

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/topics/gitops-blog-why.jpg"
                  .feature-body
                    %h3.feature-title Why GitOps should be the workflow of choice
                    %p.feature-description What is GitOps and how do you apply it in real-world applications?
                    = link_to "Read", "/blog/2020/04/17/why-gitops-should-be-workflow-of-choice/", class: "feature-more"


        .col-md-10.col-md-offset-1


          .resource-block.resource-block--webcasts
            %h3.block-title Webcasts

            %ul.resource-list.list-unstyled
              %li.resource-list-item
                %h4.resource-title
                  = link_to "[Live Panel Discussion] GitOps: The Future of Infrastructure Automation", "/why/gitops-infrastructure-automation/"
              %li.resource-list-item
                %h4.resource-title
                  = link_to "[Webcast - on demand] Managing infrastructure through GitOps with GitLab and Anthos", "/webcast/gitops-gitlab-anthos/"
              %li.resource-list-item
                %h4.resource-title
                  = link_to "[Webcast] GitLab and HashiCorp - A holistic guide to GitOps and the Cloud Operating Model", "/webcast/gitlab-hashicorp-gitops/"
              %li.resource-list-item
                %h4.resource-title
                  = link_to "[Virtual lab] Automating cloud infrastructure with GitLab and Terraform", "/webcast/gitops-gitlab-terraform/"
              %li.resource-list-item
                %h4.resource-title
                  = link_to "[Webcast - on demand] GitOps with Pulumi and GitLab", "/webcast/gitops-gitlab-pulumi/"
              %li.resource-list-item
                %h4.resource-title
                  = link_to "[Webcast - on demand] GitOps with AWS and GitLab", "/webcast/gitops-gitlab-aws-helecloud/"
              %li.resource-list-item
                %h4.resource-title
                  = link_to "[Webcast/video case study - on demand] Accelerating Digital Transformation at Northwestern Mutual", "/webcast/digital-transformation-northwestern-mutual/"


          .resource-block.resource-block--videos
            %h3.block-title Videos

            <iframe class="u-margin-bottom-md" width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL05JrBw4t0KoixDjLVMzKwVgx0eNyDOIo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            <iframe class="u-margin-bottom-md" width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL05JrBw4t0KoixDjLVMzKwVgx0eNyDOIo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            <iframe class="u-margin-bottom-md" width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLFGfElNsQthbno2laLgxeWLla48TpF8Kz" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
