---
layout: markdown_page
title: "Category Direction - Users"
description: "The concept of a user isn't a new one, but it touches a number of areas in GitLab that are critical to the success of our users. Learn more!"
canonical_path: "/direction/manage/users/"
---

- TOC
{:toc}

| Category Attribute | Link | 
| ------ | ------ |
| [Stage](https://about.gitlab.com/handbook/product/product-categories/#hierarchy) | [Manage](https://about.gitlab.com/direction/manage) | 
| [Maturity](/direction/maturity/#maturity-plan) | [Not applicable](#maturity) |
| Labels | [permissions](https://gitlab.com/groups/gitlab-org/-/epics?label_name=permissions), [user management](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=user%20management), [user management](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=user%20profile) |

## Users

The concept of a user isn't a new one, but it touches a number of areas in GitLab that are critical to the success of our users. Namely:

* Permissions: What users can do and the conditions they are allowed to do them under.
* User management: Creating and modifying users at scale.
* User profile: User settings and how a user's profile is shown to the instance/world.

## Target audience and experience

Any user of GitLab could be considered a relevant audience, but improvements in this area likely think about two specific instances:

1. Large EE instances: managing users and permissions is key. These are customers with large seat counts, who need the ability to understand who is using GitLab and who is not - and have the tools needed to manage these users at scale. They also need granular permissions settings - ideally by defining a general policy/template, but also by configuring permissions on a feature-by-feature basis.
1. GitLab.com: for individual contributors, the user profile becomes your identity as a developer. While concepts like the contribution graph or project list may not be of paramount importance to a self-managed enterprise user, individual developers on GitLab.com want their profile and presence on GitLab.com to represent them.

## Maturity

As much about users in GitLab are application-specific, users are considered a non-marketing category without a [maturity level](/direction/maturity/) that can be compared to other competing solutions.

## How you can help

As with any category in GitLab, it's dependent on your ongoing feedback and contributions. Here's how you can help:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/603).
1. Find issues in this category accepting merge requests. [Here's an example query](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=user%profile&label_name[]=Accepting%20merge%20requests)!

## What's next & why

### Current

Make it easy to identify and manage inactive users: one of the most common tasks for an administrator is understanding and identifying the activity levels of their users. Since GitLab does not use a named license model, it’s imperative that EE instances are able to use their license seats efficiently - this typically involves refining membership periodically and handling inactive users. 

We should make this easy for organizations. It should be straightforward and unambiguous to identify these users, and easy to take appropriate action.

Please see the [inactive user management](https://gitlab.com/groups/gitlab-org/-/epics/256) epic.

## Top user issue(s)

TBD

## Top internal customer issue(s)

TBD

## Top Vision Item(s)

TBD
