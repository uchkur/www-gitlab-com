--- 
title: New access privilege
wordpress_id: 119
wordpress_url: http://blog.gitlabhq.com/?p=119
date: 2011-12-07 14:25:25 +00:00
comments: false
categories: company
---
<h4>Now gitlab allow you to set website permissions &amp; git permissions separately.</h4>


Project access:

<ul>
	<li>denied - no access</li>
	<li>read - readonly mode</li>
	<li>report - create issues, comment, etc</li>
	<li>admin -  full project access</li>
</ul>


Repository access:

<ul>
	<li>denied - no access</li>
	<li>Pull - readonly mode</li>
	<li>PUll & Push - full access to git repo</li>
</ul>
