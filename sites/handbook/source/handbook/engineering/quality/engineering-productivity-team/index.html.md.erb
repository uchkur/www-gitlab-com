---
layout: handbook-page-toc
title: "Engineering Productivity team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Common Links

| **GitLab Team Handle** | [`@gl-quality/eng-prod`](https://gitlab.com/gl-quality/eng-prod) |
| **Slack Channel** | [#g_qe_engineering-productivity](https://gitlab.slack.com/archives/CMA7DQJRX) |
| **Team Boards** | [Team Board](https://gitlab.com/groups/gitlab-org/-/boards/978615) & [Priority Board](https://gitlab.com/groups/gitlab-org/-/boards/1333450) |
| **Issue Tracker** | [quality/team-tasks](https://gitlab.com/gitlab-org/quality/team-tasks/issues/) |

## Areas of Responsibility

* Increase contributor and developer productivity by making measurement-driven improvements to the development tools / workflow / processes, then monitor the results, and iterate.
  * Identify and implement quantifiable improvement opportunities with proposals and hypothesis for metric improvements.
  * Automated [merge request quality checks](https://docs.gitlab.com/ee/development/dangerbot.html) and [code quality checks](https://docs.gitlab.com/ee/development/contributing/style_guides.html).
  * [GitLab project pipeline](https://docs.gitlab.com/ee/development/pipelines.html) improvements to improve efficiency, quality or duration.
* Build automated measurements and dashboards to gain insights into the productivity of the Engineering organization to identify opportunities for improvement.
  * Build automated [measurements and dashboards](#engineering-productivity-team-metrics) to gain insights into engineering productivity and bottlenecks.
  * Implement new measurements to provide visibility into improvement opportunities.
  * Collaborate with other Engineering teams to provide visualizations for measurement objectives.
* Dogfood GitLab product features to improve developer workflow and provide feedback to product teams.
  * Use new features from related product groups (Analytics, Monitor, Testing).
  * Improve usage of [Review Apps] for GitLab development and testing.
* Develop automated processes for improving label classification hygiene in support of product and engineering workflows.
  * Automated issues and merge requests triage.
  * Improvements to the labelling classification and automation used to support Engineering measurements.
  * See the [GitLab Triage], [GitLab triage operations], and [GitLab triage serverless] projects for examples.
* Participate in activities related to Engineering throughput and [Quality KPIs](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/)
  * [#master-broken](https://about.gitlab.com/handbook/engineering/workflow/#broken-master) pipeline monitoring
  * KPI corrective actions such as review app stabilization
  * Quality department pipeline on-call
  * [Merge Request coach](https://about.gitlab.com/job-families/expert/merge-request-coach/) for ~"Community Contribution" merge requests
* Help with maintaining [GitLab Docs]
  * Review new enhancements.
  * Contribute to organization of Engineering documentation.

## Team Members

<%= direct_team(manager_role: 'Backend Engineering Manager, Engineering Productivity') %>

## Work prioritization

The Engineering Productivity team uses [modified prioritization and planning guidelines](prioritization.html) for targeting work within a Milestone.

## Projects

1. [GitLab] CI Pipeline configuration optimization and stability.
1. [GitLab] [Review apps] provisioning.
1. [GitLab Insights] (native) and [Development department metrics](/handbook/engineering/development/performance-indicators/groups) for measurements of Quality and Productivity.
1. [GitLab triage operations] for issues, merge requests, community contributions.
1. [GitLab Triage] engine maintenance.
1. [GitLab Docs] (to be moved to a dedicated team in UX)

## Engineering productivity team metrics

The Engineering Productivity team creates metrics in the following sources to aid in operational reporting.

- [Sisense Quality Engineering KPIs](https://app.periscopedata.com/app/gitlab/516343/Quality-KPIs)
- [Sisense Engineering Productivity Sandbox](https://app.periscopedata.com/app/gitlab/496118/Engineering-Productivity-Sandbox)
- [Sisense Engineering Productivity Pipeline](https://app.periscopedata.com/app/gitlab/564156/Engineering-Productivity---Pipeline)
- [Sisense Engineering Productivity Pipeline Durations](https://app.periscopedata.com/app/gitlab/652085/Engineering-Productivity---Pipeline-Build-Durations)
- [Sisense GitLab Issue Triage Dashboard](https://app.periscopedata.com/app/gitlab/621211/WIP:-GitLab-Issue-Triage-Dashboard)
- [GitLab-Org Native Insights](https://gitlab.com/groups/gitlab-org/-/insights)
- [Review Apps monitoring dashboard](https://app.google.stackdriver.com/dashboards/6798952013815386466?project=gitlab-review-apps)
  - [Failed deployment Tiller logs](https://console.cloud.google.com/logs/viewer?authuser=0&interval=P1D&project=gitlab-review-apps&minLogLevel=0&expandAll=false&timestamp=2019-10-11T03:56:06.077000000Z&customFacets=&limitCustomFacetWidth=true&resource=k8s_cluster&advancedFilter=resource.type%3D%22k8s_container%22%0AlogName%3D%22projects%2Fgitlab-review-apps%2Flogs%2Fstderr%22%0Aresource.labels.project_id%3D%22gitlab-review-apps%22%0Aresource.labels.location%3D%22us-central1-b%22%0Aresource.labels.cluster_name%3D%22review-apps-ee%22%0Aresource.labels.namespace_name%3D%22review-apps-ee%22%0Aresource.labels.pod_name:%22tiller-deploy%22%0AtextPayload:(%22%5Btiller%5D%22%20AND%20(%22warning:%20Upgrade%22%20OR%20%22warning:%20Release%22))&scrollTimestamp=2019-10-11T02:10:10.956872965Z&dateRangeStart=2019-10-10T03:58:45.648Z&dateRangeEnd=2019-10-11T03:58:45.648Z)
- [Deprecated Quality Dashboard](https://quality-dashboard.gitlap.com/groups/gitlab-org)

### Product MRs merged per Product team member per month

This metric captures how many Merge Requests are merged into customer facing projects divided by the number of product team members.

Customer facing projects are projects which would come with an installation of GitLab. The current list can be found with all the projects identified in the [`projects_part_of_product.csv`](https://gitlab.com/gitlab-data/analytics/blob/master/transform%2Fsnowflake-dbt%2Fdata%2Fprojects_part_of_product.csv) file in the [`gitlab-data/analytics`](https://gitlab.com/gitlab-data/analytics) project.

Product team member is defined as all non-VP or Director role in Development, UX, Quality, and Product Management.

#### Requesting new projects to be included

With this measure, there is a preference towards adding new projects into the existing product. In the event that a new GitLab project is included in the product that a customer would receive then please open an issue [on the quality team tasks project](https://gitlab.com/gitlab-org/quality/team-tasks/issues/new).

Within the issue, please add details on how this project is included with an Omnibus or Cloud Native installation of GitLab for consideration by the Engineering Productivity team. The team will evaluate the request and make any updates to charts and metrics in the sources listed above.

## Communication guidelines

The Engineering Productivity team will make changes which can create notification spikes or new behavior for
GitLab contributors. The team will follow these guidelines in the spirit of [GitLab's Internal Communication Guidelines](/handbook/communication/#internal-communication).

### Pipeline changes

For changes which dogfood new pipeline features or changes to the pipeline flow, the team will
communicate on the company call, add to the Engineering Week in Review, and [#development](https://gitlab.slack.com/messages/C02PF508L) in Slack.

For all other pipeline changes, the team will add to the Engineering Week in Review and
[#development](https://gitlab.slack.com/messages/C02PF508L) in Slack.

### Automated triage policies

Communication guidelines are documented on the [triage-ops handbook page](/handbook/engineering/quality/triage-operations/#communicate-early-and-broadly-about-expected-automation-impact)

[GitLab]: https://gitlab.com/gitlab-org/gitlab
[GitLab Insights]: https://gitlab.com/gitlab-org/gitlab-insights
[GitLab Triage]: https://gitlab.com/gitlab-org/gitlab-triage
[GitLab triage operations]: https://gitlab.com/gitlab-org/quality/triage-ops
[Review apps]: https://docs.gitlab.com/ee/development/testing_guide/review_apps.html
[GitLab triage serverless]: https://gitlab.com/gitlab-org/quality/triage-serverless
[GitLab docs]: https://gitlab.com/gitlab-org/gitlab-docs
