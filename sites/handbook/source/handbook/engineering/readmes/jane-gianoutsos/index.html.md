---
layout: markdown_page
title: "Jane Gianoutsos' README"
job: "Manager, Support Engineering (APAC)"
---

## Jane G's README

*In an effort to get this done, I'm practicing our value of 👣 [iteration](https://about.gitlab.com/handbook/values/#iteration). This is a first take and I welcome all feedback! You are very welcome to contribute to this page by opening a merge request.*

**Jane Gianoutsos - Manager, Support Engineering (APAC)** This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

You'll find as you read this I've made a clear decision to be very intentionally vulnerable.  Trust is earned, and I hope to offer you in this a basis for why you might trust me, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.  Come as you are, I take you as you come and hope you will do the same for me.

## About me 

I live in Auckland, New Zealand, with my husband, and the pleasure of myriad native New Zealand birds in our neighbourhood. 

As a kid I dreamt of working at NASA and as a teenager decided this meant becoming an astrophysicist. An uninspiring physics teacher and a revelation about how many years of study that would take put me off. An inspiring lecturer at university set me on the path of IT, and it feels like home. I love people and their stories and how they've journeyed to where they are and where they'll find themselves in their future.  I sometimes ponder doing an anthropology degree.

I'm a serial hobby-ist who loves learning new skills, but once it starts getting expensive (either financially or time-wise) to continue to learn, I tend to move to the next hobby.  See the [final section](#more-about-hobbies) if you want to know more!

## Transparency

I wear my heart on my sleeve, I'm very honest about what I struggle with, and I struggle to be as generous with myself about what I'm good (or great!) at.  Meanwhile, I love to encourage others - I'm passionate about the power of words and their impact on us. 

## Working together

What's going on in your life doesn't stop just because you're at work. Please don't feel you can't talk about something because it's 'not work related'. Life is messy, let's work together to make it a little easier.

I deeply respect your dignity and I count it a privilege that I get to do a little bit of life with you.  

I'm very at ease with emotion, I don't attach shame to tears, some things just need that release, it's incredibly healthy and part of being fully human. 

I don't like being expected to read between the lines, and I don't expect you to either. Please be as open and direct as you feel comfortable being and if you're struggling with that, let me know and we can work it out together. 

## What I assume about others

- honesty - I'll take you at your word
- a desire to contribute
- [positive intent](https://about.gitlab.com/handbook/values/#assume-positive-intent)
- you've got more going on than I know about or than you perhaps realise yourself

You can assume the same about me in all instances. 

## Communicating with me

- I'm not a morning person, I'm usually online around 8:30am NZT, but I block my calendar out for 30 minutes to give me time to be a bit more human and clear out overnight messages. I am still available at this time.
- I'm here to help - I won't always have the answers and I don't expect myself to (just as I don't expect that of you), but even if it feels off-topic, please reach out. 

---

As promised above!

## More about hobbies

A long while ago I did a [series of blog posts](https://nztebs.blogspot.com/search/label/Open%20Office%20Writer) on getting comfortable with using Open Office Write and a bit of OO Calc.

Recent hobbies these have included:

- fermented foods ([sauerkraut](https://www.instagram.com/p/B4EjBRdhSFc/), [sourdough](https://www.instagram.com/p/B8ahzBZpihR/) bread, kombucha, kefir water, [pickles](https://www.instagram.com/p/B4JopFphgmm/)), 
- [water color painting](https://www.instagram.com/p/BkZySmClad2/), 
- acrylic painting (I paint emotions, and my general guideline is "if my husband likes it, I'm not painting in my personal style" - it's a bit of a standing joke in my household!), 
- [calligraphy](https://www.instagram.com/p/BkNIQCjlXBc/), 
- book-binding, 
- [zentangle](https://www.instagram.com/p/BwBNKd3hidH/).  
- boardgames!

Further back hobbies included [crochet](https://www.flickr.com/photos/unplain-jane/albums/72157608410004917), [sewing](https://www.flickr.com/photos/unplain-jane/albums/72157622880198139), [New Zealand Sign Language](https://www.nzsl.nz/) (sadly I've forgotten most of what I learnt),  [photography](https://www.flickr.com/photos/unplain-jane/albums/72157616370056642).

Way back hobbies included horse-riding, singing, playing guitar, Hamilton Community Gospel Choir, softball, Girl Guides (yep, that far back!)

