---
layout: handbook-page-toc
title: "GitLab Plato HQ Mentoring Program"
---

## Program Overview
GitLab has partnered with [Plato HQ](https://www.platohq.com/) for an external Mentoring Program. In this program GitLab team members select Mentors external to GitLab.  Some of the other Mentoring programs we have here at GitLab are internal to GitLab.  [Minorities in Tech](/company/team/structure/working-groups/mit-mentoring/) and [Women in Sales](/handbook/people-group/women-in-sales-mentorship-pilot-program/) are both made up of GitLab Mentors and Gitlab Mentees.  The external mentoring is what makes this approach to GitLab unique.

For more information on mentoring best practice, visit [Mentoring](https://about.gitlab.com/handbook/engineering/career-development/mentoring/).

## How does it work?

1. A GitLab Team Member is recommended by their Manager to participate in the program.
2. All Mentees are invited to a KickOff Meeting with Plato
3. Mentees select one Mentor after viewing three Mentor Plato Profiles while on a Plato Call 
4. Plato sets up the framework / software to support the Mentoring Relationship
     * Plato schedules meetings and Zoom sessions (that are recorded) 
     * Plato invites Mentees to Seminars where you hear guest speakers from other companies
5. Mentees and Mentors meet (average meeting time is 30 minutes) on a regular cadence (typically bi-weekly) based on their preferences
6. Mentees create all of the Agenda and are the main drivers of the relationship

## Benefits

This program offers the following benefits:
* Serves as a Learning Opportunity for participants
* Beneficial to get an external (to GitLab) perspective to approaching challenging problems
* Employee Development support customized to your specific needs (individual areas of improvement)


## Time Commitment

These are the activities that a Mentee would perform and the time to perform these activities could vary from person to person:
* Setting up the Agenda for the meetings
* Attending the meetings
* Taking time to reflect on areas that you would like to grow or increase/improve skills
* Implementing any recommendations, suggestions made by your Mentor

