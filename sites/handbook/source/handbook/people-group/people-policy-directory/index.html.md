---
layout: handbook-page-toc
title: "GitLab People Policy Directory"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab People Policy Directory

All of the policies listed below are important for GitLab team members to read and understand as they deal with people benefits, procedures, and requirements of the company. If you have any questions around the internal policies, please reach out to People Operations.

### Workers' Compensation

If you have been injured at work, please contact Total Rewards Analysts to determine what your benefits are.

### Military Leave

GitLab is committed to protecting the rights of team members absent on military leave. No team member or prospective team member will be subjected to any form of discrimination on the basis of membership in or obligation to perform service for any of the uniformed services of their country of residency. If any team member believes that he or she has been subjected to discrimination in violation of this policy, immediately contact People Business Partners for assistance. For any questions about how to initiate a military leave, please contact People Operations.

### Hiring Significant Others or Family Members

GitLab is committed to a policy of employment and advancement based on **qualifications and merit** and does not discriminate in favor of or in opposition to the employment of significant others or family members. Due to the potential for perceived or actual conflicts, such as favoritism or personal conflicts from outside the work environment, which can be carried into the daily working relationship, GitLab will hire or consider other employment actions concerning significant others and/or family members of persons currently employed or contracted only if **all** points below are true:

* Candidates for employment or current team members will not be working directly for or supervising a significant other or family member.
* Candidates for employment or current team members will not occupy a position in the same line of authority in which team members can initiate or participate in decisions involving a direct benefit to the significant other or family member. Such decisions include hiring, retention, transfer, promotion, wages, and leave requests.
* The team member does not have access to sensitive company and/or personal information including payroll, compensation, personnel reviews/files, job performance, etc., unless the team member's job duties require access to such information.  If the position in question requires access to such information, the team member in that position shall comply with the duty to maintain confidentiality as governed by applicable law and policy, and must seek approval from the team member's direct manager for any decisions affecting the significant other or family member's personnel information.  

This policy applies to all current employees and candidates for employment. In the spirit of our [Transparency value](https://about.gitlab.com/handbook/values/#transparency), we ask the team member and the candidate to disclose the family relationship to the recruiter at the beginning of the hiring process. For purposes of this policy, a significant other or family member is any person who has a relation by blood, marriage, adoption, or domestic partnership within the third degree of our team member. If the candidate progresses through the process as a final candidate, and prior to any offer, the recruiter should notify the hiring manager and the People Business Partner of the family relationship to ensure there is not a conflict of interest.  In addition, the GitLab team member should not be part of their family member's interview process. If there is a concern that either the team member or the candidate may progress to a position that would trigger one of the conditions above a waiver will be signed by both parties acknowledging that future work assignments, promotions, etc may be impacted by enforcement of this policy. Please report any relationship with a significant other or family member to your People Business Partner, if you find yourself in a reporting relationship with the significant other or family member.  Furthermore, if two team members who are in a reporting relationship become significant others or family members in the course of their employment, they should also report the relationship to the People Business Partner.  Transfers, promotions, and future work assignments will be made in accordance with all applicable anti-discrimination laws and policies.

### Employee Safety

**Preventing Unsafe Situations**

While GitLab is 100% remote, there may be times when employees travel for work related functions or co-working events.  GitLab is committed to the value of a safe, violence-free work environment and ensuring and exhibiting equal commitment to the safety and health of employees.

In general, please consider the following recommendations to ensure safety when traveling or coworking:

1. Do the research.  Have some familiarity with the destination before you arrive.  Check with your country's government department that provides advice for traveling overseas:
  * United States citizens: check the State Department's website for country updates and enroll in a Traveler Program such as Smart Traveler Enrollment Program (STEP) [https://step.state.gov/step/](https://step.state.gov/step/).
  * New Zealand citizens: [https://safetravel.govt.nz/](https://safetravel.govt.nz/).
1. Try not to draw attention.  People who appear to be from out of town are more vulnerable to crimes.  Try to respect the culture you are visiting by blending in. Consider protective clothing to avoid pickpockets or other theft. Do not flash money or credit cards unnecessarily.
1. Make copies of important documents.  Consider carrying hard copies of important documents (passport, driver's license) in a separate location in the event your documents are misplaced or stolen.
1. Keep friends and family updated. No matter whether you’re going on an overnight jaunt or a week-long international journey, it’s always a good idea to let friends or family know your plans. Before you leave, send a copy of your itinerary to a few trusted people who can keep tabs on your whereabouts. Check in regularly with your contacts so they know you’re where you’re supposed to be.
1. Be wary of public Wi-Fi.  Be aware that hackers can steal sensitive information in the public forum.  Use a VPN or other secure access if you plan to access sensitive data. More information on [VPN usage in GitLab](/handbook/security/#why-we-dont-have-a-corporate-vpn)
1. Safeguard your hotel.  Lock and deadbolt the door while you are in the room.  Ensure the door is locked when you leave.  Keep the windows closed. Try to give the impression that you’re in your room even when you’re away, such as placing the Do Not Disturb sign on the outside of your door and keeping the blinds or windows closed.  Don’t let any strangers into your room, even if they say they work for the hotel. You can always call the front desk to check whether someone was ordered by hotel staff to come to your room.
1. Be aware of your surroundings.  Always keep an eye on your personal belongings and use good judgment when talking to strangers. A big part of the joy of traveling is the opportunities it affords to meet new people and learn about their cultures. But if someone near you is acting suspiciously, or if you feel uncomfortable, leave the area immediately.  Trust your instincts.
1. Adhere to any recommended safety recommendations made by the GitLab group. For all large self-hosted events we (jointly completed by our internal security team and our contracted security agency) will do a full risk assessment  before we converge. It will be up to employees to read said risk assessment and adhere to recommendations outlined.
1. If you are sick please do not come or participate in person workplace activities. This is for your safety and for others. We recommend GitLabbers not travel while sick.

**Measures GitLab Takes to Aid Employee Health and Safety**
1.	Hand sanitizers placed around venue of live events or attendees given hand sanitizer.
1.	Employees can expense masks for travelling if suggested in risk assessment outlined above.
1.	Sick employees should expense mask if flying.
1.	If health risk is considered high, all food to be served by food health professionals rather than team.
1.	Fist bumps over handshakes.
1.	Sick/ Contagious employees may not participate in team food preparation activities.

**Responding to Unsafe Situations:**

The following are GitLab’s procedures in the event an employee feels threatened or unsafe:

1. If at any point, an employee feels that they are in danger of physical harm, please contact the local authorities immediately.  Local law enforcement can act quickly to protect the individual and neutralize any threats.
1. If at any point an employee feels like they or another employee may require immediate medical assistance, please contact the local authorities.
1. Once the immediate threat is controlled, employees should report any safety concerns to People Ops.
1. If you believe that a certain location, event or area presents greater risk or exposure to individuals, please notify People Ops.  People Ops can proactively communicate the concerns to other [potentially] affected employees.
1. If at any point you believe you, personally, may commit an unsafe act, People Ops can assist in providing information about available [Employee Assistance Options](/handbook/total-rewards/benefits/general-and-entity-benefits/#employee-assistance-program).

### Mental Health Awareness

The World Health Organization (WHO) [defines health](https://www.google.co.uk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0ahUKEwj3oZ7I1M_XAhUsI8AKHXWiC5EQFggmMAA&url=http%3A%2F%2Fwww.who.int%2Fgovernance%2Feb%2Fwho_constitution_en.pdf&usg=AOvVaw0usN4G0kSEScrW3HqENGIr) as:

* "a state of complete physical, mental and social well-being and not merely the absence of disease or infirmity.  The enjoyment of the highest attainable standard of health is one of the fundamental rights of every human being without distinction of race, religion, political belief, economic or social condition."

The WHO [defines **mental health**](http://www.who.int/features/factfiles/mental_health/en/) as:

* “a state of well-being in which the individual realizes his or her own abilities, can cope with the normal stresses of life, can work productively and fruitfully, and is able to make a contribution to his or her community.”

Defining the terms from the sentence above:

* *"A state of well-being"* is a self-reported measure of 'wellness'.
* *"The individual realizes his or her own abilities"* requires feedback, positive or negative.
* *"Can cope with the normal stresses of life"* i.e. does not find normal life overwhelming too much of the time.
* *"Can work productively and fruitfully"* here GitLab clearly has a role to play as it can provide an opportunity for productive and fruitful work.
* *"Is able to make a contribution to his or her community"* versus the inverse, which is only being able to draw from that community.

1. Why is awareness of Mental Health important at GitLab?

   * It can affect any and all of us. The statistics from the WHO are that [1 in 4](http://www.who.int/whr/2001/media_centre/press_release/en/) of us will be affected by mental or neurological disorders at some point in our life. That said, we are all subject to periods where we or those around us find the "the normal stresses of life" harder than usual to deal with.
   * The more we are aware of mental health, the more inclusive we are. That will help encourage any colleagues currently experiencing mental health issues to talk about it.
   * Our business at its core is a group of people working together towards a common goal. With awareness of what might affect our colleagues, we are better equipped to help them if they do discuss it with us and therefore help our business.
   * Mental health has so much emotional baggage as a topic that it can initially seem scary to talk about. Promoting mental health awareness helps to remove the stigma and taboos associated with it.
   * GitLab can offer "productive and fruitful" work for all of our employees. That should not be [underestimated](https://www.google.co.uk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwitr7eP18_XAhWIORoKHY2_CA4QFggrMAA&url=https%3A%2F%2Fcardinal-management.co.uk%2Fwp-content%2Fuploads%2F2016%2F04%2FBurton-Waddell-is-work-good-for-you.pdf&usg=AOvVaw0ROXJdWquGML5bIsBEIRLo).
   * In the cold-light of business metrics, the healthier we are, [the more productive we are](https://www.rand.org/randeurope/research/projects/workplace-health-wellbeing-productivity.html).

1. At GitLab we strive to create a stigma-free workplace. In accordance with the National Mental Health Association and the National Council for Behavioral Health we would like to:

   * Educate employees about the signs and symptoms of mental health disorders.
   * Encourage employees to talk about stress, workload, family commitments, and other issues.
   * Communicate that mental illnesses are real, common, and treatable.
   * Discourage stigmatizing language, including hurtful labels such as “crazy,” “loony” or “nuts.”
   * Help employees transition back to work after they take leave.
   * Encourage consultation with our employee assistance programs.

1. What are we doing to get there?
   * People Operations Learning and Development team will be developing training for managers on this topic.
   * Talk about mental health issues and ideas in the [#mental_health_aware](https://gitlab.slack.com/messages/C834CM4HW) Slack channel.  Examples of what we discuss and share in there:
     * [Imposter Syndrome](https://pbs.twimg.com/media/EboRwohXgAIYaVN.png) 
   * GitLab would also like to encourage GitLab team-members to take their [time off](/handbook/paid-time-off) to properly take care of themselves. We encourage the team to go to yoga, take a long lunch, or anything else in their day to day life that assists in their mental and emotional well-being.
   * In addition to our current EAP programs available for employees, we encourage GitLab team-members to take a look at [Working Through It](https://www.workplacestrategiesformentalhealth.com/employee-resources/working-through-it) for insight into reclaiming well-being at work, off work, and return to work.
   * We believe that our values and culture lends itself to being able to discuss mental health open and honestly without being stigmatized, but let's work together to make it even more inclusive. For example, Finding the right words:
     * "How can we help you do your job?"
     * "You’re not your usual self."
     * "Do you want to talk about it?"
     * "It's always OK to ask for help."
     * "It’s hard for me to understand exactly what you’re going through, but I can see that it’s distressing for you."

Any questions or concerns? Please feel free to speak with anyone in People Ops.

### Background Checks

GitLab is concerned about the safety of its team members and about maintaining appropriate controls to ensure that assets of GitLab and our customer relationships and information are protected. To reduce these risks, GitLab will obtain and review background information of covered prospective, and, as applicable, current employees.

GitLab has contracted with [Sterling Talent Solutions](https://www.sterlingtalentsolutions.com/) to perform these background checks, which will cover criminal history for the last 7 years. For all candidates being considered for a Director level position or higher, an employment history for the last 5 years and/or the three most recent employers will be conducted. GitLab may use the returned background check information to make decisions regarding employment. For certain positions where the candidates financial history is relevant to the position, we may also run a check in the federal database for any financial related offenses.

All candidates who make it to the offer stage with GitLab must undergo a background screening according to this policy as part of the employment screening process. All contracts will state that employment is subject to obtaining results from an approved background screening that are satisfactory to GitLab. If a candidate is unwilling to follow this process we are unable to proceed with their candidacy for any position at GitLab. In the event the background check is not available at the time of hire (switching vendors or delays in processing), GitLab will run the background check as soon as possible. The same adjudication guidelines will apply to current employees as they do with prospective employees.

The Candidate Experience Specialists will [initiate](https://about.gitlab.com/handbook/hiring/recruiting-framework/coordinator/#step-20c-initiate-background-check) all background screenings for candidates. Background check results will be received by the Candidate Experience Specialist and brought to the relevant People Business Partner and functional leader to determine if the results warrant any adverse action, which could include rescinding of the offer or termination of employment.

#### Disclosure and Authorization

Candidates (as applicable, employees) will receive an email to fill out the background check application. The application will ask for personal and professional information. The application process includes signing a disclosure and a consent form which explains the rights of an individual undergoing a background examination. The application process is designed to take less than fifteen minutes to complete.

To prepare for the employment verification for those candidates being considered for Director level positions or higher, candidates should gather each previous employer's name and address, position title held, employment start and end dates, manager’s name and title, their phone number, and email address. Details for a Human Resources contact can be entered instead of a manager's contact details. Occasionally, Sterling will reach out to the candidate to retrieve additional information, such as backup documentation to act as proof of previous employment or picture IDs. Proof of employment can typically be provided in various ways, such as tax returns (e.g. W2s), pay stubs, LLC documentation, official company registrations, etc.

Background checks will act as an additional mechanism of transparency and will help to build trust with our clients.

#### Review Criteria

Once the background check is completed, Candidate Experience Specialists will review the report and determine if any negative information has a direct connection with an applicant’s ability to fulfill the job duties with competence and integrity. Criminal convictions that would raise a concern are job-related offenses, including but not limited to: embezzlement, extortion, computer/internet crime, fraud, tax evasion, and violent crimes.  In addition, the report should be reviewed for omissions or inaccuracies contained in the employment application or made during the interview process.

#### Fair Credit Reporting Act (FCRA) and Related State Law Compliance

**Step 1: Disclosure and Authorization**

The applicant must give the employer consent to have a third party service conduct a background check. The Disclosure and Authorization form can be presented to the applicant at the time they complete the employment application form. The form should grant the employer permission to conduct an initial background check (and, subject to state law, subsequent background checks if the applicant is hired) utilizing a third party service. Also, a “Summary Of Your Rights Under The Fair Credit Reporting Act” should be enclosed with the consent and disclosure form. For New York applicants, a copy of Article 23-A of the Correctional Law also should be enclosed and any other relevant state summary of rights.

The background investigation cannot be lawfully conducted without a signed Disclosure and Authorization form. Applicants can be advised that they will not be considered for employment without submitting the signed form. Equally for current team members, they can be advised that their employment may be impacted if they do not consent to the background check.

**Step 2: Pre-Adverse Action: Notify the Applicant of Negative Report BEFORE Adverse Action is taken**

If the consumer reporting agency reports information which may be used, in whole or in part, as a basis for an adverse employment action (e.g., rescinding a conditional offer of employment), the applicant must receive notification before a final decision is made to deny employment. As a result, the employer must provide a copy of the consumer report, a pre-adverse action letter, and another copy of the FCRA notice of rights (and for New York applicants, the Article 23-A notice). The applicant shall also receive any applicable state rights as required.

If the disqualification decision is not based on a misrepresentation or omission in the employment application, it is a best practice to discuss the potentially disqualifying information with the individual prior to issuing the pre-adverse action notice. This practice supports the individual job-related nature of any disqualification decision.

**Step 3:	Wait for a Reasonable Period of Time to Find Out What, if Any, Explanation is Offered by the Applicant**

If the applicant does not respond at all to the notification within a reasonable period of time (5 days), the employer may proceed with its decision to rescind the conditional offer. If the applicant responds, the employer should carefully consider the information submitted and then make a decision. If the explanation is reasonable under the circumstances, then it may still be possible to go forward with the new hire (for example, a case of mistaken identity). However, if the applicant's explanation is determined to be insufficient, then the employer should proceed to the next step.

**Step 4:	Notify Applicant of Adverse Action**

The employer must provide the applicant with written notice of the adverse action and the name, address, and telephone number of the consumer reporting agency.
The Adverse Action Notice form should be sent along with the federal summary of rights and any applicable state summary of rights. The notice includes a statutorily required statement that the consumer reporting agency did not make the decision and does not know why the decision was made should be included as well as a notice of the applicant's right to obtain the report and dispute the information.

**Step 5: Maintain Documentation**

For all adverse decisions, document each step taken. Keep copies of all consent and disclosure forms and other documentation sent to the applicant in the event the company has to defend its decision at some later point.

#### Record Retention

All documents related to the background check process must be retained for at least five years.

#### Equal Employment Laws

GitLab will adhere to all equal employment laws. When reviewing any criminal record information that appears on a background check, the company shall factor in any known factors relating to:

1. The facts and circumstances surrounding the offense.
1. The number of offenses for which the individual was convicted.
1. The age of the individual at the time of conviction or release from prison.
1. Evidence that the individual has performed the same type of work, post-conviction, with the same or a different employer, without incidents of criminal conduct.
1. The length and consistency of employment history before and after the offense.
1. Any efforts of the applicant towards rehabilitation.
1. Employment or character references obtained regarding the individual’s fitness for the particular position.
1. Whether the individual will be bonded for the position.

#### Financial Checks

Finance team members **only** will be required to participate in a federal check through Sterling, which searches for any tax-related or financial offenses. See [this page](https://about.gitlab.com/handbook/hiring/recruiting-framework/coordinator/#step-20c-initiate-background-check) for process details.

### Job Abandonment

When a team member is absent from work for three consecutive workdays, there is no entry on the availability calendar for time off, and fails to contact his or her supervisor, they can be [terminated](/handbook/people-group/offboarding/#involuntary-terminations) for job abandonment unless otherwise required by law.

If a manager is unable to reach a team member via email or slack within a 24 hour period they should contact their [People Business Partner](https://about.gitlab.com/handbook/people-group/how-to-reach-us/#people-business-partner-alignment-to-groups). The People Business partner will access the team member's information to obtain additional contact methods and numbers. The manager and People Business Partner will create an action plan to make all attempts to contact the team member.

### Other People Policies

* [United States Employment Status](/handbook/contracts/#united-states-employment-status)
* [PIAA Agreements](/handbook/contracts/#piaa-agreements)
* [360 Feedback](/handbook/people-group/360-feedback/)
* [Return of Property](/handbook/people-group/offboarding/#returning-property)
* [Promotions and Transfers](/handbook/people-group/promotions-transfers/)
* [General Benefits](/handbook/total-rewards/benefits/general-and-entity-benefits/)
* [Entity Specific Benefits](/handbook/total-rewards/benefits/general-and-entity-benefits/#entity-specific-benefits)
* [Parental Leave](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave)
* [Paid Time Off](/handbook/paid-time-off/)
* [Probation Period](/handbook/contracts/#probation-period)

### Partner Due Diligence

GitLab’s Anti-Bribery Policy prohibit giving illegal or improper payments to, or receiving such payments from, any person or organization, including government officials (U.S. and foreign) and persons in the private sector. Such payments also are prohibited by the U.S. Foreign Corrupt Practices Act ("FCPA") and anti-bribery laws and regulations in foreign jurisdictions, including but not limited to the U.K. Bribery Act 2010 and the European Commission on Anti-Corruption (collectively, “Anti-Bribery Laws”).

GitLab’s export policy requires that we ensure compliance with applicable export laws including the Export Administration Regulations (EAR), the International Traffic in Arms Regulations (ITAR) (if and to the extent they become applicable to GitLab’s products), general prohibitions and country control lists (collectively “Export Laws”).

It is an offense under Anti-Bribery Laws for a company to engage a third party who pays bribes in connection with the company’s business. Additionally, it is an offense under US Export Laws for a company to engage a third party who violates export laws.  In short, GitLab can be held criminally and civilly liable for the bad acts of its partners.

Therefore, the Anti-Bribery, Anti-Corruption and Export Policy applies not only to GitLab’s employees, but also to all resellers, agents, consultants, joint venture partners or other representatives who provide services directly related to obtaining, retaining or facilitating businesses or business opportunities for GitLab (“Partners”).

In order to minimize the risk relating to Anti-Bribery Law and Export Law violations from such third parties, due diligence will be conducted on GitLab Partners.  Due diligence is the process of taking reasonable steps to satisfy legal requirements and ensure that there are no red flags associated with the respective Partner ("Due Diligence"). The manager of the relationship is responsible for ensuring that the Due Diligence is conducted.

For GitLab, Due Diligence is verification that the proper contract terms are in play with the appropriate parties and verification that no Red Flags are present prior to moving forward in a relationship. If a red flag is present, the matter should be escalated to Legal.  Legal will make a recommendation on whether or not to pursue the relationship based on the circumstances.  Legal, at its sole discretion, will document justification for its final disposition.

Red Flags include:
  A poor credit check;
  Presence of media reports or rumours relating to illegal payments, bribes, export violations, corruption or other criminal activity;
  Requesting an unusually high commission if an agent or if a reseller is requesting to be paid money;
  Partner requesting a cash payment or payment to a secret account;
  Partner requesting third parties to be added to a contract;
  Partner refuses to disclose owners or principals for purposes of a credit check;
  Partner attempts to negotiate around (or refuses to agree to) terms around penalties around corruption or violation of law; and/or
  A negative hit on Denied Party Screen.  (Denied Party Screens are conducted by Legal or Compliance.)

Please contact Legal or Compliance with questions or concerns about this section.

### Partner Code of Ethics

GitLab, Inc. and its respective affiliates, subsidiaries and divisions (“GitLab”) operate business in a responsible manner. At GitLab, the way we conduct business is as important as the relationships we have and the products and services we provide. Accordingly, GitLab will only do business with suppliers, contractors, resellers, agents and consultants (collectively herein referenced as “Partners”) that comply with applicable and controlling laws, rules, and regulations (collectively herein referenced as “applicable laws”) and at a minimum, with standards of business conduct consistent with those set forth in this Partner Code of Ethics (“Code”).

It is GitLab’s expectation that Partners, their employees, sub-suppliers and any other parties involved with the execution of GitLab work, similarly comply with the applicable laws and the standards set forth in this Code. GitLab expects the following, without limitation, including respecting the human rights of employees from all its Partners:

**HUMAN RIGHTS AND LABOR STANDARDS**

*Forced Labor, Human Trafficking and Slavery*
Partner shall not use any form of forced labor including prison, indentured, bonded, military, slave or any other forms of forced labor. Partner shall not participate in the recruitment, transportation, transfer, harboring or receipt of any persons by means of threat, use of force, or any other forms of coercion, abduction, fraud, deception, abuse of power or position of vulnerability, or the giving or receiving of payments or benefits to achieve the consent of a person having control over another person for the purpose of exploitation. Partners shall not retain an employees’ government-issued identification, passports or work permits as a condition of employment and shall allow employees to resign from their positions at any time.

*Child Labor*
Partner shall ensure that no underage labor has been used in the production or distribution of their goods or services. Employees must not be younger than the minimum employment age established by the respective country or local jurisdiction. In the event no minimum employment age is established, employees must not be younger than the age of compulsory education; or if no minimum age for compulsory education is established, employees should not be younger than age 14.

*Working Hours*
Partner’s employee working hours must be in compliance with all applicable laws and regulations. Partners should encourage employees to receive at least one day off every seven days in compliance with all applicable laws.

*Wages and Benefits*
Partners must have a system in place to verify and accurately record payroll, deductions and the hours worked by legally authorized employees. Partners must comply with all applicable wage and compensation requirements as defined under applicable labor laws for regular work, overtime, maximum hours, piece rates, and other elements of compensation and employee benefits.

*Freedom of Association and Collective Bargaining*
Partner must adhere to applicable laws regarding the right to affiliate with lawful organizations without interference.

*Nondiscrimination*
Employment by Partner shall be based solely on an individual’s ability and not personal characteristics. Partner shall maintain a workplace free of unlawful discrimination, which includes, but is not limited to, race, gender, sexual orientation, age, pregnancy, caste, disability, union membership, ethnicity, religious belief or any other factors protected by applicable law. Employees shall not be subject to verbal, physical, sexual or psychological abuse or any other form of mental or physical coercion and shall be treated with respect and dignity.

*Conflict Minerals*
Partner shall abide by all regulations and laws relating to conflicts minerals and legal and sustainable sourcing.

**HEALTH AND SAFETY**

Partners shall provide safe and healthy working and housing environments (if Partner provides housing) to prevent accidents and injury to health. Partners shall minimize employee exposure to potential safety hazards by identifying, assessing and minimizing risks by developing and implementing plans and procedures.

**ENVIRONMENT**

Partners shall be sensitive to its impact on the environment (including but not limited to air emissions, water discharge, toxic substances and hazardous waste disposal) and local communities. Partner shall comply with the environmental laws and standards within its facilities. Partners must use care in handling hazardous materials or operating processes or equipment that use hazardous materials to prevent unplanned releases into the workplace or the environment.

**ANTI-BRIBERY AND ANTI-CORRUPTION**

Partners shall not engage in any form of corrupt practices including without limitation to, extortion, fraud, impersonation, false declarations, bribery, money laundering, supporting or involved with terrorist or organized crime organizations or activities. Partners shall not offer bribes, kickbacks, illegal political contributions or other improper payments to GitLab representative or agency, any customer, government official or third party, with the intention of obtaining or retaining a business or other improper advantage. Partners must have a written anti-corruption / anti-bribery policy that includes an annual review with its employees of such policy.

**PRIVACY AND SECURITY**

Partners shall ensure that there are appropriate administrative, technological, physical and technical controls in place to ensure the protection and security of any data subject subject to laws and regulations.  Partners will execute any necessary agreement relating to the handling of data and will notify GitLab of any known or suspected vulnerabilities that may compromise individuals subject to the relationship with GitLab.

**COMPLIANCE**

If a Partner’s efforts to comply with this Code have been deficient and Partner fails to cooperate in developing and implementing reasonable remedial steps, GitLab reserves the right to take appropriate actions up to, and including, discontinuing the relationship with Partner. Nothing in this Code is intended to, in any way, grant any additional rights or expectations to a GitLab Partner or, in any way, modify or otherwise limit any of GitLab’s contractual or legal rights.

No matter where we operate around the world, we are steadfast in our dedication to service and integrity. Strong partnerships are a cornerstone of GitLab’s business and a vital link in setting and achieving expectations for ethical sourcing and corporate social responsibility. At GitLab, the way we conduct business is as important as the people with whom we conduct business. services we provide.

### Commitment to Non-Retaliation

Any employee or contractor who reports a violation will be treated with dignity and respect and will not be subjected to any form of discipline or retaliation for reporting in good faith. Retaliation against anyone who provides information or otherwise assists in an investigation or proceeding will be treated as a violation of this Code.

### Open Door Policy

We encourage maximum communication between team members at all levels of the organization.  This is an important part of our culture.  Whenever problems or concerns arise, it is expected that they will be addressed as quickly as possible.  Your immediate manager is the person on the management team who is closest to you and your work.  When you need help or have questions, complaints, problems or suggestions, please contact your manager first.  It is your manager's responsibility to assist you - so please ask, and be willing to work the issue out with your manager.  They are interested in your success, the success of every team member in their department, and the overall success of GitLab.

If your manager cannot help you or answer your questions, your questions will be referred to someone who can.  If you feel your particular question, concern or suggestion cannot be discussed with your manager, you are encouraged to contact your manager's manager, your assigned People Business Partner, the Chief People Officer or the CEO.  It is important to remember that a team member who takes these steps will not be reproached.  You can expect to be treated fairly and with respect.

### Discrimination

Having a diverse workforce, made up of team members who bring a wide variety of skills, abilities, experiences and perspectives, is essential to our success. We are committed to the principles of equal opportunity, inclusion, and respect. All employment-related decisions must be based on company needs, job requirements, and individual qualifications. Always take full advantage of what our team members have to offer; listen and be inclusive.

* We do not tolerate discrimination against anyone, including team members, customers, business partners, or other stakeholders, on the basis of race, color, religion, national origin, sex (including pregnancy), age, disability, HIV status, sexual orientation, gender identity, marital status, past or present military service, or any other status protected by the laws or regulations in the locations where we operate.
* We comply with laws regarding employment of immigrants and non-citizens and provide equal employment opportunity to everyone who is legally authorized to work in the applicable country.
* We provide reasonable accommodations to individuals with disabilities and remove any artificial barriers to success.

Report suspected discrimination right away and never retaliate against anyone who raises a good faith belief that unlawful discrimination has occurred. Employees and contractors should refer to the [GitLab Anti-Harassment Policy](/handbook/anti-harassment/) for more information.

### Harassment

Every employee or contractor has a right to a work environment free from harassment, regardless of whether the harasser is a co-worker, supervisor, manager, customer, vendor, or visitor. Please refer to the [GitLab Anti-Harassment Policy](/handbook/anti-harassment/) for more information. As is the case with any violation of the Code, you have a responsibility to report any harassing behavior or condition, whether you are directly involved or just a witness.

### Personal Hygiene

When attending Contribute or any conference, public meeting, customer meeting or meet-up, kindly keep in mind you are representing GitLab. Personal hygiene and hygiene in general helps to maintain health and prevent the spread of diseases and various other illnesses. We motivate everyone to maintain cleanliness.
For more information about our Contribute Code of Conduct, read more [here](/company/culture/contribute/coc/).

### Social Responsibility

We pride ourselves on being a company that operates with integrity, makes good choices, and does the right thing in every aspect of our business. We will continually challenge ourselves to define what being a responsible company means to us, and work to translate our definition into behavior and improvements at GitLab. We seek to align our social and environmental efforts with our business goals and continue to develop both qualitative and quantitative metrics to assess our progress.

### Substance Abuse

GitLab strives to maintain a workplace that is free from illegal use, possession, sale, or distribution of alcohol or controlled substances. Legal or illegal substances shall not be used in a manner that impairs a person’s performance of assigned tasks. This will help to maintain the efficient and effective operation of the business, and to ensure customers receive the proper service. GitLab team members must also adhere to the local laws of where they reside and where they travel to, including the [GitLab Contribute](/company/culture/contribute/).

### Employee Information Privacy

GitLab respects the confidentiality of the personal information of employees and contractors. This includes employee and contractor medical and personnel records. All team member records are kept in [BambooHR](/handbook/people-group/#sts=Using BambooHR). Team members have self service access to their profile. Where available, documents and information are shared with the team member within the platform. If a team member would like to view their entire profile from the admin view, please schedule a call with People Operations Specialists to walk through a screen share or request screenshots to be sent to your personal email address. Access to personal information is only authorized when there is a legitimate and lawful reason, and access is only granted to appropriate personnel. Requests for confidential employee or contractor information from anyone outside our company under any circumstances must be approved in accordance with applicable laws. It is important to remember, however, that employees and contractors should have no expectation of privacy with regard to normal workplace communication or any personal property used for GitLab business.

If there is no requirement within someone's job description to be public-facing, then team members can opt-out of any public exposure. Team members can opt-out of being added to the [team page](/company/team/) or what content about them is shown on the team page and can use either only their initials or an alias if desired. Since GitLab publishes much of our content, including video calls and meetings, the only way to ensure no unwanted exposure from these videos is to have video turned off and initials or an alias added to the Zoom profile name whenever a call is being recorded. Zoom shows whether a call is being recorded at the top right of the video screen, and team members are always encouraged to ask if a video will be shared or not. For any GitLab livestreams through YouTube, a team member can watch and comment through YouTube instead of through the internal video call. Any questions can be sent directly to our People Business Partners or CPO.

### Employee Privacy Policy

This Employee Privacy Policy (“Privacy Policy”) explains what types of personal information we may collect about our employees and how it may be used.

While this Privacy Policy is intended to describe the broadest range of our information processing activities globally, those processing activities may be more limited in some jurisdictions based on the restrictions of their laws. For example, the laws of a country may limit the types of personal information we can collect or the manner in which we process that information. In those instances, we adjust our internal policies and practices to reflect the requirements of local law.

For personal data collected under this Privacy Policy, the controller will be GitLab and the GitLab affiliates by which you are employed. For specific security concerns around your data, please contact your Data Protection Officer (“DPO”) by emailing [dpo@gitlab.com](mailto:dpo@gitlab.com) or your Privacy Officer by emailing [compliance@gitlab.com](mailto:compliance@gitlab.com).

In the event you feel that you have not received proper attention to your data concern, or if you have any other legal/law enforcement concern - please contact GitLab's Legal Department at Legal@gitlab.com.

GitLab, Inc. is a global company with its headquarters in the U.S. This means that personal information may be used, processed, and transferred to the United States and other countries or territories and those countries or territories may not offer the same level of data protection as the country where you reside, including the European Economic Area.  However, GitLab will ensure that appropriate or suitable safeguards are in place to protect your personal information and that transfer of your personal information complies with applicable data protection laws. Where required by applicable data protection laws, GitLab has ensured that service providers (including other GitLab affiliates) sign standard contractual clauses as approved by the European Commission or other supervisory authority with jurisdiction over the relevant GitLab data exporter (which typically will be your employer).

*Who is collecting your personal data (who is the data controller)?*

The GitLab entity that is a party to your employment contract or contract for services or otherwise employees you will be the data controller of your personal data.  The following are the GitLab entities that act as controller:  GitLab, Inc., GitLab, LLC., GitLab BV, GitLab GmbH, GitLab, LTD, GitLab PTY Ltd, GitLab Canada Corp, GitLab IT BV, and other GitLab subsidiaries throughout the globe ("collectively "GitLab").

GitLab affiliates may act as processors on behalf of other GitLab affiliates and/ or controllers.  Furthermore, the GitLab its affiliates and subsidiaries participate in group-wide IT system in order to harmonize GitLab’s IT infrastructure and its use (the “System”). The System also may hold data on all employees, workers, individual contractors and contingent workers ("Staff"). Insofar the System serves to improve and harmonize most of the human resources (“HR”) processes within GitLab. GitLab, Inc. in the US is responsible for the System.

*Applicability of Other GitLab Privacy Policies*

The websites of GitLab (e.g., [about.gitlab.com](/)) have separate privacy policies and terms of use that apply to their use. Additionally, some of our third party products and services may have separate privacy policies and terms of use that apply to their use. Any personal information collected in connection with your use of those websites or products and services are not subject to this Privacy Policy.  If you are unsure how or if this Privacy Policy applies to you, please contact your DPO or Privacy Officer.

*Third Party Services*

In some cases, you may provide personal information to third parties that GitLab works with or that provide services to GitLab.  This includes, those parties identified in the [Tech Stack Application](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0) document (“Third Parties”).

The Tech Stack is updated periodically to ensure accurate, up-to-date disclosure of employee and customer third party applications used at GitLab.  This particular policy applies to those applications identified as relating to Employee applications.  The use of such Third Party websites may be governed by separate terms of use and privacy policies which are not under our control and are not subject to this Privacy Policy. Please contact such Third Parties for questions regarding their privacy practices, as well as if you would like to have them modify, update, alter or delete your personal information.  Please understand that there are exceptions to rights surrounding personal data relating to employment.  GitLab is required to maintain certain employment information by law.

*What is Personal Information?*

Personal information, also known as personally identifiable information or personal data, for purposes of this Privacy Policy means any information that (i) directly and clearly identifies an individual, or (ii) can be used in combination with other information to identify an individual. Personal information does not include such information if it is anonymous or if it has been rendered de-identified by removing personal identifiers.

Examples of personal information include:
 - An individual’s name.
 - Employee ID number.
 - Home address.
 - Home phone number.
 - Personal email address.
 - Names of family members.
 - Date of birth.

*What is Sensitive Personal Information?*

Sensitive personal information is a subset of personal information that may be more sensitive in nature for the individual concerned.

Examples of sensitive personal information include:
- Race and ethnic information.
- Sexual orientation.
- Political/religious beliefs.
- Social security or other taxpayer/government issued identification numbers.
- Financial information.
- Health or medical information, including genetic information.
- Criminal records.
- And in some regions, such as the European Union, trade union membership.

*What Personal Information Do We Collect?*

We collect and maintain different types of personal information about you in accordance with applicable law. This includes the following:
- Name
- Gender
- Home address
- Telephone number
- Date of birth
- Marital status
- Employee identification number
- Emergency contacts
- Residency
- Work permit status
- Military status
- Nationality
- passport information
- Social security or other taxpayer/government identification number
- Payroll information, banking details
- Wage and benefit information
- Retirement account information
- Sick pay, Paid Time Off, retirement accounts, pensions, insurance and other benefits information (including the gender, age, nationality and passport information for any spouse, minor children or other eligible dependents and beneficiaries).
- Information from interviews and phone-screenings you may have had, if any.
- Date of hire, date(s) of promotion(s), work history, technical skills, educational background, professional certifications and registrations, language capabilities, and training records.
- Beneficiary and emergency contact information.
- Forms and information relating to the application for, or in respect of changes to, employee health and welfare benefits; including, short and long-term disability, medical and dental care, etc.
- Physical limitations and special needs in order to provide accommodations.

- Records of work absences, vacation/paid time off, entitlement and requests, salary history and expectations, performance appraisals, letters of appreciation and commendation, and disciplinary and grievance procedures (including monitoring compliance with and enforcing our policies).

Where permitted by law and applicable we may collect the results of credit and criminal background checks, screening, health certifications, driving license number, vehicle registration, and driving history.

- Information required for us to comply with laws, the requests and directions of law enforcement authorities or court orders (e.g., child support and debt payment information).

- Acknowledgements regarding our policies, including employee handbooks, ethics and/or conflicts of interest policies, and computer and other corporate resource usage policies.

- Information captured on security systems and key card entry systems.

- Voicemails, e-mails, correspondence, documents, and other work product and communications created, stored or transmitted using our networks, applications, devices, computers, or communications equipment.

 - Date of resignation or termination, reason for resignation or termination, information relating to administering termination of employment (e.g. references).

 - Letters of offer and acceptance of employment.

 - Your resume or CV, cover letter, previous and/or relevant work experience or other experience, education, transcripts, or other information you provide to us in support of an application and/or the application and recruitment process.

 - References and interview notes.

 - Information relating to any previous applications you may have made to GitLab and/or any previous employment history with GitLab.

For specifics about what information is collected by third party applications, please refer to the [Tech Stack Applications](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0).

Apart from personal information relating to yourself, you may also provide us with personal data of related parties, notably your dependents and other family members, for purposes of your HR administration and management, including the administration of benefits and to contact your next-of-kin in an emergency. Before you provide such third-party personal data to us you must first inform these third parties of any such data which you intend to provide and of the processing to be carried out by us. You must ensure and secure evidence that these related parties, or their legal representatives if they are minors, have given their free and express consent that their personal data may be processed by GitLab and/or its affiliates and subcontractors for the purposes described in this Privacy Policy.

*How is Data Collected?*

Generally, we collect personal information directly from you in circumstances where you provide personal information (during the onboarding process, for example). However, in some instances, the personal information we collect has been inferred about you based on other information you provide us, through your interactions with us, or from third parties. When we collect your personal information from third parties it is either because you have given us express consent to do so, your consent was implied by your actions (e.g., your use of a Third-Party employee service made available to you by us), or because you provided explicit consent to the Third-Party to provide the personal information to us.  Where permitted or required by applicable law or regulatory requirements, we may collect personal information about you without your knowledge or consent.

We reserve the right to monitor the use of our equipment, devices, computers, network, applications, software, and similar assets and resources for the safety and protection of employees and intellectual property. In the event such monitoring occurs, it may result in the collection of personal information about you.  If required by applicable law, we will notify you of such monitoring and obtain your consent.

*How We Process and Use Your Personal Information*

We may collect and process your personal information in the Systems for various purposes subject to local laws and any applicable collective bargaining agreements and works council agreements, including:

 - Recruitment, training, development, promotion, career, and succession planning

 - Appropriate vetting for recruitment and team allocation including, where relevant and appropriate, credit checks, right to work verification, identity fraud checks, relevant employment history, relevant regulatory status and professional qualifications

 - Providing and administering remuneration, salary, benefits, and incentive schemes and providing relevant information to payroll

 - Allocating and managing duties and responsibilities and the business activities to which they relate

 - Identifying and communicating effectively with other employees and management

 - Managing and operating conduct, performance, capability, absence, and grievance related reviews, allegations, complaints, investigations, and processes and other informal and formal HR processes and making related management decisions

 - Consultations or negotiations with representatives of the workforce

 - Conducting surveys for benchmarking and identifying improved ways of working employee relations and engagement at work (these will often be anonymous but may include profiling data such as age to support analysis of results)

 - Processing information about absence or medical information regarding physical or mental health or condition in order to assess eligibility for incapacity or permanent disability related remuneration or benefits, determine fitness for work, facilitate a return to work, make adjustments or accommodations to duties or the workplace and make management decisions regarding employment or engagement or continued employment or engagement or redeployment and conduct related management processes

 - For planning, managing and carrying out restructuring or redundancies or other change programs including appropriate consultation, selection, alternative employment searches and related management decisions

 - Operating email, IT, Internet, intranet, social media, HR related and other company policies and procedures. The company carries out monitoring of GitLab's IT systems to protect and maintain the systems, to ensure compliance with GitLab policies and to locate information through searches where needed for a legitimate business purpose

 - Complying with applicable laws and regulation (for example maternity or parental leave legislation, working time and health and safety legislation, taxation rules, worker consultation requirements, other employment laws and regulation to which GitLab is subject in the conduct of its business)

 - Monitoring programs to ensure equality of opportunity and diversity with regard to personal characteristics protected under local anti-discrimination laws

 - Planning, due diligence and implementation in relation to a commercial transaction or service transfer involving GitLab that impacts on your relationship with GitLab (for example mergers and acquisitions or a transfer of your employment under automatic transfer rules)

 - For business operational and reporting documentation such as the preparation of annual reports or tenders for work or client team records including the use of your personal photo

 - In order to operate the relationship with Third-Party customer and suppliers including the disclosure of relevant vetting information in line with the appropriate requirements of regulated customers to those customers, contact or professional CV details or resume, or your personal photo for identification to clients or disclosure of information to data processors for the provision of services to GitLab

 - Where relevant for publishing appropriate internal or external communications or publicity material including via social media in appropriate circumstances, provided that privacy rights are preserved

 - To support HR administration and management and maintaining and processing general records necessary to manage the employment or worker relationship and operate the contract of employment or engagement

 - To centralize HR administration and management processing operations in an efficient manner for the benefit of our employees and to change access permissions

 - To provide support and maintenance for the System

 - To enforce our legal rights and obligations, and for any purposes in connection with any legal claims made by, against or otherwise involving you

 - To comply with lawful requests by public authorities (including without limitation to meet national security or law enforcement requirements), discovery requests, or where otherwise required or permitted by applicable laws, court orders, government regulations, or regulatory authorities (including without limitation data protection, tax and employment), whether within or outside your country

 - Other purposes permitted by applicable privacy and data protection legislation including where applicable, legitimate interests pursued by GitLab where this is not overridden by the interests or fundamental rights and freedoms of employees.

Additional information regarding specific processing of personal data may be notified to you by locally.

*Legal Basis for processing*

Where applicable data protection laws require us to process your personal data on the basis of a specific lawful justification, we generally process your personal data under one of the following bases:

Compliance with a legal obligation to which GitLab is subject; Entering into at-will employment (for US only) or performance under an employment contract with GitLab;
For GitLab's legitimate interests being those purposes described in the section above headed "How We Process and Use Your Personal Information";
Your consent where required and a legitimate legal basis under applicable local laws.

We may on occasion process your personal data for the purpose of the legitimate interests of a Third-Party where this is not overridden by your interests.

*Processing of Special Categories of Personal Data*

“Special Categories of Personal Data” includes information revealing racial or ethnic origin, political opinions, religious or philosophical beliefs, trade union membership, health, sex life or sexual orientation, as well as genetic and biometric data.

From time to time you may provide us with information which constitutes Special Categories of Personal Data or information from which Special Categories of Personal Data may be deduced.  In such cases, where required by law, we will obtain your express written consent to our processing of Special Categories of Personal Data.  If separate consent is not required by local law, by providing this information to GitLab, you give your freely given, informed, explicit consent for us to process those Special Categories of Personal Data for the purposes set out in How We Process and Use Your Personal Information section above.

You may withdraw your consent at any time by contacting your Local HR Department or DPO. Where you have withdrawn consent but GitLab retains the personal data we will only continue to process that Special Category Personal Data where necessary for those purposes where we have another appropriate legal basis such as processing necessary to comply with legal obligations related to employment or social security. However, this may mean that we cannot (for example) administer certain benefits or contact your next-of-kin in an emergency or provide support to you above and beyond our legal obligations.  You give your knowledgeable, freely given, express consent to GitLab for GitLab to use, disclose and otherwise process any personal health information about you that is provided to GitLab by any of your personal health information custodians, for the purposes set out in the How We Process and Use Your Personal Information section above.

*Sharing Personal Information*

Your personal information may be shared, including to our affiliates, subsidiaries, and other third parties, as follows:

 - Where you request us or provide your consent to us.

 - In order to carry out the uses of personal information described above (see, How We Process and Use Your Personal Information).
When using or collaborating with third parties in the operation of our business, including in connection with providing many of the benefits and services we offer our employees (e.g., human resources information systems, financial investment service providers, insurance providers). When we share personal information with third parties we typically require that they only use or disclose such personal information in a manner consistent with the use and disclosure provisions of this Privacy Policy and applicable law.

 - We may buy or sell businesses and other assets. In such transactions, employee information is generally one of the transferred business assets and we reserve the right to include your personal information as an asset in any such transfer. Also, in the event that we, or substantially all of our assets, are acquired, your personal information may be one of the transferred assets.

 - Where required by law, by order or requirement of a court, administrative agency, or government tribunal, which includes in response to a lawful request by public authorities, including to meet national security or law enforcement requirements or in response to legal process.

 - If we determine it is necessary or desirable to comply with the law or to protect or defend our rights or property.

 - As necessary to protect the rights, privacy, safety, or property of an identifiable person or group or to detect, prevent or otherwise address fraud, security or technical issues, or to protect against harm to the rights, property or safety of GitLab, our users, applicants, candidates, employees or the public or as otherwise required by law.

 - Where the personal information is public and exempted from coverage under applicable data protection laws.

 - To seek advice from our lawyers and other professional advisors.

 - To professional advisors (e.g. bankers, lawyers, accountants) and potential buyers and vendors in connection with the sale, disposal or acquisition by use of a business or assets.

*Access to Personal Information We Collect*

To the extent access is required by applicable law, you can ask to see the personal information that we hold about you. If you want to review, verify or correct your personal information, please submit a request to the HR Department or DPO.

When requesting access to your personal information, please note that we may request specific information from you to enable us to confirm your identity and right to access, as well as to search for and provide you with the personal information that we hold about you. We may, in limited circumstances, charge you a fee to access your personal information; however, we will advise you of any fee in advance.

We reserve the right not to grant access to personal information that we hold about you if access is not required by applicable law. There are also instances where applicable law or regulatory requirements allow or require us to refuse to provide some or all of the personal information that we hold about you. In addition, the personal information may have been destroyed, erased or made anonymous. In the event that we cannot provide you with access to your personal information, we will inform you of the reasons why, subject to any legal or regulatory restrictions.

*Correction of Collected Personal Information*

We endeavor to ensure that personal information in our possession is accurate, current and complete. If an individual believes that the personal information about him or her is incorrect, incomplete or outdated, he or she may request the revision or correction of that information. We reserve the right not to change any personal information we consider to be accurate or if such correction is not required by applicable law.

*Retention of Collected Information*

Except as otherwise permitted or required by applicable law or regulatory requirements, we may retain your personal information only for as long as we believe it is necessary to fulfill the purposes for which the personal information was collected (including, for the purpose of meeting any legal, accounting or other reporting requirements or obligations) and for IT archival purposes.

Personal data for data subjects in the European Union is by default erased by GitLab after termination of your employment, with the exception of certain types of personal data, which may be stored for an extended period of time due to administrative purposes, e.g. for payment of retirement income or for giving references to other employers, or where such personal data must be retained to comply with regulatory requirements.

You may request that we delete the personal information about you that we hold, provided that we reserve the right not to grant such request if we are not required to delete personal information under applicable law. There are instances where applicable law or regulatory requirements allow or require us to refuse to delete this personal information. In the event that we cannot delete your personal information, we will inform you of the reasons why, subject to any legal or regulatory restrictions.

*Requests to Access, Delete, or Correct Information*

Please send requests to access, delete, or correct your personal information to your DPO.

Any request by you to us to delete your personal information will not result in deletion of any information submitted by you to a Third-Party provider. If you require the Third-Party to delete any of your personal information, you must contact the Third-Party directly to request such deletion.

As stated previously, there are instances where applicable law or regulatory requirements allow or require us to refuse to delete this personal information. In the event that we cannot delete your personal information, we will inform you of the reasons why, subject to any legal or regulatory restrictions.

*Resolving Concerns*

If you have questions or concerns regarding the handling of your personal information, please contact your local HR Department or DPO. Alternatively, you may report concerns or complaints to the Legal Department.

You may also anonymously report violations of policy or law using our Third-Party managed Compliance & Fraud Prevention Hotline. You can access the Hotline by going to [How to Contact GitLab's 24 Hour Hotline](/handbook/people-group/code-of-conduct/#how-to-contact-gitlabs-24-hour-hotline)

*Changes to Privacy Policy*

We may change this Privacy Policy at any time by posting notice of such a change in the revision table below. The effective date of each version of this Privacy Policy is identified the revision table.

*Security of Collected Information*

We are committed to protecting the security of the personal information collected, and we take reasonable physical, electronic, and administrative safeguards to help protect the information from unauthorized or inappropriate access or use.

**Additional Rights**
You may also have the following additional rights, subject to certain exceptions and limitations as specified in applicable law:

*Data portability*

Where we are relying upon your consent or the fact that the processing is necessary for the performance of a contract to which you are party as the legal basis for processing, and that personal information is processed by automatic means, to the extent provided under applicable law, you have the right to receive all such personal information which you have provided to GitLab in a structured, commonly used and machine-readable format, and also to require us to transmit it to another controller where this is technically feasible;

*Right to restriction of processing*

You have the right to restrict our processing of your personal information where:

 - You contest the accuracy of the personal information until we have taken sufficient steps to correct or verify its accuracy;
 - Where the processing is unlawful, but you do not want us to erase the information;
 - Where we no longer need the personal information for the purposes of the processing, but you require them for the establishment, exercise or defense of legal claims; or
 - Where you have objected to processing justified on legitimate interest grounds (see below) pending verification as to whether GitLab has compelling legitimate grounds to continue processing.

To the extent required by applicable law, where personal information is subjected to restriction in this way we will only process it with your consent or for the establishment, exercise or defense of legal claims.

*Right to withdraw consent*

Where we are relying upon your consent to process data, you have the right to withdraw such consent at any time. You can do this by contacting your local HR Department or DPO.

*Right to object to processing justified on legitimate interest grounds*

Where we are relying upon legitimate interest to process data, then you have the right to object to such processing, and we must stop such processing unless we can either demonstrate compelling legitimate grounds for the processing that override your interests, rights and freedoms or where we need to process the data for the establishment, exercise or defense of legal claims. Normally, where we rely upon legitimate interest as a basis for processing we believe that we can demonstrate such compelling legitimate grounds, but we will consider each case on an individual basis.

You also have the right to lodge a complaint with a supervisory authority, in particular in your country of residence, if you consider that the processing of your personal data infringes this regulation.

### Proprietary and Confidential Information

In carrying out GitLab’s business, team members often learn confidential or proprietary information about our company, its customers, prospective customers, or other third parties. Team members must maintain the confidentiality of all information entrusted to them, except when disclosure is authorized or legally mandated.

Confidential or proprietary information includes:
* Any non-public information concerning GitLab, including its businesses, team members, financial performance, results or prospects
* Any non-public information of a third party in GitLab's possession and under GitLab's protection
  - With the expectation that the information will be kept confidential and used solely for the business purpose for which it was conveyed and accessed solely by those who have a need to access the information in fulfilling that purpose

GitLab’s confidentiality provisions can be found in the [employee and contractor templates](/handbook/contracts/#employee-contractor-agreements), but these may vary from what you agreed to at the time of your contract. For specific information about your obligations regarding confidentiality, please reference your contract.

In addition to confidentiality obligations owed to third parties, we also have obligations to protect the personal and sensitive information of our fellow team members. Therefore, you may not access and/or disseminate any team member's personal information (i.e. address, personal phone number, salary, etc.) that the team member has not made publicly available, unless the team member has provided written permission to share the information. An exception to this restriction would be when access is a necessary function of your job duties. A violation of this obligation is considered severe and could result in disciplinary action, up to and including termination.
