---
layout: handbook-page-toc
title: "Analyst Relations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Industry Analyst Relations at GitLab

Industry Analyst Relations (IAR) is generally considered to be a corporate strategy, communications and marketing activity, but at GitLab, because of our mission that everyone can contribute, we view the industry analyst community as participants as well. The primary owner/DRI of Industry Analyst Relations (including but not limited to relationships, communication, coordination, research participation, and contractual arrangements with industry analysts and their respective firms) at GitLab is Strategic Marketing. This is in order to provide the most accurate, consistent, and comprehensive perspective on GitLab to industry analysts and to enable receiving the same from industry analysts with equal fidelity.   

Industry analysts usually focus on corporate or organizational buyers and their needs rather than on those of individual developers. At GitLab we view them as a marketing focus for those markets. They are a conduit to a different group than our traditional developer communities, although there is overlap to some degree between those communities.

If you would like to engage the analysts (e.g. research a topic, ask questions of an analyst, request analyst reports, brief an analyst) please click [here](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=AR-ResearchRequest) for the research request form. 

## How we interact with the analyst community

Examples of how we engage with analysts include:
- Schedule briefings where we update analysts on our product, our company, our future direction and strategy;
- Answer questions for analyst reports such as Gartner MQs or Forrester Waves that provide in-depth information on product features, customer references and company information;
- Use analyst reports such as Gartner MQs or Forrester Waves that feature GitLab to help enhance or clarify our story for customers and partners;
- Provide our analyst newsletter to update analysts on what GitLab is doing between briefings;
- Schedule inquiries where analysts answer specific questions we have about products, markets, or strategies we want to understand better;
- Schedule consulting days for an extended dive with an analyst into products, markets, or strategies we are working on;
- Invite analysts to participate in webinars, speaking engagements, quotes for media, or other events where an analyst presence would be beneficial; and
- Hire analyst’s market research departments to help us create, run, and interpret survey research that helps us target markets or develop products optimally.

## How we on-board a new analyst to the GitLab Analyst Relations community

- Open an issue in the Product Marketing project using the Analyst On-boarding template that details:
  - who they are
  - best fit for coverage
  - plan for briefing or an introductory inquiry
- Set them up in ARInsights, the third party analyst-tracking software we use
- Add them to the appropriate analyst newsletter list
- Add them to the forthcoming analyst heat map
- Add them to the appropriate use case profile(s)

## How we off-board an analyst from the GitLab Analyst Relations community

- Open an issue in the Product Marketing project using the Analyst Off-boarding template that details:
  - Highlight their change of coverage area or
  - Identify their reason for off-boarding
  - Identify who is their short-term/long-term replacement
  - Remove them from ARInsights, the third party analyst-tracking software we use
  - Remove them from the appropriate use case profile(s)
  - Notify the PM/PMM team of transition and change to relationship

## [How We Conduct Industry Analyst Briefings](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/ar-briefings/)
## [How We Conduct Industry Analyst Inquiries](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/ar-inquiry/)

## How we incorporate Use Cases into our industry analyst interactions 

- Research, identify and assign the key analysts for each use case
- Schedule initial round of inquiries to explore and get input into market capabilities / requirements that analysts see in each use case/market
- Schedule ongoing sessions with analysts to share internal(sales/product)/external(customers/partners) feedback and collect new analyst feedback on market reception of our use cases, new use cases, and changes in scope to existing use cases
- We also expect that conversations with analysts will help us identify and/or participate in analyst reports or other co-marketing activities that help enhance, clarify or amplify our use case messaging for customers and partners


## Responding to requests to participate in industry analyst research comparing vendors and/or their products (e.g. Magic Quadrants (MQs), Waves, etc.)

- GitLab will participate in any industry analyst comparative research (MQs, Waves, etc.) to which we are invited, provided we meet the qualification criteria. We are committed to providing our best possible answers for the questionnaires.
- The parts of the team responsible for answering the questionnaire will make this a priority. We will:

   - Present every feature in the best possible light so we have the most defensible chance at high scores.
   - Provide whatever evidence we can that illustrates our current and future capabiity to address the market needs identified by the analysts.
   - When responding to the analyst request, challenge ourselves to find a way to honestly, unhesitatingly say “yes” and paint the product in the best light possible. We carefully work to understand specific feature and capability descriptions that often make up inclusion, exclusion and even evaluation criteria. If, at first glance we think we may not or do not support a specific feature or capability as described, we will take a deeper, second look at the requirement and look to see if there is a way that our existing features solve the problem at hand.
   - Present our overall solution within the context of the entirety of a specific, defined market, meaning these exercises are bake-offs; we are being evaluated in comparison to everyone else rather than in comparison to an ideal world. We should take into account the competitive landscape as we craft our position and support it.
   - Our demo should be viewed as a further proof point of our solution and a way to demonstrate things we might not be able to illustrate or otherwise communicate the way we'd like to in the questionnaire response.
   - Even if we don't score well on the product today, we *must* score well on strategy and/or vision.

- Once the report is published, analyst relations, in conjunction with product marketing and product will decide on the overall messaging and positioning of the report. This will decide whether this is a Level 1 or Level 2 announcement and which activities are needed, including, a press release, social media, and one or more blog posts. 

- AR, PMM, digital marketing and content marketing will also decide whether or not to reprint the report. The decision will be based on:
    - GitLab's final positioning in the report
    - Whether this is GitLab's first time appearing in this report or it is a new report
    - What other assets GitLab has
    - What campaigns GitLab is running 
    - Whether this report supports a use case

 Once we decide on reprints, we may choose to create the following: 
   - An optional press release - this may be published over the wire or just to our press page.  It gives us a piece to refer to in marketing or other external pieces.
   - An optional blog or series of blogs to create discussion and interest in the results and to drive readers to the landing page where they can download the report. The goal is to attract higher up the funnel.
   - A landing page which serves as a short summary for marketing activities to point to if we decide to reprint the report.
   - A unique web page for each report that details the analysts' opinions of our relative strengths and weaknesses as well as GitLab PMM/PM's take on the results. PMM/PM will provide links to relevant epics and issues that show either how we are working to improve areas mentioned in the report or illustrate the recognized strengths. The goal is to give sales a tool to correctly position GitLab from a competitive perspective, or to provide information for customers who want a deeper dive. This page is intended to help sales with prospects who are further into the conversation or with customers who are engaging around the topic covered in the report.

## Process for responding to industry analyst research comparing vendors and/or their products (e.g. Magic Quadrants (MQs), Waves, etc.)

#### Before the questionnaire arrives:

- GitLab AR is aware of what industry analyst research comparing vendors and/or their products (e.g. Magic Quadrants (MQs), Waves, etc.) we may be asked to participate in, particularly if we have done so before.  AR should be checking with Forrester/Gartner to see if
    - A MQ or Wave or similar comparative research is likely to be repeated
    - The likely time frame for that comparative research (MQ, Wave, etc.) to begin
- Once the timeline is determined, 30-45 days in advance AR will
    - Set up an inquiry with the lead analyst(s) to discuss customer inquiries they have received on this topic over the last year and gain insight on where they think the market is at currently
    - Set up a short (30 minute) demo on the relevant part of GitLab to have them aware of our present position
- GitLab usually receives notice 1-2 weeks in advance that a Wave or MQ is forthcoming, in which they can participate.
    - With Forrester, the process may begin with a Now Tech report.  With Gartner, this process may begin with a Market Guide. It may also begin earlier, with an analyst asking for a meeting to discuss what we see in the market.  This happened for example with Chris Condo of Forrester in 2019 with CI.
    - AR works with PMM team to determine lead PMM, lead TMM and lead PM for the project.
    - AR notifies Customer Reference that a request is imminent.
    - If it is a Gartner MQ, AR will work with the Peer Review team to make sure GitLab reviews are being collected by the Peer Insights team or to get that activated, and coordinate with Peer Review and Customer Reference to funnel references to the appropriate Peer Insights page.

### When the questionnaire arrives:

- AR replies to the industry analyst firm, acknowledging receipt of invitation and agreement to participate, and indicates key participants (AR Manager and lead PMM.)
- AR responsibilities:
    - Create an issue for the report, a place to keep all links, due dates, supporting materials, related issues, and overall conversation
    - Create a slack channel dedicated to this report, inviting all relevant members to the channel, pinning the issue in the channel
    - Forward any Customer Reference material to the CR Manager
    - Send out an invite for any kickoff call that the analyst may run related to this report and create a notes page for the meeting, adding that to the issue
    - Coordinate all activities for deadlines
    - Manage all correspondence between the industry analyst firm and GitLab, including an questions or clarifications, any requests for extension for demo or customer references, any conversation on choosing dates for demos/briefings, and submission of final questionnaire and customer references
    - Coordinate with team for demo/briefing time slot
    - Work with finance and sales ops, for company information
    - Support the team in finding additional help where necessary
    - Start and track issues for marketing, including a blog, changes to the web page, and gating the asset
    - Remind the team of all due dates and hold periodic meetings if necessary to discuss progress and potential issues
    - Create or update the report web page and make it available to PMM/PM to add their commentary - get it approved by the analyst company
    - Work with corporate marketing and PR to decide if the results should be used in a press release or campaign and if reprint rights are required

- Product Marketing Manager (PMM) responsibilities:
    - read all information provided by AR in the issue
    - attend all meetings with the analysts related to this report
    - own responsibility for coordinating, collating, and organizing all input requirements including:
        - draft questionnaire feedback
        - final questionnaire response
        - briefing presentation to analysts
        - demo for analysts
        - follow up questions for analysts
    - drive the briefing to the analysts
    - coordinate with Product leadership/CEO/others on final version of the questionnaire   

- Technical Marketing Manager (TMM) responsibilities:
    - read all information provided by AR in the issue
    - attend all meetings with the analysts related to this report
    - work with PMM and PM in creation of demo and briefing
    - drive the demo during the demo and briefing time slot

- Product Management (PM) responsibilities
    - read all information provided by AR in the issue
    - attend all meetings with the analysts related to this report
    - provide feedback to PMM on draft questionnaire and demo
    - place the questionnaire and briefing/demo at the top of the work priority list
    - provide first pass answers to the questionnaire to PMM
    - work with PMM to finalize all answers to the questionnaire

### When the Factual Review (Fact Check) arrives:

- AR shares the draft report with the PMM/PM/TMM team to check for technical accuracy
- PMM takes lead in verifying/correcting technical accuracy and the initial rewrite suggestions
- PM & TMM provide corrections for technical accuracy
- AR works with PMM for company accuracy issues
- AR submits final corrected version to analysts

### When the courtesy copy of the report arrives (when we know final positioning):
- AR schedules a meeting with PMM/PM to discuss key messages and overall positioning
    - basis for blogs
    - basis for sales enablement
    - basis for report web page
    - basis for announcement level and reprint rights (may require additional budget to be allocated for reprint rights)
- **Social Media:** AR will follow the [social marketing guidelines](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/#requesting-social-posts-) to create appropriate social media posts
- **Blog posts and PR:** will follow the [Request for Announcement process](https://about.gitlab.com/handbook/marketing/corporate-marketing/#requests-for-announcements) to determine if this is a Level 1 or Level 2 announcement, get blogs and press releases started
- **Landing page:** If reprints will be contracted, then AR will initiate a Landing Page with the [gated content request issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new#?issuable_template=gated-content-request-analysts-mpm)
- PMM writes and submits a blog post on the report - following the [blog process for time sensitive announcements](https://about.gitlab.com/handbook/marketing/blog/#process-for-time-sensitive-posts)
- AR will follow the process to [create a new web page](https://about.gitlab.com/handbook/marketing/website/#creating-a-new-page). Once that page is created:
    - AR adds the preliminary analyst information to the top section.
    - PM/PMM adds content on the GitLab response
    - AR gets the webpage link to the industry analyst firm for citations approval
- PMM [requests a slot on the Sales Enablement calendar](https://about.gitlab.com/handbook/sales/training/sales-enablement-sessions/#to-request-new-sales-enablement-sessions) to discuss the report results and the web page with sales


### Once the report is published by the industry analyst firm:

- AR posts the results/summary on the What's Happening at GitLab page with either an internal link or a link to the reprint
- AR sets up an inquiry with the industry analyst firm to discuss the results of the report and answer questions for PM/PMM/TMM
- AR adds the link to the spreadsheet of sales report
- AR adds the report to the appropriate Use Case resource page(s)


## Accessing analyst reports

Most analyst companies charge for access to their reports.

-   If GitLab purchases reprint rights to a report, then that link will be available here, on the [Analyst Relations web page](/analysts/), and on the relevant product page. Reprint rights are the rights to share the link to the report - these generally last six months to one year.
-   GitLab maintains relationships with some analyst companies that provide us with access to some or all of their relevant research. These reports are for internal use only and sometimes limited to named individuals.  Those reports are generally kept in an internal [GitLab folder for analyst relations](https://drive.google.com/drive/u/0/folders/1oFmtmoXsbjMb6IuPIgIIZ-MG-tLfOjpw). If you are a GitLab team-member and you need access to a particular report, please reach out to [Analyst Relations](mailto:analysts@gitlab.com) and I'll help you find the research you need.

## Analyst reports that can help you deepen your knowledge
As part of XDR and Sales Enablement, some analyst reports are educational - they can help you build your understanding of the market. We've collected some of those articles to share with you here. These are behind the firewall and for use of employees where they have access rights. If you have any questions on access, please contact [Analyst Relations](mailto:analysts@gitlab.com).

### Forrester Total Economic Impact<sup>TM</sup> of GitLab
In 2020, GitLab commissioned Forrester Research to create a Total Economic Impact (TEI) study on GitLab. According to Forrester, "The TEI methodology quantifiably measures the business value of an IT decision or project. TEI is composed of four main elements with associated tools and methodologies for quantification. Individually, each provides only a piece of the decision-support puzzle. Together, they provide a holistic tool for assessing and justifying IT investments."

In Forrester's methodology, the four main elements of the TEI are:
- Benefits -  measure financial impacts of positive business results. 

- Cost - measures the negative impact on IT and the business.

- Risk - quantifies the impact of inherent uncertainties. 

- Flexibility - monetizes the value of future options. 

[Click here](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/forrester-tei/) to learn more about the TEI, how to access it, and how to use it with customers and prospects.

[Additional reports to aid Sales can be found here.](/handbook/marketing/product-marketing/analyst-relations/sales-training/)

### Which analyst relations team member should I contact?

  - Listed below are areas of responsibility within the analyst relations team:

    - [Joyce](/company/team/#joycetompsett), Analyst Relations Manager
    - [Ryan](/company/team/#ryanragozzine), Analyst Relations Manager
    - [Colin](/company/team/#colinwfletcher), Manager, Market Research and Customer Insights
    - [Ashish](/company/team/#kuthiala), Senior Director, Strategic Marketing
