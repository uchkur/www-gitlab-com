---
layout: handbook-page-toc
title: Growth Marketing Handbook
description: Growth Marketing Handbook
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---
## On this page 
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

# Growth Marketing Overview
{:.no_toc}
---
## Goals

### Objective

Meet/Exceed inbound Marketing Qualified Leads ([MQL]((/handbook/marketing/marketing-operations/#lead-scoring-lead-lifecycle-and-mql-criteria))) generation targets 

### MQL Targets

| Fiscal Quarter | Large |  Mid-Market |  SMB |
| ------ | ------ |
| Q1FY21 | 5,000 | 3,000 | 11,947 |
| Q2FY21 | 5,500 | 3,300 | 16,157 |
| Q3FY21 | 6,000 | 3,750 | 19,931 |
| Q4FY21 | 6,500 | 4,000 | 21,925 |

*Targets are based on the original FY21 plan, and may be adjusted through the course of the year. GitLab internal team members can find this information in "FY21 FINAL Marketing: SSOT & Budget"*

### Key Results

To support MQL generation in FY21, Growth Marketing is responsible for:
* Increasing GitLab.com website traffic 20% YoY on an ongoing basis
* Increasing Conversion of Marketing Website visitors to MQLs by 20%

### Deliverables

[Growth Marketing Q3FY21 OKRs](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/21)

### Q3 FY21 Initiatives

- [Marketing Website Refresh](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/21#website-refresh-strategy-httpsgitlabcomgroupsgitlab-commarketinggrowth-marketing-epics32)
- [Up-Level Content Marketing Programs](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/21#content-marketing-strategy-growth-marketingglobal-content-epics4)
- [Deliver Cohesive Brand Strategy & Guidelines](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/-/epics/21#brand-strategy-httpsgitlabcomgroupsgitlab-commarketinggrowth-marketing-epics60)

## Teams
Growth Marketing is comprised of these Marketing Teams. Visit their handbooks to learn more. 
* [Global Content](/handbook/marketing/growth-marketing/content/#what-does-the-global-content-team-do)
* [Brand and Digital Design](/handbook/marketing/growth-marketing/brand-and-digital-design/) 
* [Inbound Marketing](/handbook/marketing/growth-marketing/inbound-marketing/)

## Stable Counterparts
Growth Marketing works across teams and one way we stay efficient is by identifying and working with stable counterparts across GitLab. Identifying [stable counterparts](/blog/2018/10/16/an-ode-to-stable-counterparts/) help us know where to turn when we work on specific processes and share knowledge from our team.

### Current Stable Counterparts
* Shane Rice <> Dara Warde (Marketing Ops)
* Danielle Morrill <> Hila Qu (Product Growth)
* Michael Preuss <> Jean du Plessis (Static Site Editor)

## Communication

### Meeting Cadence

Most of our team meetings are recorded, and can be viewed on GitLab Unfiltered in the [Growth Marketing playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp0T5rN49dxNeJ5uuJ73wMl).

* Monday team leads kickoff to review priorities and blockers
* Friday full team results recap & demo day to share completed work

### The Handbook 
Is our single source of truth (SSoT) for processes and relevant links

*  All processes at the Growth Marketing team level will be put in the Growth Marketing Overarching Handbook. Individual team should link back to these SSoT sections to avoid confusion. 
*  Individual teams within Growth Marketing will create their handbook sections for specifics to their teams
*  The handbook will be iterated on as we establish and test processes

### When you submit an issue 
You can expect these communications/notifications (either through GitLab or in a comment)

*  You will be provided with specific templates to help you input relevant information
*  Those issues will be vetted at the beginning of each week. The issue will either be: 
   *  Moved into `mktg-status::WIP` or;
   *  A comment will be added for what additional information is needed
*  The issues moved to `mktg-status::WIP` are then put into a sprint and assigned to a week sprint based on priorities, resources, weights and assignment loads
*  The `Assignee` will comment that they have received your Issue and when you can expect more information (ex: "On it. Will respond within the week on timeline")
*  If needed, the Issue Brief will be broken into a Project Epic with relevant issues and those issues re-assigned to the relevant DRIs as `Assignee(s)` 
*  A timeline  will be provided in that Project Epic or individual issue and the due date will be adjusted accordingly.
*  When feedback is needed the `Assignee(s)` will comment in the issue(s) asking for your input. They will be as specific as possible with what they are looking for comments on. 
*  Final deliverables will be added in the appropriate form (file, link to repository, web link, etc.) and the Assignee(s) will @ you
*  When final deliverables have been completed the `Assignee(s)` will close the issue. If additional items are needed please open a new issue and relate to each other.  
*  If an issue needs to be moved to the next milestone the `Assignee` will comment in the issue with the reason (ex: problems with testing)

## The Marketing Website

Everyone can [contribute the GitLab Marketing website](/handbook/git-page-update/). 

To make Marketing as inclusive as possible to all contributors, including those who have never written an MR (Merge Request) before, we also provide Marketing support across GitLab in the following ways:

### Requesting Support
Please fill out one of these Issue Templates to request support. Please note, if these are not filled out we won't have the proper information for us to support your request.

#### Website Issue Templates
* [Requesting a new webpage](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-new-website-brief)
  * Use this template to request new webpage on logged-out gitlab.com, or `about.gitlab.com`
  * Do NOT use this for logged-in (In-App) gitlab.com or the Handbook
  * For more information on this process click [HERE](LINK)
* [Requesting an update to an existing webpage](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-update-webpage-brief)  
  * Use this template to update an existing webpage on logged-out gitlab.com, or `about.gitlab.com`
  * Do NOT use this for logged-in (In-App) gitlab.com or the Handbook
* [Requesting homepage promotion](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion)
   * Use this template to request to be put on the content calendar for homepage promotion
   * Note: this is a request and must be approved 
   * If this is for a campaign, please put in the request as part of the initial campaign distribution plan 
   * For more information on this process click [HERE](/handbook/marketing/growth-marketing/content/index.html#homepage-promotion-guidelines)
* Report a bug on about.gitlab.com](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-bug-report)
   * Please supply all information if possible. The questions might not seem relevant but if we can't reproduce the bug then we can't fix it.
  
#### Brand and Design Issue Templates
  * [Requesting a new design](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-design-general)
    * Use this template to request a new design of a single asset
    * Do NOT use this template for complicated campaign design support
    * Do NOT use this template for brand reviews
  * [Requesting Brand review](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-brand-review)
    * Use this template to request a brand or design review 
    * Do NOT use this template for requesting new assets or designs
  * [Requesting design concepting for integrated campaigns](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-design-concepting-integrated-campaign)
    * Use this template to request design concepting for an integrated campaign
    * You may also use this template for a single-group campaign, but the ROI but be sufficient to utilize the resources
    * You must also complete a [Design Requirements Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=requirements-design-integrated-campaign) for final files to be delivered.
    * Design concepting will be completed before the design requirements file is picked up. 
  * [Design requirements](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=requirements-design-integrated-campaign)
    * Use this template for capturing all the design requirements for an integrated campaign
    * Use this issue along with a [Design Concepting Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-design-concepting-integrated-campaign)

#### Inbound Marketing templates
  * [Request a redirect](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/issues/new?issuable_template=set-up-a-new-redirect)
  * [Request a Hotjar heatmap](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/issues/new?issuable_template=hotjar-heatmap-request)
  * [Request keyword research](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/issues/new?issuable_template=keyword-research-request)
  * [Request on page optimization research](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/issues/new?issuable_template=on-page-optimization)

#### Image Guidelines

When requesting support we ask that you follow these guidelines when providing logos or other images.

[Image guidelines](/handbook/marketing/growth-marketing/brand-and-digital-design/image-guidelines/)

# How we use GitLab 

## Boards
Overarching Marketing Growth Boards

#### [Triage Board](https://gitlab.com/groups/gitlab-com/-/boards/1761578?&label_name[]=mktg-growth&label_name[]=mktg-status%3A%3Atriage) 

*  **GOAL:** Weed out issues that superfluous or don’t contain extra information 
*  **ACTIONS:** Move to either
   *  Move to either `mktg-status:WIP` which puts into the Sprint cycle or;
   *  [mktg-status: blocked] and comment why it is being blocked

#### [Sprint Board](https://gitlab.com/groups/gitlab-com/-/boards/1761580?label_name[]=mktg-growth&label_name[]=mktg-status%3A%3Awip)
*  **GOAL:** Assign issues to milestones to be worked on then based on due dates, weights, priorities, resources
*  **ACTIONS:** 
   *  Move to a specific week taking into consideration the above 
   *  Backlog if:
      *  It doesn’t fit into current goals or priorities
      *  We don't have the time or resources currently
      *  It is just a cool idea (some of these may be labeled for future review for upcoming strategies & tactics) 
   *  Comment why it is backlogged
   *  Never backlog PR issues without prior discussion

   > HANDOFF: This is where the handoff to Growth Marketing Teams takes place to run their processes

#### [Growth Marketing Leads Overview](https://gitlab.com/groups/gitlab-com/-/boards/1787553?milestone_title=%23started&&label_name[]=mktg-growth&label_name[]=mktg-status%3A%3Awip)
* **GOAL:** An overview of the work on each of the Growth Marketing Teams during the current Milestone Sprint
* **ACTIONS:**
  * Review and help prioritze
  
## Labels
Meanings and usage

*  `mktg-growth`
   *  Denotes it could be part of the Growth Marketing Teams scope. 
   *  Used to help pull in boards
   *  Is not a label to denote the status of an issue
*  `mktg-status::triage`
   *  Universal marketing label
   *  Is used to denote the status of an issue as being reviewed to go into the working pipeline
   *  Does not denote the timeline it will be worked on
*  `mktg-status::WIP`
   *  Universal marketing label
   *  Is used to denote the status of an issue as having been reviewed for all details needed and will go into the working pipeline
   *  Does not denote the timeline it will be worked on
*  `mktg-status::blocked`
   *  Universal marketing label
   *  Is used to denote the status of an issue as not having enough information to proceed
   *  Does not denote the timeline it will be worked on
   *  This should ONLY be used in the Triage process or if an issue becomes so blocked it needs to be re-scheduled. 
      * If an issue is blocked it goes back into the Triage process and is scheduled from there
*  `mktg-status::review`
   *  Universal marketing label
   *  Is used to denote the status of an issue as in the peer-review process.
   *  Does not denote the timeline it will be worked on
*  [Group Labels] `design`, `web-analytics`, `mktg-website`, `mktg-content`, `mktg-inbound`
   *  Denotes it could be part of a Growth Marketing Group scope
   *  Is not a label to denote the status of an issue
   * [Individual Team Labels] `content marketing`, `editorial`, `digital-production`, `Brand and Digital Design`
      * Denotes it could be part of Growth Marketing Group Team's scope
      * Is not a label to denote the status of an issue
*  `MG-Support`
   *  Denotes if this is support provided to another team outside of Growth Marketing
   *  Will be used to help quickly view how many issues Growth Marketing supported each year to provide as references for budget and resources requests.  
*  `MG-Review-Future`
   *  Will be reviewed by Marketing Growth for potential future strategy
*  `sprint-addition`
   * This issue was not originally in the sprint and was added after the sprint started

## Milestones
Meanings and usage

* `Fri: MONTH DAY`
   * Denotes which week an issue will be worked on 
   * Worked on = will be broken down into smaller Issues or Issue will be completed and closed
* `MG-Backlog:Goal` 
   *  Denotes that this not fit into current goals - can be reviewed later for informing future goals
   *  Does NOT mean the issue request is cancelled
*  `MG-Backlog:Time/Resources`
   *  Fits into goals, but;
   *  Needs to be reviewed later because we don't have the time or resources during this review
   *  Does NOT mean the issue request is cancelled
*  `MG-Backlog:General`
   *  Denotes general ideas that are cool ideas but doesn't move the boat forward. Will be reviewed when there is extra time or resources.

## Epics
This is how we use Epics to organize our projects. We aim to include the highest level information at the top level and specific information in lower-level epics. This is to 
keep a SSoT and avoid duplicative placement of information that would need to be updated in multiple places

* **Top Level Epic**
  * Quarterly OKRs
  * Links to the yearly strategy that contains themes
* **Second Tier Epic**
  * Stores the team or channel strategy/improvements broken into phases
  * Includes
    * Current state
    * Stakeholders
    * Metrics - Baseline and Target
    * Desired state - Key audience + Intent and Key company goals
    * Improvement Roadmap - broken out by quarter if it will cross multiple
    * Phases - per quarter and includes: 
      *   The Problem
      *   The Solution 
      *  Why this Effort
* **Third Tier Epics**
  * Epic per phase - one epic per phase
  * Contains:
   * Scope of project
   * Roles & Responsbilities Chart if different than the parent epic
   * Relevant Documents
   * High-level epic production only - All production for individual issues should be placed in those issues to avoid redundancy 
* **Fourth Tier Epics**
  * MVCs for project - one epic per MVC  
  * Contains: 
    * Scope of MVC
    * Relevant Documents
    * High-level epic production only - All production for individual issues should be placed in those issues to avoid redundanc
* **Fifth Tier Epics**
  * Production issues 
  



## Issues 

### Issue Types
This is how we work with Issues and their scope
#### External
(outside of Growth Marketing)
**REQUEST ONLY ISSUE**
*  This is for complicated projects (example: campaign). 
*  This type of Issue is submitted by a team OUTSIDE of Growth Marketing. 
*  This serves as a brief to us. 
*  For each type of request we have a corresponding  **Request [Type] Issue Template** that should be filled out to begin the process. 
     * [Requesting Support](/handbook/marketing/growth-marketing/#requesting-support)
* Actions taken on issue: 
  * Reviewed for all information and label changed to `mktg-status::WIP` if all there or `mktg-status::blocked` if not all info is there
  * Assigned to a team member to execute on
  * Assigned to a Sprint week to be worked on (worked on= broken down into Production Issues)
  * Issue is added to a child Epic and execution issues are made for creating
  * Assignee will comment and close the Request Issue and deliver the work in the Child Epic

**REQUEST AND PRODUCTION ISSUE**
* This is for a simple ask (example: single asset)
* This type of Issue is submitted by a team outside of Growth Marketing and will contain relevant information for the project AND;
* Execution Items/Production Checklists for the Growth Marketing Teams.
* For each type of request we have a corresponding **Request [Type] Issue Template** that should be filled out to begin the process. 
* Actions taken on issue: 
  * Reviewed for all information and label changed to `mktg-status::WIP` if all there or `mktg-status::blocked` if not all info is there
  * Assigned to a team member to execute on
  * Assigned to a Sprint week to be worked on (worked on = deliverables created)
  * Assignee will follow the production checklist and deliver work within the original Issue

#### Internal
(inside of Growth Marketing)

**PRODUCTION ISSUE**
 * This is an Issue we use internally to create and execute on the request/brief. 
 * These are only used by people inside of Growth Marketing 
 * For each type of request we have a corresponding  **Production [Type] Issue Template** that should be filled out to begin the process. 
 * The teams will deliver the individual pieces to these Issues, but not the completed ask. That should be delivered into the Child Epic created


## Issue Templates

#### External: 
(outside of Growth Marketing): 

**Issue Author**
* Pick template based on Handbook directions 
  * Template tags will be automatically added:
    * `mktg-growth`
    * `mktg-status::triage`
    * The relevant Group Label: `design`, `content marketing`, `mktg-website`, or `mktg-analytics`
    * `MG-Support` 
  * Template contains specific information to help fill out including directions for picking a due date
  * Template has auto-assignees
  * Links to Handbook page for additional information

#### Internal: 
(inside of Growth Marketing): 

**Assignees of Issues:**
*  Pick template for breakout of brief
  *  Template tags: 
     *  Keep existing above EXCEPT if we start the project then we remove `MG-Support`
     *  Add your team's process tags
* If the ask (External brief Issue) requires one ask:
  * Re-assign as need, make sure it is in a sprint week and follow documented processes
* If the ask (External brief Issue) requires more than one ask:
   * Create an Epic to hold project
   * Copy and paste markdown into Epic Description and use header: “External Brief”
   * Associate Issue with Epic 
   * Comment Close Issue that the project epic has been created
   * Build out necessary Issues in project, Associate with Epic, assign Issues and update Epic Description as needed with additional brief information 


# Process

## Weekly Process

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/xeFP0-Kdq-M" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

**Beginning of week:** 

*  Geekbot Check-In
* Triage board is vetted and items moved (see above for details on actions)
*  Growth Team Leadership meeting: 
   *  Discuss priorities for the week, Danielle relays exec and PR info, we decide if any project, company or larger Mkrg priorities are bubbling up that would make us reconsider priorities 
   *  Groom issues into weeks using the Sprint Board 

> HANDOFF: This is where the handoff to Growth Marketing Teams takes place to run their processes

*  Growth Marketing Leads meets with their teams (anyway they want: meeting, slack, async, geek bot) and relays that information
*  Teams: Assign (if not auto-assigned) Issues, Projects, Tasks and follow their process 


**End of week:**
*  Confirm if everything has been closed and move to next milestone sprint if not completed
*  Comment in issue why it was moved to the next milestone (ex: person x out sick, complications with testing, etc.)

## Quarterly Process 
* Epics for the quarterly OKRs are created and links are added to to the yearly strategy in the handbook
* Child epics are created per the [epic structure](/handbook/marketing/growth-marketing/#epics) listed above
## Yearly Process 
Work in Progress
* Strategy for the year is documented in the handbook

----

Return to the main [Marketing Handbook](/handbook/marketing/).

