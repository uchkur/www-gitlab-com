---
layout: markdown_page
title: "Michael Preuss' README (Senior Manager, Digital Experience)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Michael Preuss’ README

Hi 👋 my name is Michael Preuss. My last name is pronounced like Rolls Royce ... Royce ... Proyce ... Preuss. I know, it's German 🤷‍♂️ I'm the Senior Manager, Digital Experience at GitLab.

- [GitLab Handle](https://gitlab.com/mpreuss22)
- [Team Page](https://about.gitlab.com/company/team/#mpreuss22)
- [My Website](http://assemblydigital.com/)
- [LinkedIn](https://www.linkedin.com/in/michaelapreuss/)

## About Me

- 🤙 I'm born and raised on the West Coast
- 🇨🇦 I live in North Vancouver, BC
- 🔮 A psychic predicted I'd work in digital
- ❤️ I love what I do
- 📖 I'm a lifelong learner with a growth mindset
- 😀 I'm enthusiastic and persuasive
- 🦉 When organizing or planning, I'm both alert and easily distractible
- 😋 Life is too short for bad food, bad coffee, bad wine
- 🍔 Cheeseburgers are a way of life to me
- 👨‍👩‍👦‍👦 I'm a husband and father
- 🐕‍🦺 I have a Bouvier des Flandres named Sebastian
- 🏀 I roll deep with NBA knowledge
- 🏎 I support the Scuderia
- 🚴‍♂️ I ride my bike as much as I can (which fluctuates wildly!)

### Myers-Briggs Personality type:

🔗[**“The Protagonist”** (ENFJ-A)](https://www.16personalities.com/enfj-personality)

**Individual traits:**

Mind: **51% Extraverted**

Energy: **59% Intuitive**

Nature: **57% Feeling**

Tactics: **54% Judging**

Identity: **65% Assertive**

### DiSC Profile

🔗[**Di:** Driver](https://www.crystalknows.com/disc/di-personality-type/)

**Strengths**

- I'm quick, independent, and firm when making decisions: ⏱️ [Efficiency](https://about.gitlab.com/handbook/values/#efficiency)
- I take a goal-oriented approach to assigning work: 🌐 [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion)
- I'm comfortable with responsibility and ownership over results: 📈 [Results](https://about.gitlab.com/handbook/values/#results)

**Blind spots (I'm working on these, please call me out if you're affected by any!)**

- I can be impatient when providing detailed instruction: 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration)
- I can take too much responsibility for results that aren't in my control: 📈 [Results](https://about.gitlab.com/handbook/values/#results)
- I can provide insufficient structure for people who need definition: 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration)

### 10 Books On My Bookshelf

1. Thinking Fast and Slow
2. Tribal Leadership
3. What Got You Here, Won't Get You There
4. Thanks for the Feedback
5. Good To Great
6. Start with Why
7. Seeing Around Corners
8. The Signal and the Noise
9. Radical Candor
10. Principles

## Working With Me

### Overview

- I'm **data informed** and **performance driven** 📈 [Results](https://about.gitlab.com/handbook/values/#results) & 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration) & ⏱️ [Efficiency](https://about.gitlab.com/handbook/values/#efficiency)
- I'm responsive and independent
- I'm objective, yet sympathetic
- I'm predominantly idealistic, I value cooperative effort and the concepts of trust, loyalty and team spirit 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 👁️ [Transparency](https://about.gitlab.com/handbook/values/#transparency) & 📈 [Results](https://about.gitlab.com/handbook/values/#results)
- I think and reason in terms of intangible benefits, and prefer to minimize face-to-face, competitive rivalry 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 📈 [Results](https://about.gitlab.com/handbook/values/#results) & ⏱️ [Efficiency](https://about.gitlab.com/handbook/values/#efficiency)
- I'm warm, yet practical 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 🌐 [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) & 👁️ [Transparency](https://about.gitlab.com/handbook/values/#transparency)
- I'm flexible and enthusiastic 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & ⏱️ [Efficiency](https://about.gitlab.com/handbook/values/#efficiency) & 🌐 [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion)
- I'm naturally friendly and, as a rule, am accepting of people 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 🌐 [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion)
- I reserve the right to change my mind when given new information 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration) & 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 📈 [Results](https://about.gitlab.com/handbook/values/#results)
🔗[https://en.wikipedia.org/wiki/Bayesian_probability](https://en.wikipedia.org/wiki/Bayesian_probability)
- "Strong opinions weakly held." 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration) & 📈 [Results](https://about.gitlab.com/handbook/values/#results) & ⏱️ [Efficiency](https://about.gitlab.com/handbook/values/#efficiency)
🔗[https://www.saffo.com/02008/07/26/strong-opinions-weakly-held/](https://www.saffo.com/02008/07/26/strong-opinions-weakly-held/)
- I’m a lateral thinker who applies multiple mental models to avoid blind spots. I strive to make sure the right questions are being asked 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration)  & 🌐 [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) & 📈 [Results](https://about.gitlab.com/handbook/values/#results)
🔗[https://fs.blog/mental-models/#general_thinking_concepts](https://fs.blog/mental-models/#general_thinking_concepts)
- I'm an advocate for Design Thinking 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration) & ⏱️ [Efficiency](https://about.gitlab.com/handbook/values/#efficiency)
🔗[https://uxdesign.cc/the-business-of-design-thinking-2c73b388e444](https://uxdesign.cc/the-business-of-design-thinking-2c73b388e444)
- One of the principles I live by is: "better is always better" 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration)  & 📈 [Results](https://about.gitlab.com/handbook/values/#results) & ⏱️ [Efficiency](https://about.gitlab.com/handbook/values/#efficiency)
- Behavioral economics can help us connect qualitative and quantitative information
🔗[https://en.wikipedia.org/wiki/Thinking,_Fast_and_Slow](https://en.wikipedia.org/wiki/Thinking,_Fast_and_Slow)
- I believe in the power of the "3 Cs":
    - **Co-create:** Our purpose is to engage, work with, and empower users, our clients and our team. Our goal is to generate ideas that lead to the best solutions to the challenge we're focusing on.
    - **Collaborate:** Good ideas come from everywhere. We start from yes, plus up each others ideas, and live into possibility.
    - **Coordinate:** Amazing products are the result of teamwork. We need to effectively communicate as a team and coordinate our efforts to deliver a product to our client that their customers will love.
- Clay Christensen's Jobs To Be Done framework is an effective way for us to frame what matters
🔗[https://hbr.org/2016/09/know-your-customers-jobs-to-be-done](https://hbr.org/2016/09/know-your-customers-jobs-to-be-done)
- The Experience Economy was written in 1998 and I believe it's more relevant now than ever
🔗[https://hbr.org/1998/07/welcome-to-the-experience-economy](https://hbr.org/1998/07/welcome-to-the-experience-economy)
- I'm purpose driven
🔗[https://www.bcg.com/en-ca/featured-insights/how-to/purpose-driven-business.aspx](https://www.bcg.com/en-ca/featured-insights/how-to/purpose-driven-business.aspx)
🔗[https://hbr.org/2018/07/creating-a-purpose-driven-organization](https://hbr.org/2018/07/creating-a-purpose-driven-organization)

### Typical Behaviours

**Self Development:** I have a positive attitude toward personal growth and development. I'm motivated to make contributions and exercise professional or managerial responsibility. 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration) & 📈 [Results](https://about.gitlab.com/handbook/values/#results)

**Corporate Adaptability:** I'm positively committed to relationships and organizational goals necessary for advancing in the organization and corporate structure / culture. I'm dedicated to and identify with the corporate initiatives that require significant individual and team commitment. 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration) & 📈 [Results](https://about.gitlab.com/handbook/values/#results)

**Social Adaptability:** I'm adaptable to people, social situations, corporate and legal rules. I have a positive attitude toward others, and an ability to withstand extended stress. I meets unexpected changes in an optimistic, tolerant manner, and extend trust to others easily. 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 🌐 [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) & 👁️ [Transparency](https://about.gitlab.com/handbook/values/#transparency)

**Social Responsibility:** I value and support social conventions in my own social groups. I provide and support stability in work, family, legal and social relationships. 🌐 [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) & 👁️ [Transparency](https://about.gitlab.com/handbook/values/#transparency)

**Detail:** I have the ability to give myself wholly to new things or to follow the pattern, whichever is most appropriate. ⏱️ [Efficiency](https://about.gitlab.com/handbook/values/#efficiency) & 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration) & 📈 [Results](https://about.gitlab.com/handbook/values/#results)

**Linear-ish:** My preference is for combining organized and systematic methods with a flexibility of approach. 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration) & 📈 [Results](https://about.gitlab.com/handbook/values/#results)

**Conceptual:** I utilize abstract information, experience, intuition, and knowledge to find fresh and imaginative solutions. 🤝 [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) & 👣 [Iteration](https://about.gitlab.com/handbook/values/#iteration) & 🌐 [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) & 👁️ [Transparency](https://about.gitlab.com/handbook/values/#transparency) & 📈 [Results](https://about.gitlab.com/handbook/values/#results)

## Communication Style

- I use candor well and am sensitive to the feelings of others
- I'm decisive and can synthesize information quickly
- I'm objective with a preference for frank and direct relationships.
- I find it easy to come to the point without beating around the bush
- I'm invasive, we're teammates and don't need to share anything we don't feel like
- I enjoy debate and active, participative discussion
- When giving or accepting direction, I'm self-directed and flexible, resisting close control and rigid schedules

## Management Style

#### [Servant Leader](https://en.wikipedia.org/wiki/Servant_leadership)

### What I Do

- Strive to inspire others
- Enable people to achieve their full potential
- Seek innovation and generate psychological safety by fostering a climate of discovery, questioning, and exploration
- Build shared understanding and meaning
- Communicate clearly

### How I Do It

- Experimentation, learning, and iterating
- Focus on results
- Utilize Humour
- Keep teams in high spirits
- Communicating purpose
- Simplifying the complex

### One-on-Ones

- These are your meetings, I want to listen and am comfortable in uncomfortable silence
- My preference is shorter and more frequent meetings, let me know if that works best for you
- We'll start with weekly 25min meetings and potentially shift to biweekly 45min meetings once we're in flow
- Please come prepared with 4-5 things you'd like to discuss
- Help me calibrate by leadership style by sharing with me what I should be doing more of/less of (I have thick skin, don't worry 😀)
- Please do not feel you have to wait for a 1:1 to bring anything up, my door/calendar is always open

## Coaching Me

- Provide me with opportunities for one-on-one discussions
- Express appreciation personally but without excessive sentiment
- Provide some freedom of action balanced with inclusion in group activities and plans
- Allow me to use initiative and autonomy; impose a minimum of routine
- Keep debates controlled and positive
- Criticize and correct me gently, and be reassuring
- Utilize my strengths; avoid exposing my weaknesses

## Conclusion

Life is short, we're all trying our best, so let's make awesome sh*t together.