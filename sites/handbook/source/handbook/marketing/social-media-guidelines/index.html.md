---
layout: handbook-page-toc
title: "Team Member Social Media Guidelines"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

This document is a guide for GitLab team members to produce better social media on your own channels.

You are personally responsible for the tweets, likes, and replies you post on social media while representing GitLab. Everything you publish is publicly viewable and will be available for a long time even if redacted. Be careful and thoughtful when representing.

Remember that you represent GitLab and our culture. When commenting on posts please keep in mind: "Don't argue but represent".

## Tips for Writing Your Own Posts
**Twitter Length**\
If your copy is on the longer side, try to break it up visually into paragraphs or one-liners, even using emojis/bullets. If you have something longer than 280 characters, like an anecdote or a schedule, consider composing a thread. [Learn how to make a Twitter Thread here](https://help.twitter.com/en/using-twitter/create-a-thread).

**LinkedIn Length**\
You aren't bound by character count the same way you are on Twitter, so you can think about LinkedIn posts as mini blog posts. This is especially effective if you have a really good anecdote that you think might complement the asset you're promoting. However, keep in mind that, unless it's *really* interesting, people won't click `more` on your post (the only way to read an entire post longer than a few lines), so keep it on the short side unless you have a great hook above the `more` button.

Longer posts are easier to read if you have a lot of paragraph breaks, so feel free to be creative with your formatting - one-liners, emojis, bullet points, etc. can help break up your text visually.

**Tone**\
That said, do sense check your copy. The most important thing is that it sounds like you, a human being. Overly contrived posts generally do not do well. If loading up your post with emojis isn't your style, don't force it. 

**Visuals**\
Add an image or video! This makes a huge difference for impressions (and clicks), so it's worth the hassle. Feel free to experiment if you have an idea to personalize it in a relevant way.

**Style**\
Don't worry about writing posts that sound like news or have an editorial perspective, the brand channels will have that covered. You should write like you're at #GitLabCommit and you're learning/loving/doing something new and fun - personalize the message. 

Instead of sharing a quote from a speaker, consider sharing the quote and adding why it personally resonated with you.

**Make Links Trackable**\
In order for us to measure the traffic you're driving, it is paramount that you add a tracked link. If you retweet GitLab posts, you're already safe. `The links we provided above in the "Grab 'n Go" section are prefixed with the UTMs needed, so you can copy + paste them.`

## A Thought on Arguments

Many of us have seen (or have been a part of) a conversation on a social channel that went way too far down the hole. The people in the chat are yelling over each other on the internet. There are too many responses and it's easy to see as someone who's not in the argument that it should have ended several responses ago. 

Sometimes, the best course of action is to walk away and not engage with the person at all. Use your judgment in how you approach rude or off-putting comments from strangers in real life to help you decide.

For a foundational understanding of these nuances, read [GitLab's guide to communicating effectively and responsibly through text](/company/culture/all-remote/effective-communication/).

**If you are unsure of how or if to respond to someone who has responded to your posts, consider joining the `#social_media` Slack channel for an easy way to connect with the social + PR team for help.**

## FAQ

**Am I required to share/like a post from @GitLab social channels?**\
No, you're not required to participate in any way.

**Am I required to have the social + PR team review my posts before I publish them?**\
No, this type of oversight directly contradicts our [results value, focused on giving our team members agency](https://about.gitlab.com/handbook/values/#give-agency). The GitLab social + PR team will never require a review or another oversight of your own personal social media posts.

**Am I required to identify myself as a GitLab employee on my social media channel?**\
No, you are not required to identify yourself as an employee.

**I don't want to write my own posts. I'd rather share @GitLab's social posts. Is there an easy way to know when they are published?**\
Yes, you can join the `#social_media` Slack channel for easy access to our latest posts as they publish.

**I don't know how to respond to a comment someone posted to me. How can I find help?**\
Consider joining the `#social_media` Slack channel for an easy way to connect with the social + PR team for help.

**What should I do if a friend/follower/user responds to me about controversies?**\
Consider not engaging with that person and ignoring their comments. The probability of "Twitter arguments" around various crises and controversies is high and it is recommended that you don't fall into a fight with someone on the internet.

**Where can I learn more about social media for our company?**\
You can find our @GitLab Social Media Guidelines [here](https://about.gitlab.com/handbook/marketing/social-media-guidelines/) and the social media handbook [here](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/). Note, these guidelines were created for company use, but they may have some good information for you. (And note, these two separate locations will be updated into one in the future)

## Acting as a public-facing expert or brand personality for GitLab
##### It's best to use your own social media profiles to respond, not GitLab brand channels. 
When we need assistance from an expert or the representative to a part of the company, we want your name and voice to deliver the message. Not only does this humanize the message, it provides an opportunity for you to extend your own reach and expertise by coaching others. If you are ever prompted to use a GitLab brand channel to respond, it should include a signature at the end of the message identifying who you are by your GitLab handle. 

## Responding to Requests

- **Respond to mentions.** Any time GitLab is mentioned or relevant to a thread it gets directed to our different chat channels and someone from GitLab should be responding to questions or feedback. The idea here is to be available for people without making them feel obligated to talk to you.
- **Respond quickly.** The urgency to respond depends on the medium. On Hacker News we need to respond within an hour or so, responses after an item is no longer on the homepage are much less valuable.
- **Everyone in the team helps.** Responding to social media is a task of all of us. Every team member is expected to contribute, especially if you have a leadership position.
- **Delegate to the experts.** Nobody knows everything. Send the most relevant team member that is available a link over chat so they can respond. Make sure they can follow up quickly or find someone else.
- **Answer yourself.**  We should be personable and human. So if something is delegated to you don't answer the question and have someone else post it. Engage yourself. Make or create an account for the service if needed.
- **Continue the conversation by providing meaningful responses instead of just upvoting or saying a one word “Thanks!”** If a post will add to a discussion, post it. If someone has given you feedback, link them to a relevant issue showing the feedback has been received. You can even provide people with links to non-GitLab things such as Git guides or DevOps guides. There is always a feature to mention or gratitude to express "Thank you so much for spreading the word about GitLab.". For an example see the first three responses to [a HN post where the first three comments were positive but no questions](https://news.ycombinator.com/item?id=12052695). Or see [this response](https://twitter.com/sytses/status/762578230197022720) that links to the actual issue and therefore a reply that the original 'thank you' didn't get.
- **Stay positive.** Don't feel the need to defend GitLab when someone says something negative (see "Assume good faith" below). Negative feedback is a great chance to learn or consider something new, so thank them for their feedback, document it in the issue tracker (add a comment to the issue if one is already open), and invite them to leave more comments if they wish. When people make criticisms, remember to acknowledge mistakes and stay constructive.
- **Remember that you’re speaking to a human being.** The people behind the comments are real, so treat them how you’d want to be treated if the roles were reversed. Try to find their real name so that you can personalize your message to them.
- **Begin by thanking them for their feedback or apologizing to them for the inconvenience they experienced.** We want to start and end conversations on a high note.
- **Assume good faith**. Remember, people are commenting on the product, not the people. The line can get blurry, but it’s important to stay objective.
- **Address any and all points the user has made.** If they made 4 points/requests, respond to each. If you don’t know the answer, don’t be afraid to tell them you don’t know but will look into it.
- **You’re allowed to disagree with people.** Try to inform the person (respectfully) why they might be misguided and provide any links they may need to make a more informed decision. Don’t say “you’re wrong” or “that’s stupid.” Instead try to say “I disagree because…” or “I don’t think that’s accurate because…”.
- **Replies are not endorsements.** Just because you’re replying and not publicly disagreeing doesn’t mean you agree with the statement.
- **Always seek feedback.** If someone has something negative to say, ask them how we could make it better. If they can provide examples that’s even better. As [said on HN](https://news.ycombinator.com/item?id=12235172): "the responsiveness and agreeableness to look into issues and invite people to be a part of the solution they seek out are phenomenal habits".
- **Open issues!** If someone has a point they’d like to discuss, feel free to open an issue and link them to it. Regardless of whether or not you agree with the point, you should be inviting the community to participate in GitLab’s direction. Be sure to link to the relevant post in the issue for easier tracking. This won’t work for all cases, so use your best judgment.
- **Provide details.** If the request or the question is opened to more than just a straight answer, don't be afraid to detail it as much as possible.
If there is already an opened issue or merge request for that case, link it to your answer. Do the same if there are other
sources of information to support your statement, such as [blog posts](/blog) and [webpages]().
- **Do what you say you will.** If you say you'll look into something, set a reminder to circle back with the person or link them to an issue on the issue tracker. Saying you'll look into something makes it impossible to continue the conversation until you do so, so it is essential that you continue the conversation at some later point. If this is too much work just say 'I don't know' and leave it at that.
- **Disclose**. If you comment on social media (Hacker News, Reddit, Twitter, etc.) your profile should mention that you work at GitLab. By mentioning that you work at GitLab your comment will rightfully be vetted more critically. If you post a lot don't mention it in every post, prefacing everything with 'GitLab CEO here' became annoying. A subtle disclosure is possible by speaking in the we form (we shipped, we messed up, we will make that our product).

## GitLab Voice

When speaking for GitLab, use the “GitLab voice.” When replying from the official GitLab account, speak as “we” and represent the software and community. On the official @GitLab account Twitter account and in other social media we should model attributes of our software and community. We strive to respond to all messages and questions. We respond by encouraging collaboration and contribution.

Consider what benefits the software and community, and how the software would respond if we personified it. Be responsive, positive, open minded, curious, welcoming, apologetic, transparent, direct, and honest. Someone doesn’t like something? Ask them to tell us more in the issue tracker. Someone thinks GitLab could be better? Invite them to submit a feature proposal. Any criticism is an opportunity to improve our software.

When responding to posts from your personal account, feel free to incorporate your own style and voice. Talk to people as if you were talking to them in person. 

Please do not engage in competitor bashing. Instead, highlight positive differences — it's best to focus on the ways that GitLab outperforms other solutions.

## Dealing with Conflict

You may come across angry users from time to time. When dealing with people who are confrontational, it’s important to remain level-headed. You may also send them to Sid directly.

- Assume good faith. People have opinions and sometimes they’re strong ones. It’s usually not personal.
- If it’s getting personal, step away from the conversation and delegate to someone else.
- Sometimes all people need is acknowledgment. Saying “Sorry things aren’t working for you” can go a long way. 

## Hacker News

- Never submit GitLab content to Hacker News. Submission gets more credibility if a non-GitLab Hacker News community member posts it, we should focus on making our posts interesting instead of on submitting it.
- Don't link to any Hacker News submissions to prevent setting off the voting ring detector. Trying to work around the voting ring detector by up-voting from the new page is not reliable, just don't announce nor ask for votes.
- Don't make the first comment on a HN post about GitLab, wait for people to leave comments and ask questions.
- Avoid using corporate jargon like 'PeopleOps'.
- Always address the HN community as peers. Be sure to always be modest and grateful in responses.
- If you comment yourself make sure it is interesting and relevant.

## Profile Assets

Profile assets for social media can be found [in the corporate marketing repository](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/social-media/profile-assets/png/assets/_general)

Please do not use the GitLab logo as the avatar for your personal accounts on social. You are welcome to use our branded banners, but it is important that your profile avatar does not lead users to confuse your account with the official GitLab accounts. 

While you should display the fact that you work at GitLab in your bio if you intend to advocate for GitLab on social, we suggest that you avoid including the word `gitlab` in your handle. Team member advocacy is incredibly valuable and we are lucky to have so many engaged team members, but creating an account to *solely* post about GitLab is not effective. The reason team member advocacy is so powerful is because people [trust employees](https://www.scribd.com/doc/295815519/2016-Edelman-Trust-Barometer-Executive-Summary) more than brands and executives. Your advocacy is powerful when it is authentic, and having an account that only exists to promote GitLab will not ring true to others who browse your tweets. 

For more info on representing GitLab on social, please see the [social marketing handbook](/handbook/marketing/corporate-marketing/social-marketing/).

## Connecting with Team Members on Social Media

In case you want to connect with fellow team members of GitLab on social media you have to keep it professional. With this communication we would want you to consider GitLab’s [Communication Guidelines](https://about.gitlab.com/handbook/communication/) at all times. Aligned with our [Anti-Harrassment Policy](https://about.gitlab.com/handbook/anti-harassment/) it is expected that everyone will contribute to an inclusive and collaborative working environment and respect each other at all times.
