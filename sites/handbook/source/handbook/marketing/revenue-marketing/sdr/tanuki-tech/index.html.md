---
layout: markdown_page
title: "Tanuki Tech"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

## Tanuki Tech

Tanuki Tech (short for "Tanuki Instutite of Technology") is the world class sales/technology bootcamp that we are building out for the SDR organization. Our goal is to powerfully equip our SDRs to be successful in their current roles as well as prepare them for their next role, whatever that may be. We are doing this because we care about our SDRs and want them to be successful.

## Goals
How do we measure success? The specific goals for this program are:
* Increased SDR effectiveness and organizational efficiency (increased quota attainment).
* Decrease time to value for SDR new hires (we want to equip our new SDRs to be successful quickly).
* **Job placement for SDRs who complete our program.** We want the people that we develop here as part of our SDR program to be abundantly prepared for their next role, whether that's in sales, marketing, or wherever else.

The average coding bootcamp here in the United States costs 13.5k and graduates report an average starting salary of 67k (CourseReport 2020). Our goal is to far exceed the job placement and starting salary metrics of our peer institutions, **completely as a free value to our SDRs as part of being part of this organization.**

## Guiding Principles
* Enablement should be powerful and effective. If there is something that you feel would help our organization and is currently missing or have suggestions for improvement, let us know and we will act.
  * At the end of each class, we will give surveys to figure out what's working and how we can improve.
* Enablement should be quick and effective. We are not trying to inundate you with information to eat up all of your time. Most courses in our curriculum are designed to take up around three hours including examination.
* We test often and we test behaviors that apply to your actual job. Research shows that the most effective teachers consistently test more than their peers. **Do not get discouraged if you fail a test; it is better to fail in front of a peer who is trying to help you than in front of a customer.**
* Class sizes should be small (especially for the 100- and 200-level courses. We aim to have a teacher-to-student ratio of 7:1 for 100- and 200-level courses.
* All classes are offered through Zoom and additionally have an optional asynchronous option (recordings and documentation). Examinations are either written or through Zoom.

## Curriculum
The following are the current courses that we offer here at Tanuki Tech. The following course list is currently flux.

**100s -**

TT100: Introduction to Technology.
> Serves as an introduction to the technology industry and GitLab's space in it. In this course, we will explain fundamental concepts such as applications, operations, and the hybrid cloud. We will also explain why technology has been the main driver of innovation in the twenty first century.
> 
> 3 credit hours, required.

TT101: Introduction to GitLab.
> GitLab is an extremely promising suite of technologies that is broad as well as deep. In this course, we will introduce GitLab and explain why it is such a special piece of technology.
> 
> 3 credit hours, required.

TT102: The People of Technology.
> To effectively engage in customer conversations, we need to understand the personas that make up the technology industry. In this course, we will explain the various people that you will meet, their responsibilities and drivers, and how to have effectively engage each type of audience.
>
> 3 credit hours, required.

TT190: SDR Technical Development.
> This is the Google Classroom course currently in place for Q2FY2021. This course will be deprecated at the end of the quarter.
>
> 9 credit hours, required.

**200s -**
TT200: Introduction to Development
> In this class, our goal is to have more effective conversations with developers by understanding what they do and learning to speak their language. Topics include life as a developer, how software is developed, where applications run, the public cloud, and APIs.
>
> 3 credit hours, required.

TT210: GitLab Create and Plan
> In this class, we go in depth explaining create and plan functionality. Topics include GitLab's SCM and Agile featureset, and competitive differentiators. Students will use GitLab for their own mini-project.
>
> 3 credit hours, optional.

TT211: GitLab Verify and Security
> In this class, we will learn about verify (CI) and security functionality. We will talk about what we bring to the table and product differentiators.
>
> 3 credit hours, optional.

TT280: Technical Questions for Sales
> This is a class offered by the sales development organization equipping salespeople to answer the most commonly asked technical questions. You may see the curriculum [here](https://about.gitlab.com/handbook/sales/training/technical-questions-for-sales/).
>
> 4 credit hours, optional.

**300s -**
TBD

**400s -**
TBD

**500s -**
TBD

## Enrollment

To enroll for a class:
* Surveys will go out the last two weeks of each quarter. Classes will begin promptly the first week of the next quarter.
* **Live classes are filled on a first-come-first-serve basis and there is no guarantee that you will be given a spot**. Once again, all classes have an asynchronous option so if a class is full, you can always watch the recording.
* Exact logistics TBD.

## SDR Leveling

Completing coursework is directly tied to SDR leveling.
* To qualify for SDR2, you must complete TBD 100-level credits.
* To qualify for SDR3, you must complete TBD credits and pass all required courses.
* To qualify for SDR4, you must complete TBD credits and pass all required courses.

## Dean of Tanuki Tech

[Christopher Wang]([https://about.gitlab.com/company/team/#cs.wang](https://about.gitlab.com/company/team/#cs.wang)) currently serves as the first dean of Tanuki Tech.
