---
layout: handbook-page-toc
title: "Success Plans"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

----

- [Success Plan Highlights and Things to Keep in Mind](https://www.youtube.com/watch?v=qY--n6Obexk&list=PL05JrBw4t0KpThYLhg5uck84JLxljUi_f&index=3&t=0s)
- [Success Plan FAQs](https://www.youtube.com/watch?v=bfxy8mXnudE&list=PL05JrBw4t0KpThYLhg5uck84JLxljUi_f&index=4&t=0s)
- [Gainsight Update: Feb 2020](https://www.youtube.com/watch?v=mV-72Nga_z0&list=PL05JrBw4t0KpThYLhg5uck84JLxljUi_f&index=2&t=0s)

## What is a Success Plan?

A success plan is the guiding document which connects the customer’s pain (often the reason for purchasing) to their GitLab solution and verified business outcomes. It is a living roadmap for initiatives GitLab is working on with the customer, tailored to each individual customer based on their needs and adoption.

There are two types of success plans in [Gainsight](/handbook/customer-success/tam/gainsight/), which is GitLab's Customer Success tool and where we track customer success plans.

1. The first type is the ROI Success Plan. This success plan is a customer-facing interactive map to align the purchase (or renewal) reasons to customer outcomes, develop joint accountabilities, measure progress, and evolve as customers’ needs change. It’s the way the customer can see and know that they’re attaining significantly more value than the cost of a product or service over time. Since this is customer-facing, it provides a high-level, executive overview of the customer's top initiatives.
2. The second type is the Stage Adoption Success Plan. This success plan is an internal document allows the TAM to track progress and goals on driving stage adoption within an account. Since this is internal, we can track high-level initiatives for the customer such as stage adoption, efforts on uptiering, expansion efforts, etc.

For more information on what success plans are, view the slide deck, [Success Plans: Foundation for Success](https://docs.google.com/presentation/d/15Qt-UcfRt9cX-4CV7zMsurojTZg_8Kf-u0dMYL16JXQ/edit?ts=5e398d2d#slide=id.g6e9082c768_3_234), that helped to kick off GitLab's use of them.

## What is not a Success Plan?

A success plan should not be confused with the use of [collaboration projects](#differences-between-collaboration-projects-and-success-plans), which are more tactical and less strategic than success plans. Collaboration projects may show some element of strategy and progress as they are used to collaborate on initiatives; however, it is harder to measure and consume the information in issues across projects and groups and in other systems.

A key element of transparency is clarity and focus. If a strategic document becomes noisy with day to day activities, it becomes less transparent. With dedicated success plans, we can focus on our strategic and holistic initiatives and track our progress.

## Purpose of Success Plans

- Document the customer’s desired outcomes (e.g., KPIs, problems to solve, benefits to achieve)
- Align GitLab’s product adoption plan to customer outcomes
- Define activity (e.g., change management, training) roadmaps and timeframes for a successful adoption journey
- Develop shared understanding and commitment to the plan between GitLab and customer stakeholders
- Serve as baseline to track delivery of outcomes and adoption
- Provide foundation for supporting processes like health checks and business reviews

## How to Create a Success Plan in Gainsight

To create a success plan, the TAM will perform the following activities, detailed below:

1. [Understand the customer’s motivation for purchasing GitLab through reviews with the sales team](/handbook/customer-success/tam/success-plans/#step-1-understand-the-customers-motivation)
1. [Identify the customer’s business objectives](/handbook/customer-success/tam/success-plans/#step-2-identify-business-objectives)
1. [Socialize and confirm the business objectives with the customer](/handbook/customer-success/tam/success-plans/#step-3-socialize-and-confirm-the-business-objectives)
1. [Document the success plan in Gainsight](/handbook/customer-success/tam/success-plans/#step-4-documenting-the-success-plan)
1. [Track realization of the objectives outlined in the success plan](/handbook/customer-success/tam/success-plans/#step-5-track-value)

### Step 1: Understand the Customer’s Motivation

- Review the opportunity [Command Plan](/handbook/sales/command-of-the-message/#command-plan) in Salesforce
- Work with the SAL/AE and SA to understand the customer's reasons for purchasing (e.g. [Value Driver](/handbook/sales/command-of-the-message/#customer-value-drivers)) and their expectations or Positive Business Outcomes (PBOs), as well as any potential risks or barriers, key stakeholders, and any other relevant information regarding their desire to use GitLab
- Prepare discovery questions to discuss with the customer to collect any missing information
- Review [account onboarding](/handbook/customer-success/tam/onboarding/) and [account engagement](/handbook/customer-success/tam/engagement/) for additional information that is useful to collecting information and developing a relationship

### Step 2: Identify Business Objectives for the ROI Success Plan

The objectives should be written from the customer’s perspective and be measurable goals that they drive. For example, an Objective could be “Reduce Cycle Time from X to Y” rather than “Adopt Manage stage” (which would be more appropriate for a Stage Adoption success plan).

You should know how the customer will track the benefits of purchasing GitLab, how the customer will measure success, and how the customer is currently measuring these objectives and if they have those metrics available. If these details are currenly unknown, add some relevant discovery questions to your agenda for your next [cadence call](/handbook/customer-success/tam/cadence-calls/), below are some examples:

- Who on the customer side might have important or relevant input?
- Why is each business outcome important?
- What outcomes or ROI is the customer pursuing?
- Who will notice or benefit from this outcome?
- Who will contribute to the effort?
- When do we need to reach this goal?
- What steps do we need to take to get there?
- What roadblocks are there, if any?

Business objectives can be either quantitative or qualitative, but both types still need to be actionable and measurable. See below for examples and common pitfalls of each.

**Numerical Goals (quantitative)**: data of past events, such as time saved or money earned

Examples:
- 20% reduction in Cycle Time
- XX% change to Deployment Frequency

Common pitfalls:
- Using unrealistic objectives: be transparent about what we can achieve
- Not addressing the real goal: don't say “activate 50% of users” when we really want to measure the impact of GitLab to their bottom line
- Not making them measurable: “Delight the customer!” is a great goal, but not measurable

**Initiatives (qualitative)**: description of an outcome that will be a substantial strategic win for the customer

Examples:
- “Migrate to single App for DevOps lifecycle”
- “Launch SAST and DAST across the company”

Common pitfalls:
- Getting into the weeds: don't use technical jargon that your contact may understand, but not the decision-maker or executive team
- Including "nice-to-haves" or out-of-date objectives: focus on the top 1-3 customer initiatives
- Being ambiguous: instead of saying “increase team collaboration”, say "50% of users are contributing to the project"

### Step 3: Socialize and Confirm the Business Objectives for the ROI Success Plan

All stakeholders should agree on the business objectives that the project is pursuing, which products and services will help them reach that goal, and to keep the scope focused to 1-3 objectives. As you close out objectives, you can restart the process to define and add new ones.

Start by validating what we understand to be the customer’s pain and your best practices for the group to adapt from there.

For each business objective, review with the customer:
- What is the objective's priority relative to the others'?
- Which stakeholders will benefit?
- How should we prioritize efforts for your teams?
- Are there any quick wins?
- Are there deadlines for individual steps or overall completion?
- Key contributions & responsibilities of everyone involved

### Step 4: Documenting the Success Plan

To create a success plan in Gainsight, perform the following steps:

1. From the Gainsight NXT home page, navigate to the customer's page
1. From the menu on the left-hand side of the screen, choose "Success Plan"
1. In the upper right-hand side, click "+ Success Plan"
1. Add a Name for the success plan, e.g. "Customer Name ROI Success Plan" for customer-facing plans and "Customer Name Stage Adoption Success Plan" for internal plans
1. Add Type as "ROI Success Plan" for customer-facing plans or "Stage Adoption" for internal plans
1. Set the "Due Date" as end of GitLab's fiscal calendar or a date logical given the content of the success plan
1. Click "Save"

After creating a success plan, you will need to input the objectives that you've collected. Success plans can have multiple objectives, though it’s best to limit it to 1-3 for focus and achievability. Key components of the objectives in the success plan include:

- **Name**: Objective title, such as “Cut Time to First Commit by 20%”
- **Owner**: Who is invested in this objective
- **Due Date**: Due date for the objective
- **Objective Category**:  Indicate if the objective is Stage Adoption, Rev Expansion, or ROI Success
   - **Stage Name**: if `Stage Adoption` is selected, select the Stage Name for reference and reporting purposes
- **Stage Name**: if the objective category above is `Stage Adoption`, then select from this dropdown which stage you're helping the customer adopt
- **Status**: Mark the objective “New” or “Work in Progress”
- **Priority**: Indicate if the priority is Low, Medium, or High
- **Playbook**: If the objective is for Stage Adoption, choose the corresponding playbook
- **Success Criteria**: When you first create the Objective (and each time you check in with the customer), log an Activity update on it to capture the progress
- **Comments**: Share any info that might be relevant to the objective (e.g. potential blockers, architecture details, or other important details worth mentioning)

Objectives should be actionable, and Gainsight provides a way to create action items as part of the objective, called tasks. A task in Gainsight is equivalent to a milestone in GitLab's historical success plan terminology.

To create a task, perform the following steps:

1. Click on the objective so that the side panel appears on the right
1. To the right of the due date, click the three vertical dots, and click "Add Task"
1. Fill out the details of the task:
   - **Name**: The title of the task, such as "Establish baseline metrics for comparision"
   - **Owner**: The person who is responsible for this task
   - **Due Date**: When the task should be completed by
   - **Status**: Is the task ongoing or completed?
   - **Priority**: In relation to other tasks for the objective, how important is it?
   - **Description**: Any additional details that are helpful to understanding what the task is, such as external references, risk factors that could impact the task, etc.
   - **Milestone Details**: Additional details specific to our success plan structure
     - **Customer DRI**: Who on the customer side is responsible for the task?
     - **Milestone Risk**: How significant are the risk factors that could impact being able to complete the task?
     - **Progress (%)**: How far along is the task?

Tasks will affect the overall completion of the objective, and provide more granular visibility into progress on the objective when looking at the Gantt chart. To do this, navigate to the Gantt chart tab (next to the Objectives tab) in the success plan and confirm the representation captures the plan. You can adjust dates accordingly (for example, if a task actually started in the past but the entry defaulted its start date to today's date).

Finally, next to the success plan due date, change the "Status" of the success plan from "Draft" to "Active".

### Step 5: Track Value of the ROI Success Plan

It's important to share progress with everyone involved as time goes on. The Sales team and the customer should both be kept up to date on where the success plan objectives and tasks stand, so they can continue working on new tasks and in turn sharing the progress with anyone else they think should be aware.

A customer’s business and strategies will change, so the value that they need from you will change with that. To stay up to date, show the success plan to the customer regularly to help keep a fresh understanding of their needs. You can email a report to the customer once a month (or other frequency), listing the objectives and inviting them to reply if they’re out of date.

To share a ROI Success Plan, click the link icon next to the success plan due date and status, search for the users you want to share it with, then click "Preview and Send" and send the email. Alternatively, you can export the success plan by clicking "Export" at the top right.

It's also helpful to identify key times when you interact with a customer that would be good opportunities to review and refresh the success plan. This would ideally be when a discussion of business goals feels appropriate and the right stakeholders are at the table, for example: key handoffs between teams, [EBRs](/handbook/customer-success/tam/ebr/), or executive check-ins.

It's recommended for TAMs to use [EBRs](/handbook/customer-success/tam/ebr/) and/or other recurring meetings such as [cadence calls](/handbook/customer-success/tam/cadence-calls/)) to review steps achieved thus far and set next steps or new objectives as needed. After these meetings, it's important to log an "Activity" in the relevant objective to record how the customer is trending towards their goals. Before closing an objective, get confirmation from a customer (ideally in writing) that it has been achieved and include that in the activity log.

To keep track that the success plan is up to date, use the custom date field on the Objective in the success plan, “Last Validated”. The TAM will update it when they get confirmation from the customer that it is still a business priority for them.

Internally, TAMs can use the data to track their own trends and objectives achieved over time (e.g. quarter over quarter reports) and use the progress of the success plan to measure the ROI health scorecard.

## Differences between Collaboration Projects and Success Plans

### Collaboration Projects

Collaborations projects are meant for continuous use with our customers to track/create issues, collaborate with product/SAs/others on feature requests, engagements, blockers, etc. The collaboration project is often focused on the tactical, day-to-day, and is more specific to Developers, DevOps Leads, Engineering Managers, and Admins, because they use it to talk about issues to solve as opposed to an overall business objectives.

Within the collaboration project, TAMs focus on new/closed/open issues and collaboration with the customer on these issues. It's the primary communication method for the TAM to communicate with the customer on a day-to-day basis and is a way to help the customers adopt using GitLab.

For additional details, see [Account Engagement](https://about.gitlab.com/handbook/customer-success/tam/engagement/).

### Gainsight ROI Success Plans

Meanwhile, Gainsight ROI Success Plans are a separate entity, as they are meant for articulating and tracking business objectives, typically with executive sponsors, decision-makers, and economic buyers. The succcess plans focus on high-level strategic objectives, instead of the technical and tactical initiatives that are covered in the collaboration project.

TAMs use success plans as part of their EBRs to share insights, progress, and objectives to offer deeper insight into their organization and the value of GitLab, as success plans can track progress towards goals and report on it to the executives.

**Stage Adoption**: If a customer has as one of *their* desired business outcomes the adoption of one of Gitlab's stages, then this stage adoption objective would be appropriate to add as a customer objective in the ROI Success Plan.

### Gainsight Stage Adoption Success Plans

Gainsight Stage Adoption Success Plans are internal-only and used to track stage adoption campaigns, driving expansion and growth efforts. We see across our customer base an even better retention and growth rate among customers who have adopted 3 stages or more, and we want to measure and iterate on our efforts to drive stage adoption. Data points such as outcomes per stage, length of time for customers to adopt new stages, number of customers adopted per stage/region/segment, and more help us manage growth strategies towards IACV growth. It also gives us the ability to see the relationship between adoption goals and outcomes and relate feedback and blockers to the wider team to help improve GitLab's processes.

The reporting on stage adoption happens at the objective level, so it is key to open an objective with "Stage Adoption" as the category and choose the relevant stage from the dropdown list below.

If we are looking to drive the adoption of a particular stage in a customer account but the customer themselves would not say (yet!) it is a strategic objective for them, then a TAM should use the Stage Adoption Success Plan to open the objective. This second success plan ensures that all customer-facing objectives are aligned with the customer's own goals and strategies but allows us to still track our internal initiatives.

### Open and Categorize a Stage Adoption Objective

Please review this [3-minute video](https://youtu.be/gWW3t45QCFs) on how to open a stage adoption objective and categorize it correctly to enable reporting on our team's progress (Gitlab only).
