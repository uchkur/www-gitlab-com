---
layout: handbook-page-toc
title: "Business Operations Performance Indicators" 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Performance Indicators (PI)

## Adoption of Data Team BI charts throughout company
This performance indicator is tracked by several metrics including:
*  End User Adoption of BI charts from Data Team by views and minutes 
*  % of GitLab Team that utilizes BI charts from Data Team
*  % reports automated 

**Aligns with the following core business objectives**:
*  Follow and improve our processes for maintaining high quality data and reporting.
*  Build rapport with internal stakeholders to encourage data-driven decision-making. 


## Average Delivery Time of Laptop Machines < 21 days 
This performance indicator tracks the average delivery time of laptop machines. The goal is that 99% of laptops will arrive prior to start date or 21 days from the date of order.
 
**Aligns with the following core business objectives**:
*  Ensure system / hardware availability 


## Average Merge Request 
This performance indicator tracks the growth of the knowledge base across teams.
 
**Aligns with the following core business objectives**:

**Data Team**
*  Follow and improve our processes for maintaining high quality data and reporting.
*  Ensure goal to keep systems up and running while giving insights into partnership with Infrastructure team.
*  Ensure data availability for data analytics and reporting for business units to make data-driven decisions. 

**IT Systems**
*  Build a knowledge base of IT practices and pragmatic problem solving in the handbook
*  Support Weekly IT Onboarding Sessions for new Team Members
*  Train end-users how to setup and use new technologies


## Average Issues 
This performance indicator tracks the growth of the knowledge base across teams.
 
**Aligns with the following core business objectives**:

**Data Team**
*  Follow and improve our processes for maintaining high quality data and reporting.
*  Ensure goal to keep systems up and running while giving insights into partnership with Infrastructure team.
*  Ensure data availability for data analytics and reporting for business units to make data-driven decisions. 

**IT Systems**
*  Build a knowledge base of IT practices and pragmatic problem solving in the handbook
*  Support Weekly IT Onboarding Sessions for new Team Members
*  Train end-users how to setup and use new technologies


## Cost Actual vs Plan 
This performance indicator is tracked by several metrics including: Infrastructure Cost vs Plan and Hiring Cost vs Plan. This PI is to track the financial position of all the team cost vs. the budget set by the team. 

**Aligns with the following core business objectives**:
*  [Hires vs Plan](https://about.gitlab.com/handbook/hiring/performance_indicators/#hires-vs-plan)

## Customer Satisfaction Survey (CSAT)
This performance indicator measures how satisfied our customers' (internal GitLab employees) are with their interaction with the GitLab IT team. This is based on survey responses from customers. 


## Cycle Time for IT Support Issue Resolution 
This performance indicator tracks against the SLA for requests. 

**Aligns with the following core business objectives**:
*  Triage all IT related questions as they arise
*  On call support for immediate software and hardware issues during local business hours


## Discretionary bonus per employee per month > 0.1

**Aligns with the following core business objectives**:
* [Discretionary Bonuses](https://about.gitlab.com/handbook/incentives/#discretionary-bonuses)  


## Evaluating System or Process Efficiency
This performance indicator tracks the efficiency of new or updated system or processes. 

**Aligns with the following core business objectives**:
*  Understand system capabilities and business requirements, and drive standard & scalable solutions. Anticipate risks and mitigating them before they become serious.
*  Identify opportunities to increase efficiency and productivity within the context of the overall business strategy

## Infrastructure Cost vs Plan
This performance indicator tracks the financial position of the actual cost vs the planned costs for the data infrastructure (warehouse, ETL pipelines, etc.). 

**Aligns with the following core business objectives**:
*  Follow and improve our processes for maintaining high quality data and reporting.
*  Tracks the financial cost of the data infrastructure set up. 


## IT ROI
This performance indicator tracks the financial position of the IT systems by looking at the net profits of an investment and what it cost to implement it. This is often expressed as a percentage comparing the profits to the cost. 

**Aligns with the following core business objectives**:
*  Build, scale and manage our IT Operations and Helpdesk teams to support our needs as distributed company
*  Create and execute a plan to develop and mature our IT capabilities


## Mean Time between Failures (MTBF)
This performance indicator tracks the mean time since last system failure based on set cadence.

**Aligns with the following core business objectives**:
*  Follow and improve our processes for maintaining high quality data and reporting.
*  Ensure goal to keep systems up and running while giving insights into partnership with Infrastructure team.
*  Ensure data availability for data analytics and reporting for business units to make data-driven decisions. 


## Mean Time to Repair (MTTR)
This performance indicator tracks the mean time to get the system back up and running based on set cadence.
 
**Aligns with the following core business objectives**:

**Data Team**
*  Follow and improve our processes for maintaining high quality data and reporting.
*  Ensure goal to keep systems up and running while giving insights into partnership with Infrastructure team.
*  Ensure data availability for data analytics and reporting for business units to make data-driven decisions. 

**IT Systems**
*  Diagnose computer errors and provide technical support
*  Troubleshoot software and hardware


## New Hire Location Factor < 0.69
* [Average Location Factor](https://about.gitlab.com/handbook/people-group/people-operations-metrics/#average-location-factor)
* [Finance Team KPIs](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/#finance-team-kpis)


## Number of days since last environment audit 
Number of days since the last audit on all the data, processes, and users within a given system. We will set a cadence that will be the cap we will measure against. 

**Aligns with the following core business objectives**:

**Data Team**
*  Follow and improve our processes for maintaining high quality data and reporting.

**IT Systems**
*  Develop tooling and process to facilitate end-user asset management, provisioning and tracking

**All**
*  Tracks our alignment with security, compliance, and legal.


## Number of new trainings hosted per month > 2 
Number of new Data Team hosted trainings to teach data-driven decision-making. 

**Aligns with the following core business objectives**:
*  Build rapport with internal stakeholders to encourage data-driven decision-making. 
*  Encourage DataOps across the company.


## Percent (%) of company data in data warehouse
Percentage of all company data in data warehouse

**Aligns with the following core business objectives**:
*  Build rapport with internal stakeholders to encourage data-driven decision-making. 
*  Helps to identify if there are data sources not within the current data warehouse.
*  Helps to identify if there may be data quality discrepancies across different business units. 
*  Helps to achieve an unified data warehouse that allows for the mapping of user journeys. 


## Percent (%) of issues requested triaged with first response within 36 hours (per business unit)
Percentage of all issues that are requested by internal stakeholders outside of the Data Team that are triaged with a first response within 36 hours. 

**Aligns with the following core business objectives**:
*  Build rapport with internal stakeholders to encourage data-driven decision-making. 


## Percent (%) of team who self-classify as diverse
Percentage of team who self-classify as diverse. This is one metric of many to track the diversity lifecycle. 

**Aligns with the following core business objectives**:
*  [Diversity Lifecycle: Applications, Recruited, Interviewed, Offers Extended, Offers Accepted, and Retention](https://about.gitlab.com/handbook/hiring/metrics/#diversity-lifecycle-applications-recruited-interviewed-offers-extended-offers-accepted-and-retention)
*  [Diversity, Inclusion & Belonging](https://about.gitlab.com/company/culture/inclusion/)

## Percent (%) of vendor spend on Purchase Order
Percentage of all vendor spend for any department that is purchased via PO. 

**Aligns with the following core business objectives**:
*  SOX Compliance.
*  Streamline the purchasing process.


## SLO achievement per data source 
This performance indicator tracks all metrics related to achieve the service-level objective (SLO) per data source.

**Aligns with the following core business objectives**:

**Data Team**
*  Ensure data availability for data analytics and reporting for business units to make data-driven decisions. 
*  Follow and improve our processes for maintaining high quality data and reporting.
*  Build rapport with internal stakeholders to encourage data-driven decision-making. 
*  Encourage DataOps across the company.

**IT Systems**
*  Develop secure integrations between Enterprise Business Systems and with our Data Warehouse
*  Build API Integrations from the HRIS to third party systems and GitLab.com


## Support Tickets and Access Request Closed per Employee 
This performance indicator tracks the number or percentage of support tickets closed per Employee. 

**Aligns with the following core business objectives**:
*  Track productivity and resource allocation. 


## System roll out vs plan 
This performance indicator tracks the time it took to roll out a new system vs the planned time for that system roll out. 

**Aligns with the following core business objectives**:
*  Creating a road-map for system changes
*  Understanding bandwidth constraints on the IT team 


## Vendor cost avoidance
Monitor the cost avoidance achieved through the procure to pay process. Cost Avoidance is the savings achieved off of the initial vendor proposal price. Note this is not directly tied to budget. 

**Aligns with the following core business objectives**:
*  Control spend and build a culture of long-term savings on procurement costs.
*  Streamline the purchasing process.
*  Minimize financial risk.

